/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2008 by CANTICO ( http://www.cantico.fr )              *
 ************************************************************************/

/**
 * Permet de copier une url dans le presse-papier. La fonction lance une popup avec l'url � copier dans un champ input pour les navigateurs n'acceptant pas la copie par s�curit�.
 * 
 * @access  public
 * @param   string	url	url http
 */
function multimediaspace_copieurl(url) {
	var copywin = window.open('','copy','status=no,menubar=no,personalbar=no,width=300,height=50,top=100,left=100,scrollbars=yes,resizable=yes');
	copywin.document.write('<div align="center"><input type="text" style="width:100%;" id="copy" value="'+url+'"><br /><br /><input type="button" value="Fermer" onclick="window.close()"></div>');

	var copy = copywin.document.getElementById('copy');
	copy.focus();
	copy.select();
	if (typeof copy.createTextRange != 'undefined') {
		therange=copy.createTextRange();
		therange.execCommand("Copy");
		copywin.close();
	}
}

/* Ordonner un choix dans une liste � choix multiples */
function multimediaspace_move(bDir, el) {
	var idx = el.selectedIndex
	if (idx != -1) { //une case est s�lectionn�e
		if( idx == 0 && bDir == true)
			return;
		if( idx == el.options.length-1 && bDir == false)
			return;
		var nxidx = idx + ( bDir ? - 1 : 1)
		if ( nxidx<0 )
			nxidx=el.length-1
		if ( nxidx>=el.length) nxidx=0
		var oldVal = el[idx].value
		var oldText = el[idx].text
		el[idx].value = el[nxidx].value
		el[idx].text = el[nxidx].text
		el[nxidx].value = oldVal
		el[nxidx].text = oldText
		el.selectedIndex = nxidx
	}
}

/* S�lectionne tous les �l�ments d'une liste � choix multiples */
function multimediaspace_selectAll(list) {
	for(i=0; i < list.options.length; i++) {
		list.options[i].selected = true;
	}
}

/* Permet d'imprimer la page */
function multimediaspace_imprimer() {
	window.print();
}

/* Ferme la fen�tre en cours */
function multimediaspace_fermefenetre() {
	window.close();
}
