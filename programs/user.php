<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2008 by CANTICO ( http://www.cantico.fr )              *
 ************************************************************************/
include_once 'base.php';

//Chargement des fonctions
global $babAddonPhpPath;
global $babInstallPath;
global $babBody;

include_once dirname(__FILE__) . '/fonctionsgen.php';
include_once dirname(__FILE__) . '/fonctions.php';
include_once dirname(__FILE__) . '/adminpages.php';
include_once dirname(__FILE__) . '/userpages.php';

//Menu Utilisateur
if (!isset($idx )) {
	$idx = 'utilisateur';
}

//Gestion des erreurs : si vide pas d'erreurs
$erreurs = array();
//Test si l'utilisateur est bien authentifi�
$estauthentifie = multimediaspace_estauthentifie();

switch($idx) {
	
	case 'gestion':
		/* V�rifie si l'utilisateur est gestionnaire */
		if (multimediaspace_estgestionnaire()) {
			multimediaspace_titre('Management');
			multimediaspace_gestion($donnees, $erreurs);
			$espacemultimedia = new Multimediaspace_EspaceMultimedia();
			if ($espacemultimedia->adroitslecture()) {
				$repertoireparent = $espacemultimedia->repertoireparent();
				multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
			}
			multimediaspace_onglet('gestion','Management','user&idx=gestion');
			multimediaspace_onglet('fichiersovmlgestion','Management of files OVML','user&idx=fichiersovmlgestion');
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page");
		}
		break;


	case 'fichiersovmlgestion':
		/* V�rifie si l'utilisateur est gestionnaire */
		if (multimediaspace_estgestionnaire()) {
			multimediaspace_titre('Management of files OVML');
			multimediaspace_fichiersovmlgestion($donnees, $erreurs);
			$espacemultimedia = new Multimediaspace_EspaceMultimedia();
			if ($espacemultimedia->adroitslecture()) {
				$repertoireparent = $espacemultimedia->repertoireparent();
				multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
			}
			multimediaspace_onglet('gestion','Management','user&idx=gestion');
			multimediaspace_onglet('fichiersovmlgestion','Management of files OVML','user&idx=fichiersovmlgestion');
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page");
		}
		break;


	case 'ajoutfichierovmlgestion':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				multimediaspace_titre('Add of a file OVML');
				multimediaspace_ajoutfichierovmlgestion($erreurs);
				$espacemultimedia = new Multimediaspace_EspaceMultimedia();
				if ($espacemultimedia->adroitslecture()) {
					$repertoireparent = $espacemultimedia->repertoireparent();
					multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
				}
				multimediaspace_onglet('gestion','Management','user&idx=gestion');
				multimediaspace_onglet('fichiersovmlgestion','Management of files OVML','user&idx=fichiersovmlgestion');
				multimediaspace_onglet('ajoutfichierovmlgestion','Add of a file OVML','user&idx=ajoutfichierovmlgestion');
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
		}
		break;


	case 'ajoutfichierovmlgestionenr':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				$champs = array('antif5', 'cheminfichierovml', 'description');//champs � r�cup�rer
				$donnees = multimediaspace_recuperedonnees('post',$champs);
				$champsnv = array('antif5', 'cheminfichierovml'); //champs obligatoires (non vides)
				if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
					//V�rifie le formulaire pour emp�cher les doublons avec F5
					$antif5 = multimediaspace_antif5(MULTIMEDIASPACE_ANTIF5, $donnees['antif5']);
					if ($antif5) {
						multimediaspace_ajoutfichierovmlgestionenr($donnees,$erreurs);
					}
				}
				//Des champs n'ont pas �t� renseign�s
				$idx = 'fichiersovmlgestion';
				multimediaspace_titre('Management of files OVML');
				multimediaspace_fichiersovmlgestion($erreurs);
				$espacemultimedia = new Multimediaspace_EspaceMultimedia();
				if ($espacemultimedia->adroitslecture()) {
					$repertoireparent = $espacemultimedia->repertoireparent();
					multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
				}
				multimediaspace_onglet('gestion','Management','user&idx=gestion');
				multimediaspace_onglet('fichiersovmlgestion','Management of files OVML','user&idx=fichiersovmlgestion');
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
		}
		break;


	case 'suppressionfichierovmlgestion':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				$champs = array('idfichierovml');//champs � r�cup�rer
				$donnees = multimediaspace_recuperedonnees('get',$champs);
				$champsnv = array('idfichierovml'); //champs obligatoires (non vides)
				if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
					multimediaspace_suppressionfichierovmlgestion($donnees, $erreurs);
					multimediaspace_titre('Deletion of a file OVML');
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
					multimediaspace_onglet('fichiersovmlgestion','Management of files OVML','user&idx=fichiersovmlgestion');
					multimediaspace_onglet('suppressionfichierovmlgestion','Deletion of a file OVML','user&idx=suppressionfichierovml&idfichierovml='.$donnees['idfichierovml']);
					break;
				} else {
					//Des champs n'ont pas �t� renseign�s
					$idx = 'gestion';
					multimediaspace_titre('Management');
					multimediaspace_gestion($donnees, $erreurs);
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
				}				
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
		}
		break;


	case 'suppressionfichierovmlenr':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				$champs = array('idfichierovml', 'antif5');//champs � r�cup�rer
				$donnees = multimediaspace_recuperedonnees('post',$champs);
				$champsnv = array('idfichierovml', 'antif5'); //champs obligatoires (non vides)
				if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
					//V�rifie le formulaire pour emp�cher les doublons avec F5
					$antif5 = multimediaspace_antif5(MULTIMEDIASPACE_ANTIF5, $donnees['antif5']);
					if ($antif5) {
						multimediaspace_suppressionfichierovmlgestionenr($donnees,$erreurs);
					}
				}
				//Des champs n'ont pas �t� renseign�s
				$idx = 'fichiersovmlgestion';
				multimediaspace_titre('Management of files OVML');
				multimediaspace_fichiersovmlgestion($donnees, $erreurs);
				$espacemultimedia = new Multimediaspace_EspaceMultimedia();
				if ($espacemultimedia->adroitslecture()) {
					$repertoireparent = $espacemultimedia->repertoireparent();
					multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
				}
				multimediaspace_onglet('gestion','Management','user&idx=gestion');
				multimediaspace_onglet('fichiersovmlgestion','Management of files OVML','user&idx=fichiersovmlgestion');
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
		}
		break;


	case 'editionfichierovmlgestion':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				$champs = array('idfichierovml');//champs � r�cup�rer
				$donnees = multimediaspace_recuperedonnees('get',$champs);
				$champsnv = array('idfichierovml'); //champs obligatoires (non vides)
				if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
					multimediaspace_titre('Edition of a file OVML');
					multimediaspace_editionfichierovmlgestion($donnees,$erreurs);
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
					multimediaspace_onglet('fichiersovmlgestion','Management of files OVML','user&idx=fichiersovmlgestion');
					multimediaspace_onglet('editionfichierovmlgestion','Edition of a file OVML','user&idx=editionfichierovmlgestion&idfichierovml='.$donnees['idfichierovml']);
				} else {
					//Des champs n'ont pas �t� renseign�s
					$idx = 'gestion';
					multimediaspace_titre('Management');
					multimediaspace_gestion($donnees, $erreurs);
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
				}
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
		}
		break;


	case 'editionfichierovmlgestionenr':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				$champs = array('idfichierovml', 'antif5', 'cheminfichierovml', 'description');//champs � r�cup�rer
				$donnees = multimediaspace_recuperedonnees('post',$champs);
				$champsnv = array('idfichierovml', 'antif5', 'cheminfichierovml'); //champs obligatoires (non vides)
				if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
					//V�rifie le formulaire pour emp�cher les doublons avec F5
					$antif5 = multimediaspace_antif5(MULTIMEDIASPACE_ANTIF5, $donnees['antif5']);
					if ($antif5) {
						multimediaspace_editionfichierovmlgestionenr($donnees,$erreurs);
					}
				}
				//Des champs n'ont pas �t� renseign�s
				$idx = 'fichiersovmlgestion';
				multimediaspace_titre('Management of files OVML');
				multimediaspace_fichiersovmlgestion($donnees, $erreurs);
				$espacemultimedia = new Multimediaspace_EspaceMultimedia();
				if ($espacemultimedia->adroitslecture()) {
					$repertoireparent = $espacemultimedia->repertoireparent();
					multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
				}
				multimediaspace_onglet('gestion','Management','user&idx=gestion');
				multimediaspace_onglet('fichiersovmlgestion','Management of files OVML','user&idx=fichiersovmlgestion');
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
		}
		break;


	case 'ajoutrepertoire':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				$champs = array('idrepertoireparent');//champs � r�cup�rer
				$donnees = multimediaspace_recuperedonnees("get",$champs);
				$champsnv = array('idrepertoireparent'); //champs obligatoires (non vides)
				if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
					multimediaspace_ajoutrepertoire($donnees, $erreurs);
					multimediaspace_titre('Add a subfolder');
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
					multimediaspace_onglet('ajoutrepertoire','Add a subfolder','user&idx=ajoutrepertoire&idrepertoireparent='.$donnees['idrepertoireparent']);
				} else {
					$idx = 'gestion';
					multimediaspace_titre('Management');
					multimediaspace_gestion($donnees, $erreurs);
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
				}
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
		}
		break;


	case 'ajoutrepertoireenr':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				$champs = array('idrepertoireparent','antif5','nom','heritedesdroits');//champs � r�cup�rer
				$donnees = multimediaspace_recuperedonnees("post",$champs);
				$champsnv = array('idrepertoireparent','antif5','nom','heritedesdroits'); //champs obligatoires (non vides)
				if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
					//V�rifie le formulaire pour emp�cher les doublons avec F5
					$antif5 = multimediaspace_antif5(MULTIMEDIASPACE_ANTIF5, $donnees['antif5']);
					if ($antif5) {
						multimediaspace_ajoutrepertoireenr($donnees,$erreurs);
					}
					$idx = 'gestion';
					multimediaspace_titre('Management');
					multimediaspace_gestion($donnees, $erreurs);
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
				} else {
					//Des champs n'ont pas �t� renseign�s
					$idx = 'gestion';
					multimediaspace_titre('Management');
					multimediaspace_gestion($donnees, $erreurs);
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
				}
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		}
		break;


	case 'suppressionrepertoire':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				$champs = array('idrepertoire');//champs � r�cup�rer
				$donnees = multimediaspace_recuperedonnees('get',$champs);
				$champsnv = array('idrepertoire'); //champs obligatoires (non vides)
				if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
					multimediaspace_suppressionrepertoire($donnees, $erreurs);
					multimediaspace_titre('Deletion of a folder');
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
					multimediaspace_onglet('suppressionrepertoire','Deletion of a folder','user&idx=suppressionrepertoire&idrepertoire='.$donnees['idrepertoire']);
					break;
				} else {
					//Des champs n'ont pas �t� renseign�s
					$idx = 'gestion';
					multimediaspace_titre('Management');
					multimediaspace_gestion($donnees, $erreurs);
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
				}				
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
		}
		break;


	case 'suppressionrepertoireenr':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				$champs = array('idrepertoire', 'antif5');//champs � r�cup�rer
				$donnees = multimediaspace_recuperedonnees('post',$champs);
				$champsnv = array('idrepertoire', 'antif5'); //champs obligatoires (non vides)
				$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
				$repertoire->charge();
				if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
					//V�rifie le formulaire pour emp�cher les doublons avec F5
					$antif5 = multimediaspace_antif5(MULTIMEDIASPACE_ANTIF5, $donnees['antif5']);
					if ($antif5) {
						multimediaspace_suppressionrepertoireenr($donnees,$erreurs);
					}
				}
				//Des champs n'ont pas �t� renseign�s
				$idx = 'gestion';
				multimediaspace_titre('Management');
				multimediaspace_gestion($donnees, $erreurs);
				$espacemultimedia = new Multimediaspace_EspaceMultimedia();
				if ($espacemultimedia->adroitslecture()) {
					$repertoireparent = $espacemultimedia->repertoireparent();
					multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
				}
				multimediaspace_onglet('gestion','Management','user&idx=gestion');
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
		}
		break;


	case 'editionrepertoire':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				$champs = array('idrepertoire');//champs � r�cup�rer
				$donnees = multimediaspace_recuperedonnees('get',$champs);
				$champsnv = array('idrepertoire'); //champs obligatoires (non vides)
				$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
				$repertoire->charge();
				if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
					multimediaspace_titre('Edition of a folder');
					multimediaspace_editionrepertoire($donnees,$erreurs);
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
					multimediaspace_onglet('editionrepertoire','Edition of a folder','user&idx=editionrepertoire&idrepertoire='.$donnees['idrepertoire']);
				} else {
					//Des champs n'ont pas �t� renseign�s
					$idx = 'gestion';
					multimediaspace_titre('Management');
					multimediaspace_gestion($donnees, $erreurs);
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
				}
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
		}
		break;


	case 'editionrepertoireenr':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				$champs = array('idrepertoire', 'antif5', 'nom', 'heritedesdroits');//champs � r�cup�rer
				$donnees = multimediaspace_recuperedonnees('post',$champs);
				$champsnv = array('idrepertoire', 'antif5', 'nom', 'heritedesdroits'); //champs obligatoires (non vides)
				$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
				$repertoire->charge();
				if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
					//V�rifie le formulaire pour emp�cher les doublons avec F5
					$antif5 = multimediaspace_antif5(MULTIMEDIASPACE_ANTIF5, $donnees['antif5']);
					if ($antif5) {
						multimediaspace_editionrepertoireenr($donnees,$erreurs);
					}
				}
				//Des champs n'ont pas �t� renseign�s
				$idx = 'gestion';
				multimediaspace_titre('Management');
				multimediaspace_gestion($donnees, $erreurs);
				$espacemultimedia = new Multimediaspace_EspaceMultimedia();
				if ($espacemultimedia->adroitslecture()) {
					$repertoireparent = $espacemultimedia->repertoireparent();
					multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
				}
				multimediaspace_onglet('gestion','Management','user&idx=gestion');
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
		}
		break;


	case 'droitsaccesrepertoire':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				$champs = array('idrepertoire');//champs � r�cup�rer
				$donnees = multimediaspace_recuperedonnees('get',$champs);
				$champsnv = array('idrepertoire'); //champs obligatoires (non vides)
				$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
				$repertoire->charge();
				if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
					multimediaspace_titre('Access rights of a folder');
					multimediaspace_droitsaccesrepertoire($donnees,$erreurs);
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
					multimediaspace_onglet('droitsaccesrepertoire','Access rights of a folder','user&idx=droitsaccesrepertoire&idrepertoire='.$donnees['idrepertoire']);
				} else {
					//Des champs n'ont pas �t� renseign�s
					$idx = 'gestion';
					multimediaspace_titre('Management');
					multimediaspace_gestion($donnees, $erreurs);
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
				}
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
		}
		break;


	case 'droitsaccesrepertoireenr':
		multimediaspace_enregistreACL();
		$erreurs[] = multimediaspace_traduire('Save done');
		$idx = 'gestion';
		multimediaspace_titre('Management');
		multimediaspace_gestion($donnees, $erreurs);
		$espacemultimedia = new Multimediaspace_EspaceMultimedia();
		if ($espacemultimedia->adroitslecture()) {
			$repertoireparent = $espacemultimedia->repertoireparent();
			multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
		}
		multimediaspace_onglet('gestion','Management','user&idx=gestion');
		break;


	case 'listevideosgestion':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				$champs = array('idrepertoire');//champs � r�cup�rer
				$donnees = multimediaspace_recuperedonnees('get',$champs);
				$champsnv = array('idrepertoire'); //champs obligatoires (non vides)
				$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
				$repertoire->charge();
				if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
					multimediaspace_titre('List of the videos');
					multimediaspace_listevideosgestion($donnees,$erreurs);
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
					multimediaspace_onglet('listevideosgestion','List of the videos','user&idx=listevideosgestion&idrepertoire='.$donnees['idrepertoire']);
				} else {
					//Des champs n'ont pas �t� renseign�s
					$idx = 'gestion';
					multimediaspace_titre('Management');
					multimediaspace_gestion($donnees, $erreurs);
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
				}
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
		}
		break;

		

	case 'ajoutvideogestion':

		if (!$estauthentifie) {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
			break;
		}
		//V�rifie que l'utilisateur est gestionnaire
		if (!multimediaspace_estgestionnaire()) {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			break;
		}

		//V�rification des donn�es
		$champs = array('idrepertoire');//champs � r�cup�rer
		$donnees = multimediaspace_recuperedonnees('get',$champs);
		$champsnv = array('idrepertoire'); //champs obligatoires (non vides)
		$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
		$repertoire->charge();

		if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
			multimediaspace_titre('Add of a video');
			multimediaspace_ajoutvideogestion($donnees, $erreurs);
			$espacemultimedia = new Multimediaspace_EspaceMultimedia();
			if ($espacemultimedia->adroitslecture()) {
				$repertoireparent = $espacemultimedia->repertoireparent();
				multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
			}
			multimediaspace_onglet('gestion','Management','user&idx=gestion');
			multimediaspace_onglet('listevideosgestion','List of the videos','user&idx=listevideosgestion&idrepertoire='.$donnees['idrepertoire']);
			multimediaspace_onglet('ajoutvideogestion','Add of a video','user&idx=ajoutvideogestion&idrepertoire='.$donnees['idrepertoire']);
		} else {
			//Des champs n'ont pas �t� renseign�s
			$idx = 'gestion';
			multimediaspace_titre('Management');
			multimediaspace_gestion($donnees, $erreurs);
			$espacemultimedia = new Multimediaspace_EspaceMultimedia();
			if ($espacemultimedia->adroitslecture()) {
				$repertoireparent = $espacemultimedia->repertoireparent();
				multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
			}
			multimediaspace_onglet('gestion','Management','user&idx=gestion');
		}
		break;

		
	case 'ajoutvideogestion2':
		if (!bab_requireCredential(multimediaspace_traduire('You must be authenticated to access this page.'))) {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
			break;
		}
		//V�rifie que l'utilisateur est gestionnaire
		if (!multimediaspace_estgestionnaire()) {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			break;
		}

		//V�rification des donn�es
		$champs = array('idrepertoire', 'type', 'template');//champs � r�cup�rer
		$donnees = multimediaspace_recuperedonnees('get',$champs);
		$champsnv = array('idrepertoire', 'type'); //champs obligatoires (non vides)
		$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
		$repertoire->charge();

		if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
			multimediaspace_titre('Add of a video');
			multimediaspace_ajoutvideogestion2($donnees, $erreurs);
			$espacemultimedia = new Multimediaspace_EspaceMultimedia();
			if ($espacemultimedia->adroitslecture()) {
				$repertoireparent = $espacemultimedia->repertoireparent();
				multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
			}
			multimediaspace_onglet('gestion','Management','user&idx=gestion');
			multimediaspace_onglet('listevideosgestion','List of the videos','user&idx=listevideosgestion&idrepertoire='.$donnees['idrepertoire']);
			multimediaspace_onglet('ajoutvideogestion2','Add of a video','user&idx=ajoutvideogestion&idrepertoire='.$donnees['idrepertoire']);
		} else {
			//Des champs n'ont pas �t� renseign�s
			$idx = 'gestion';
			multimediaspace_titre('Management');
			multimediaspace_gestion($donnees, $erreurs);
			$espacemultimedia = new Multimediaspace_EspaceMultimedia();
			if ($espacemultimedia->adroitslecture()) {
				$repertoireparent = $espacemultimedia->repertoireparent();
				multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
			}
			multimediaspace_onglet('gestion','Management','user&idx=gestion');
		}
		break;


	case 'ajoutvideogestion2enr':
		if (!bab_requireCredential(multimediaspace_traduire('You must be authenticated to access this page.'))) {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
			break;
		}
		//V�rifie que l'utilisateur est gestionnaire
		if (!multimediaspace_estgestionnaire()) {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			break;
		}

		//V�rification des donn�es
		$champs = array('idrepertoire', 'antif5', 'nom', 'description', 'largeur', 'hauteur', 'valeur', 'type', 'template', 'jouerautomatiquement');//champs � r�cup�rer
		$donnees = multimediaspace_recuperedonnees('post',$champs);
		$champsnv = array('idrepertoire', 'antif5', 'nom', 'type'); //champs obligatoires (non vides)
		$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
		$repertoire->charge();
		if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
			//V�rifie le formulaire pour emp�cher les doublons avec F5
			$antif5 = multimediaspace_antif5(MULTIMEDIASPACE_ANTIF5, $donnees['antif5']);
			if ($antif5) {
				multimediaspace_ajoutvideogestionenr($donnees,$erreurs);
			}
		}
		//Des champs n'ont pas �t� renseign�s
		$idx = 'listevideosgestion';
		multimediaspace_titre('List of the videos');
		multimediaspace_listevideosgestion($donnees,$erreurs);
		$espacemultimedia = new Multimediaspace_EspaceMultimedia();
		if ($espacemultimedia->adroitslecture()) {
			$repertoireparent = $espacemultimedia->repertoireparent();
			multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
		}
		multimediaspace_onglet('gestion','Management','user&idx=gestion');
		multimediaspace_onglet('listevideosgestion','List of the videos','user&idx=listevideosgestion&idrepertoire='.$donnees['idrepertoire']);

		break;


	case 'suppressionvideo':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				$champs = array('idvideo');//champs � r�cup�rer
				$donnees = multimediaspace_recuperedonnees('get',$champs);
				$champsnv = array('idvideo'); //champs obligatoires (non vides)
				if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
					multimediaspace_suppressionvideo($donnees, $erreurs);
					multimediaspace_titre('Deletion of a video');
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
					$video = new Multimediaspace_Video($donnees['idvideo']);
					$video->charge();
					multimediaspace_onglet('listevideosgestion','List of the videos','user&idx=listevideosgestion&idrepertoire='.$video->idrepertoire);
					multimediaspace_onglet('suppressionvideo','Deletion of a video','user&idx=suppressionvideo&idvideo='.$donnees['idvideo']);
					break;
				} else {
					//Des champs n'ont pas �t� renseign�s
					$idx = 'gestion';
					multimediaspace_titre('Management');
					multimediaspace_gestion($donnees, $erreurs);
					$espacemultimedia = new Multimediaspace_EspaceMultimedia();
					if ($espacemultimedia->adroitslecture()) {
						$repertoireparent = $espacemultimedia->repertoireparent();
						multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
					}
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
				}				
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
		}
		break;


	case 'suppressionvideoenr':
		if ($estauthentifie) {
			//V�rifie que l'utilisateur est gestionnaire
			if (multimediaspace_estgestionnaire()) {
				//V�rification des donn�es
				$champs = array('idvideo', 'antif5');//champs � r�cup�rer
				$donnees = multimediaspace_recuperedonnees('post',$champs);
				$champsnv = array('idvideo', 'antif5'); //champs obligatoires (non vides)
				$video = new Multimediaspace_Video($donnees['idvideo']);
				$video->charge();
				if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
					//V�rifie le formulaire pour emp�cher les doublons avec F5
					$antif5 = multimediaspace_antif5(MULTIMEDIASPACE_ANTIF5, $donnees['antif5']);
					if ($antif5) {
						multimediaspace_suppressionvideoenr($donnees,$erreurs);
					}
				}
				//Des champs n'ont pas �t� renseign�s
				$idx = 'listevideosgestion';
				multimediaspace_titre('List of the videos');
				$donnees['idrepertoire'] = $video->idrepertoire;
				multimediaspace_listevideosgestion($donnees,$erreurs);
				$espacemultimedia = new Multimediaspace_EspaceMultimedia();
				if ($espacemultimedia->adroitslecture()) {
					$repertoireparent = $espacemultimedia->repertoireparent();
					multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
				}
				multimediaspace_onglet('gestion','Management','user&idx=gestion');
				multimediaspace_onglet('listevideosgestion','List of the videos','user&idx=listevideosgestion&idrepertoire='.$video->idrepertoire);
			} else {
				$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
		}
		break;



	case 'editionvideo':

		if (!bab_requireCredential(multimediaspace_traduire('You must be authenticated to access this page.'))) {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
			break;
		}
		//V�rifie que l'utilisateur est gestionnaire
		if (!multimediaspace_estgestionnaire()) {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			break;
		}

		$champs = array('idvideo');//champs � r�cup�rer
		$donnees = multimediaspace_recuperedonnees('get', $champs);
		$champsnv = array('idvideo'); //champs obligatoires (non vides)
		if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
			multimediaspace_titre('Edition of a video');
			multimediaspace_editionvideo($donnees, $erreurs);
			$video = new Multimediaspace_Video($donnees['idvideo']);
			$video->charge();
			$espacemultimedia = new Multimediaspace_EspaceMultimedia();
			if ($espacemultimedia->adroitslecture()) {
				$repertoireparent = $espacemultimedia->repertoireparent();
				multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$video->idrepertoire);
			}
			multimediaspace_onglet('gestion','Management','user&idx=gestion');
			multimediaspace_onglet('listevideosgestion','List of the videos','user&idx=listevideosgestion&idrepertoire='.$video->idrepertoire);
			multimediaspace_onglet('editionvideo','Edition of a video','user&idx=editionvideo&idvideo='.$video->id);
		} else {
			//Des champs n'ont pas �t� renseign�s
			$idx = 'gestion';
			multimediaspace_titre('Management');
			multimediaspace_gestion($donnees, $erreurs);
			$espacemultimedia = new Multimediaspace_EspaceMultimedia();
			if ($espacemultimedia->adroitslecture()) {
				$repertoireparent = $espacemultimedia->repertoireparent();
				multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
			}
			multimediaspace_onglet('gestion','Management','user&idx=gestion');
		}
		break;



	case 'editionvideoenr':
		
		if (!bab_requireCredential(multimediaspace_traduire('You must be authenticated to access this page.'))) {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not authenticated");
			break;
		}
		//V�rifie que l'utilisateur est gestionnaire
		if (!multimediaspace_estgestionnaire()) {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not manager");
			break;
		}
		//V�rification des donn�es
		$champs = array('idvideo', 'antif5', 'nom', 'description', 'largeur', 'hauteur', 'valeur', 'supprimerimage', 'jouerautomatiquement');//champs � r�cup�rer
		$donnees = multimediaspace_recuperedonnees('post', $champs);
		$champsnv = array('idvideo', 'antif5', 'nom'); //champs obligatoires (non vides)
		$video = new Multimediaspace_Video($donnees['idvideo']);
		$video->charge();
		if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
			//V�rifie le formulaire pour emp�cher les doublons avec F5
			$antif5 = multimediaspace_antif5(MULTIMEDIASPACE_ANTIF5, $donnees['antif5']);
			if ($antif5) {
				multimediaspace_editionvideoenr($donnees, $erreurs);
			}
		}
		//Des champs n'ont pas �t� renseign�s
		$idx = 'listevideosgestion';
		multimediaspace_titre('List of the videos');
		$video = new Multimediaspace_Video($donnees['idvideo']);
		$video->charge();
		$donnees['idrepertoire'] = $video->idrepertoire;
		multimediaspace_listevideosgestion($donnees, $erreurs);
		$espacemultimedia = new Multimediaspace_EspaceMultimedia();
		if ($espacemultimedia->adroitslecture()) {
			$repertoireparent = $espacemultimedia->repertoireparent();
			multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
		}
		multimediaspace_onglet('gestion','Management','user&idx=gestion');
		multimediaspace_onglet('listevideosgestion','List of the videos','user&idx=listevideosgestion&idrepertoire='.$video->idrepertoire);
		break;


	case 'telechargevideo':
		$trouve = false;
		//V�rification des donn�es
		$champs = array('idvideo');//champs � r�cup�rer
		$donnees = multimediaspace_recuperedonnees('get',$champs);
		$champsnv = array('idvideo'); //champs obligatoires (non vides)
		if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
			$trouve = true;
			/* V�rifie si l'utilisateur a des droits de lecture dans le r�pertoire ou est gestionnaire */
			$video = new Multimediaspace_Video($donnees['idvideo']);
			$video->charge();
			$repertoire = $video->repertoire();
			if ($repertoire->adroitslecture(true) || multimediaspace_estgestionnaire()) {
				multimediaspace_telechargevideo($donnees, $erreurs);
			}
		}
		break;


	case 'voirvideo':
		$trouve = false;
		//V�rification des donn�es
		$champs = array('idvideo');//champs � r�cup�rer
		$donnees = multimediaspace_recuperedonnees('get',$champs);
		$champsnv = array('idvideo'); //champs obligatoires (non vides)
		if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
			$trouve = true;
			/* V�rifie si l'utilisateur a des droits de lecture dans le r�pertoire */
			$video = new Multimediaspace_Video($donnees['idvideo']);
			$video->charge();
			$repertoire = $video->repertoire();
			if ($repertoire->adroitslecture(true)) {
				$trouve = true;
				multimediaspace_titre('Multimedia space');
				multimediaspace_voirvideo($donnees, $erreurs);
				$espacemultimedia = new Multimediaspace_EspaceMultimedia();
				if ($espacemultimedia->adroitslecture()) {
					$repertoireparent = $espacemultimedia->repertoireparent();
					$idx = 'accesrepertoire';
					multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
				}
				if (multimediaspace_estgestionnaire()) {
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
				}
			}
		}			
		if (!$trouve) {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page");
		}
		break;


	case 'voirminiature':
		$trouve = false;
		//V�rification des donn�es
		$champs = array('idvideo', 'largeurvoulue', 'hauteurvoulue');//champs � r�cup�rer
		$donnees = multimediaspace_recuperedonnees('get',$champs);
		$champsnv = array('idvideo'); //champs obligatoires (non vides)
		if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
			$video = new Multimediaspace_Video($donnees['idvideo']);
			$video->charge();
			if ($video->adroitslecture()) {
				multimediaspace_voirminiature($donnees, $erreurs);
			}
		}
		break;


	case 'accesrepertoire':
		$trouve = false;
		//V�rification des donn�es
		$champs = array('idrepertoire');//champs � r�cup�rer
		$donnees = multimediaspace_recuperedonnees('get',$champs);
		$champsnv = array('idrepertoire'); //champs obligatoires (non vides)
		if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
			$trouve = true;
			/* V�rifie si l'utilisateur a des droits de lecture dans le r�pertoire */
			$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
			$repertoire->charge();
			if ($repertoire->adroitslecture(true)) {
				$trouve = true;
				multimediaspace_titre('Multimedia space');
				multimediaspace_accesrepertoire($donnees, $erreurs);
				$espacemultimedia = new Multimediaspace_EspaceMultimedia();
				if ($espacemultimedia->adroitslecture()) {
					$repertoireparent = $espacemultimedia->repertoireparent();
					multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
				}
				if (multimediaspace_estgestionnaire()) {
					multimediaspace_onglet('gestion','Management','user&idx=gestion');
				}
			}
		}			
		if (!$trouve) {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page");
		}
		break;


	case 'editeurwysiwyglisterepertoires':
		/* V�rifie si l'utilisateur a des droits de lecture dans des r�pertoires */
		$espacemultimedia = new Multimediaspace_EspaceMultimedia();
		if ($espacemultimedia->adroitslecture()) {
			multimediaspace_editeurwysiwyglisterepertoires($erreurs);
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page");
		}
		break;


	case 'editeurwysiwyglistevideos':
		/* V�rifie si l'utilisateur a des droits de lecture dans des r�pertoires */
		$espacemultimedia = new Multimediaspace_EspaceMultimedia();
		if ($espacemultimedia->adroitslecture()) {
			//V�rification des donn�es
			$champs = array('idrepertoire');//champs � r�cup�rer
			$donnees = multimediaspace_recuperedonnees('get',$champs);
			$champsnv = array('idrepertoire'); //champs obligatoires (non vides)
			if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
				$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
				$repertoire->charge();
				if ($repertoire->adroitslecture(true)) {
					multimediaspace_editeurwysiwyglistevideos($donnees, $erreurs);
				} else {
					$erreurs[] = multimediaspace_traduire("You don't have access to this page");
				}
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page");
		}
		break;


	case 'editeurwysiwygvideo':
		/* V�rifie si l'utilisateur a des droits de lecture dans des r�pertoires */
		$espacemultimedia = new Multimediaspace_EspaceMultimedia();
		if ($espacemultimedia->adroitslecture()) {
			//V�rification des donn�es
			$champs = array('idvideo');//champs � r�cup�rer
			$donnees = multimediaspace_recuperedonnees('get',$champs);
			$champsnv = array('idvideo'); //champs obligatoires (non vides)
			if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
				$video = new Multimediaspace_Video($donnees['idvideo']);
				$video->charge();
				$repertoire = new Multimediaspace_Repertoire($video->idrepertoire);
				$repertoire->charge();
				if ($repertoire->adroitslecture(true)) {
					multimediaspace_editeurwysiwygvideo($donnees, $erreurs);
				} else {
					$erreurs[] = multimediaspace_traduire("You don't have access to this page");
				}
			}
		} else {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page");
		}
		break;


	case 'utilisateur':
	default:
		$trouve = false;
		/* V�rifie si l'utilisateur a des droits de lecture dans des r�pertoires */
		$espacemultimedia = new Multimediaspace_EspaceMultimedia();
		if ($espacemultimedia->adroitslecture()) {
			$trouve = true;
			$idx = 'accesrepertoire';
			multimediaspace_titre('Multimedia space');
			multimediaspace_accueilespacemultimedia($erreurs);
			$repertoireparent = $espacemultimedia->repertoireparent();
			multimediaspace_onglet('accesrepertoire','Videos','user&idx=accesrepertoire&idrepertoire='.$repertoireparent->id);
		}
		/* V�rifie si l'utilisateur est gestionnaire */
		if (multimediaspace_estgestionnaire()) {
			if (!$trouve) {
				$idx = 'gestion';
				$trouve = true;
				multimediaspace_titre('Management');
				multimediaspace_gestion($donnees, $erreurs);
			}
			multimediaspace_onglet('gestion','Management','user&idx=gestion');
		}
		if (!$trouve) {
			$erreurs[] = multimediaspace_traduire("You don't have access to this page");
		}
		break;	
}

$babBody->setCurrentItemMenu($idx);
multimediaspace_erreurs($erreurs);

