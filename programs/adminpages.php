<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2008 by CANTICO ( http://www.cantico.fr )              *
 ************************************************************************/
include_once 'base.php';

require_once dirname(__FILE__) . '/fonctionsgen.php';
require_once dirname(__FILE__) . '/fonctions.php';

/* Configuration du module : �metteur des mails... */
function multimediaspace_configuration()
{
	global $babBody;

	class multimediaspace_configuration
	{
		//Variables

		public function __construct()
		{
			$this->aide = multimediaspace_traduire("This field must be informed");
			$this->tlien = multimediaspace_traduire("Point of entry to the application");
			$this->texpediteur_nom = multimediaspace_traduire("Name of the transmitter of the notifications");
			$this->texpediteur_mail = multimediaspace_traduire("Address email of the transmitter of the notifications");
			$this->tlecteur_video_par_defaut = multimediaspace_traduire("Video player");
			$this->enregistrer = multimediaspace_traduire("Save");
			$this->taideflvplayer = multimediaspace_traduire("FLV Player maxi contains functionality as choice of the volume, display of the time of reading and mode full screen. However he can sometimes be incompatible with the navigator Internet Explorer 6. Prefer in that case, FLV Player mini.");
			//R�cup�ration de la configuration
			$resultats = multimediaspace_configurationmodule();
			$this->lien = $resultats['lien'];
			$this->expediteur_nom = $resultats['expediteur_nom'];
			$this->expediteur_mail = $resultats['expediteur_mail'];
			$this->lecteur_video_par_defaut = $resultats['lecteur_video_par_defaut'];
			$this->compteur = 0;
			$this->selection = false;
			$this->idtypesliens = array(1,2,3);
			$this->nomstypesliens = array(multimediaspace_traduire('No point of entry'),multimediaspace_traduire('In User section'),multimediaspace_traduire('In specific section'));
		}
		
		public function listetypesliens()
		{
			if ($this->compteur > count($this->idtypesliens) - 1) {
				return false;
			}
			$this->idtypelien = $this->idtypesliens[$this->compteur];
			$this->nomtypelien = $this->nomstypesliens[$this->compteur];
			if ($this->lien == $this->idtypelien) {
				$this->selection = true;
			} else {
				$this->selection = false;
			}
			$this->compteur++;
			return true;
		}
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_configuration();
	multimediaspace_template($a, 'adminpages.html', 'configuration');
}


/* Enregistrement de la configuration du module */
function multimediaspace_configurationenr($donnees, &$erreurs)
{
	/* Mise � jour du plan du site (cache) du noyau Ovidentia afin d'afficher le lien dans les sections */
	if (class_exists('bab_sitemap')) {
		bab_sitemap::clearAll();
	}
	
	multimediaspace_echappesql($donnees);
	$lien = $donnees['lien'];
	$expediteur_nom = $donnees['expediteur_nom'];
	$expediteur_mail = $donnees['expediteur_mail'];
	$lecteur_video_par_defaut = $donnees['lecteur_video_par_defaut'];
	$requete = "update `".MULTIMEDIASPACE_CONFIGURATION."` set `lien`='$lien',`expediteur_nom`='$expediteur_nom',`expediteur_mail`='$expediteur_mail',`lecteur_video_par_defaut`='$lecteur_video_par_defaut';
							";
	$idrequete = 0;
	multimediaspace_sql($requete, $erreurs, $idrequete);
	if (count($erreurs) == 0) {
		$erreurs[] = "Save done";
	}
}

//Formulaire pour d�finir les gestionnaires du module
function multimediaspace_droitsgest(&$erreurs)
{
	$filtres = array(0,0,1,0,1,array());
	global $babAddonTarget;
	$tg = $babAddonTarget.'/admin';
	$idx = 'droitsgestenr';
	multimediaspace_acl($tg, $idx, 1, MULTIMEDIASPACE_DROITSGEST, 'Who are the administrators of the module?', $filtres);
}



function multimediaspace_videotemplates()
{

	global $babBody;
	
	class multimediaspace_videotemplates 
	{
		public $t_template;
		public $t_description;
		public $t_edit;
		public $t_delete;
		public $t_add_template;
		
		public $template_name;
		public $template_description;
		public $edit_url;
		public $delete_url;
		public $edit_title;
		public $delete_title;
		public $odd = true;
		
		public $templates;
		
		
		
		public function __construct()
		{
			$this->t_template = multimediaspace_traduire('Video template');
			$this->t_description = multimediaspace_traduire('Description');
			$this->t_edit = multimediaspace_traduire('Edit');
			$this->t_delete = multimediaspace_traduire('Delete');
			$this->t_add_template = multimediaspace_traduire('Add a video template');
			
			$this->add_video_url = $GLOBALS['babAddonUrl'] . 'admin&idx=videotemplateedit';
			
			$templates = multimediaspace_getVideoTemplates();
			$this->templates = array();
			foreach ($templates as $templateName) {
				
				$template = multimediaspace_getVideoTemplate($templateName);
				$this->templates[] = $template;
			}
		}

		
		public function listTemplates()
		{
			list(,$template) = each($this->templates);
			if (!$template) {
				reset($this->templates);
				return false;
			}
			$this->odd = !$this->odd;
			$this->template_name = bab_toHtml($template['name']);
			$this->template_description = bab_toHtml($template['description']);
			$this->edit_url = bab_toHtml($GLOBALS['babAddonUrl'] . 'admin&idx=videotemplateedit&template=' . $template['name']);
			$this->delete_url = $GLOBALS['babAddonUrl'] . 'admin&idx=videotemplatedelete&template=' . $template['name'];
			$this->edit_title = bab_toHtml(sprintf(multimediaspace_traduire('Edit template %s'), $this->template_name));
			$this->delete_title = bab_toHtml(sprintf(multimediaspace_traduire('Delete template %s'), $this->template_name));
			return true;
		}

	}

	$videotemplates = new multimediaspace_videotemplates();

	$html = bab_printTemplate($videotemplates, $GLOBALS['babAddonHtmlPath'].'adminpages.html', 'videotemplates');
	$babBody->addStyleSheet('toolbar.css');
	$babBody->babecho($html);
}







function multimediaspace_videotemplateedit($template)
{

	global $babBody;

	class multimediaspace_videotemplateedit
	{
		public $t_template;
		public $t_edit;
		public $t_delete;

		public $template;

		public function __construct($template = null)
		{
			if (!isset($template)) {
				$template = array('name' => '', 'description' => '', 'body' => '', 'parameters' => '');
			} elseif (!is_array($template)) {
				$template = multimediaspace_getVideoTemplate($template);
			}
			$this->t_template_name = multimediaspace_traduire('Template name');
			$this->t_template_description = multimediaspace_traduire('Template description');
			$this->t_template_body = multimediaspace_traduire('Template body');
			$this->t_template_parameters = multimediaspace_traduire('Template parameters');
			
			$this->t_save = multimediaspace_traduire('Save');
			$this->t_cancel = multimediaspace_traduire('Cancel');
			
			$this->cancel_url = $GLOBALS['babAddonUrl'] . 'admin&idx=videotemplates';
			
			$this->template = $template;
		}
	
	}

	$videotemplateedit = new multimediaspace_videotemplateedit($template);

	$html = bab_printTemplate($videotemplateedit, $GLOBALS['babAddonHtmlPath'].'adminpages.html', 'videotemplateedit');
	$babBody->babecho($html);
}



function multimediaspace_videotemplatesave($template)
{
	multimediaspace_saveVideoTemplate($template);
}

