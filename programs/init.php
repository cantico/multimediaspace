<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2008 by CANTICO ( http://www.cantico.fr )              *
 ************************************************************************/
include_once 'base.php';


//Chargement des fonctions
global $babAddonPhpPath;
global $babInstallPath;
require_once dirname(__FILE__).'/fonctionsgen.php';
require_once dirname(__FILE__).'/fonctions.php';
require_once $babInstallPath.'utilit/devtools.php'; //section sp�cifique

//Lien en section Administrateur
function multimediaspace_getAdminSectionMenus(&$url, &$text) {
    static $nbMenus=0;
    global $BAB_SESS_USERID;
    global $babAddonUrl;
    if (!$nbMenus && multimediaspace_estauthentifie()) {
        $url = $babAddonUrl."admin&idx=configuration"; //index.php?tg=addon/multimediaspace/admin
        $text = multimediaspace_traduire("Multimedia space");
        $nbMenus++;
        return true;
    }
    return false;
}

//Lien en section Utilisateur
function multimediaspace_getUserSectionMenus(&$url, &$text) {
    static $nbMenus=0;
    global $babAddonUrl;

    static $nbMenus=0;
    global $babAddonUrl;
    if (!$nbMenus) {
        /* Commence par v�rifier la configuration pour savoir si le lien est plac� en section Utilisateur */
        $res = multimediaspace_configurationmodule();
        if ($res['lien'] != 2) {
            return false;
        }

        /* V�rifie si l'utilisateur est gestionnaire ou a des droits de lecture sur des r�pertoires
         */
        $espacemultimedia = new Multimediaspace_EspaceMultimedia();
        if (multimediaspace_estgestionnaire() || $espacemultimedia->adroitslecture()) {
            $url = $babAddonUrl."user&idx=utilisateur"; //index.php?tg=addon/multimediaspace/user
            $text = multimediaspace_traduire("Multimedia space");
            $nbMenus++;
            return true;
        }
    }
    return false;
}

function multimediaspace_onUserCreate( $id ) {
    return false;
}

function multimediaspace_onUserDelete( $id ) {
    return false;
}

function multimediaspace_onGroupCreate( $id ) {
    return false;
}

function multimediaspace_onGroupDelete( $id ) {
    return false;
}

//Section sp�cifique du module
function multimediaspace_onSectionCreate(&$title, &$content) {
    static $nbMenus=0;
    global $babAddonUrl;
    if (!$nbMenus) {
        /* Commence par v�rifier la configuration pour savoir si le lien est plac� en section sp�cifique */
        $res = multimediaspace_configurationmodule();
        if ($res['lien'] != 3) {
            return false;
        }

        /* V�rifie si l'utilisateur est gestionnaire ou a des droits de lecture sur des r�pertoires
         */
        $espacemultimedia = new Multimediaspace_EspaceMultimedia();
        if (multimediaspace_estgestionnaire() || $espacemultimedia->adroitslecture()) {
            $title = multimediaspace_traduire("Multimedia space");
            $url = $babAddonUrl."user&idx=utilisateur"; //index.php?tg=addon/multimediaspace/user
            $content = "<ul><li><a href=\"$url\">$title</a></li></ul>";
            $nbMenus++;
            return true;
        }
    }
    return false;
}

//Installation ou mise � jour du module
function multimediaspace_upgrade($version_base,$version_ini)
{
    global $babBody;
    global $babUploadPath;
    global $babInstallPath;
    global $babAddonPhpPath;
    global $babAddonHtmlPath;
    $erreurs = array();

    /* Ajoute l'�couteur sur l'ouverture de la popup des fonctions d'Ovidentia dans l'�diteur wysiwyg */
    include_once $babInstallPath.'utilit/eventincl.php';
    bab_addEventListener(
        'bab_eventEditorFunctions',
        'multimediaspace_editeurwysiwyg_lien',
        $babAddonHtmlPath.'fonctions.php',
        'multimediaspace'
    );

    //Cr�ation du r�pertoire dans le r�pertoire d'upload s'il n'existe pas
    $repertoire=$babUploadPath."/addons/";
    if(!multimediaspace_existerepertoire($repertoire)) {
        multimediaspace_creationrepertoire($repertoire);
    }
    $repertoiremod=$repertoire."multimediaspace";
    if(!multimediaspace_existerepertoire($repertoiremod)) {
        multimediaspace_creationrepertoire($repertoiremod);
    }

    //Cr�ation des tables de donn�es
    //Table multimediaspace_droitsgest
    $requete = "SHOW TABLES LIKE '".MULTIMEDIASPACE_DROITSGEST."'";
    $idrequete = 0;
    $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    if (count($resultats) == 0) {
        $requete = "CREATE TABLE `".MULTIMEDIASPACE_DROITSGEST."` (
                      `id` int(11) unsigned NOT NULL auto_increment,
                      `id_object` int(11) unsigned NOT NULL default '0',
                      `id_group` int(11) unsigned NOT NULL default '0',
                      PRIMARY KEY  (`id`),
                      KEY `id_object` (`id_object`),
                      KEY `id_group` (`id_group`)
                    )";
        $idrequete = 0;
        $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    }

    //Table multimediaspace_configuration
    $requete = "SHOW TABLES LIKE '".MULTIMEDIASPACE_CONFIGURATION."'";
    $idrequete = 0;
    $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    if (count($resultats) == 0) {
        $requete = "CREATE TABLE `".MULTIMEDIASPACE_CONFIGURATION."` (
                      `id` int(11) unsigned NOT NULL auto_increment,
                      `lien` int(11) unsigned NOT NULL,
                      `expediteur_nom` varchar(255) NOT NULL,
                      `expediteur_mail` varchar(255) NOT NULL,
                      `lecteur_video_par_defaut` varchar(255) NOT NULL,
                      PRIMARY KEY  (`id`)
                    )";
        $idrequete = 0;
        $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    }
    //Ajout du champ lecteur_video_par_defaut s'il n'existe pas : version 1.7 du module
    $requete = "SHOW COLUMNS from `".MULTIMEDIASPACE_CONFIGURATION."` LIKE 'lecteur_video_par_defaut'";
    $idrequete = 0;
    $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    if (count($resultats) == 0) {
        /* Ajoute la colonne dans la table */
        $requete = "ALTER TABLE `".MULTIMEDIASPACE_CONFIGURATION."` ADD `lecteur_video_par_defaut` varchar(255) NOT NULL";
        $idrequete = 0;
        $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
        /* Initialise les valeurs de la colonne � 1 : les vid�os ne sont pas jou�es automatiquement */
        $requete = "UPDATE `".MULTIMEDIASPACE_CONFIGURATION."` SET `lecteur_video_par_defaut` = 'flvplayer_maxi'";
        $idrequete = 0;
        $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    }
    //Ajout de la configuration par d�faut si inexistante
    $requete = "select `id` from ".MULTIMEDIASPACE_CONFIGURATION;
    $resultats = multimediaspace_sql($requete, $erreurs = array(), $idrequete='');
    if (count($resultats) == 0) {
        $requete = "insert into `".MULTIMEDIASPACE_CONFIGURATION."` (  `lien` , `expediteur_nom` , `expediteur_mail`, `lecteur_video_par_defaut` )
                              values ( '1' , 'admin' , 'admin@mysite.com', 'flvplayer_maxi' )
                            ";
        $resultats = multimediaspace_sql($requete, $erreurs = array(), $idrequete='');
    }

    //Table multimediaspace_repertoire
    $requete = "SHOW TABLES LIKE '".MULTIMEDIASPACE_REPERTOIRE."'";
    $idrequete = 0;
    $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    if (count($resultats) == 0) {
        $requete = "CREATE TABLE `".MULTIMEDIASPACE_REPERTOIRE."` (
                      `id` int(11) unsigned NOT NULL auto_increment,
                      `nom` varchar(255) NOT NULL,
                      `bornegauche` int(11) unsigned NOT NULL,
                      `bornedroite` int(11) unsigned NOT NULL,
                      `heritedesdroits` int(11) unsigned NOT NULL,
                      `commentairesactifs` int(11) unsigned NOT NULL,
                      PRIMARY KEY  (`id`)
                    )";
        $idrequete = 0;
        $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    }
    //Ajout du r�pertoire principal n�cessaire pour la repr�sentation intervallaire
    $requete = "select `id` from ".MULTIMEDIASPACE_REPERTOIRE;
    $resultats = multimediaspace_sql($requete, $erreurs = array(), $idrequete='');
    if (count($resultats) == 0) {
        $requete = "insert into `".MULTIMEDIASPACE_REPERTOIRE."` (  `nom` , `bornegauche` , `bornedroite` , `heritedesdroits` , `commentairesactifs` )
                              values (  'Espace Multim�dia' , '1' , '2', '0', '0' )
                            ";
        $resultats = multimediaspace_sql($requete, $erreurs = array(), $idrequete='');
    }

    //Table multimediaspace_droitslecturerepertoire
    $requete = "SHOW TABLES LIKE '".MULTIMEDIASPACE_DROITSLECTUREREPERTOIRE."'";
    $idrequete = 0;
    $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    if (count($resultats) == 0) {
        $requete = "CREATE TABLE `".MULTIMEDIASPACE_DROITSLECTUREREPERTOIRE."` (
                      `id` int(11) unsigned NOT NULL auto_increment,
                      `id_object` int(11) unsigned NOT NULL default '0',
                      `id_group` int(11) unsigned NOT NULL default '0',
                      PRIMARY KEY  (`id`),
                      KEY `id_object` (`id_object`),
                      KEY `id_group` (`id_group`)
                    )";
        $idrequete = 0;
        $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    }

    //Table multimediaspace_video
    $requete = "SHOW TABLES LIKE '".MULTIMEDIASPACE_VIDEO."'";
    $idrequete = 0;
    $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    if (count($resultats) == 0) {
        $requete = "CREATE TABLE `".MULTIMEDIASPACE_VIDEO."` (
                      `id` int(11) unsigned NOT NULL auto_increment,
                      `idrepertoire` int(11) unsigned NOT NULL,
                      `idservice` int(11) unsigned NOT NULL,
                      `nom` varchar(255) NOT NULL,
                      `description` TEXT NOT NULL,
                      `date` datetime default '0000-00-00 00:00:00' NOT NULL,
                      `nomfichiervideo` TEXT NOT NULL,
                      `type` varchar(255) NOT NULL,
                      `largeur` int(11) unsigned NOT NULL,
                      `hauteur` int(11) unsigned NOT NULL,
                      `largeurlecteur` int(11) unsigned NOT NULL,
                      `hauteurlecteur` int(11) unsigned NOT NULL,
                      `nomfichierminiature` TEXT NOT NULL,
                      `typeminiature` varchar(255) NOT NULL,
                      `largeurminiature` int(11) unsigned NOT NULL,
                      `hauteurminiature` int(11) unsigned NOT NULL,
                      `nblecture` int(11) unsigned NOT NULL,
                      `auteur` int(11) unsigned NOT NULL,
                      `valeur` TEXT NOT NULL,
                      PRIMARY KEY  (`id`)
                    )";
        $idrequete = 0;
        $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    }
    //Ajout du champ jouerautomatiquement s'il n'existe pas : version 1.1 du module
    $requete = "SHOW COLUMNS from `".MULTIMEDIASPACE_VIDEO."` LIKE 'jouerautomatiquement'";
    $idrequete = 0;
    $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    if (count($resultats) == 0) {
        /* Ajoute la colonne dans la table */
        $requete = "ALTER TABLE `".MULTIMEDIASPACE_VIDEO."` ADD `jouerautomatiquement` int(11) unsigned NOT NULL";
        $idrequete = 0;
        $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
        /* Initialise les valeurs de la colonne � 1 : les vid�os ne sont pas jou�es automatiquement */
        $requete = "UPDATE `".MULTIMEDIASPACE_VIDEO."` SET `jouerautomatiquement` = '1'";
        $idrequete = 0;
        $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    }
    /* Corrige un probl�me sur l'enregistrement de la taille du lecteur : dans l'interface on renseigne la taille du lecteur
     * et c'�tait la taille de la vid�o qui �tait enregistr�e
     * Version 1.7 du  module */
    $videosTmp = multimediaspace_listevideos('', true, 'id', 'croissant', '', '', false);
    foreach ($videosTmp as $video) {
        $modifEffectuee = false;
        if ($video->largeurlecteur == 0 || $video->largeurlecteur == '') {
            $video->largeurlecteur = $video->largeur;
            $modifEffectuee = true;

        }
        if ($video->hauteurlecteur == 0 || $video->hauteurlecteur == '') {
            $video->hauteurlecteur = $video->hauteur;
            $modifEffectuee = true;
        }
        if ($modifEffectuee) {
            $video->enregistre();
        }
    }

    //Table multimediaspace_commentaire
    $requete = "SHOW TABLES LIKE '".MULTIMEDIASPACE_COMMENTAIRE."'";
    $idrequete = 0;
    $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    if (count($resultats) == 0) {
        $requete = "CREATE TABLE `".MULTIMEDIASPACE_COMMENTAIRE."` (
                      `id` int(11) unsigned NOT NULL auto_increment,
                      `idvideo` int(11) unsigned NOT NULL,
                      `auteur` int(11) unsigned NOT NULL,
                      `texte` TEXT NOT NULL,
                      `date` datetime default '0000-00-00 00:00:00' NOT NULL,
                      PRIMARY KEY  (`id`)
                    )";
        $idrequete = 0;
        $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    }

    //Table multimediaspace_antif5
    $requete = "SHOW TABLES LIKE '".MULTIMEDIASPACE_ANTIF5."'";
    $idrequete = 0;
    $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    if (count($resultats) == 0) {
        $requete = "CREATE TABLE `".MULTIMEDIASPACE_ANTIF5."` (
                      `id` int(11) unsigned NOT NULL auto_increment,
                      `valeurformulaire` varchar(255) NOT NULL,
                      `date` DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
                      PRIMARY KEY  (`id`)
                    )";
        $idrequete = 0;
        $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    }

    //Table multimediaspace_fichiersovml
    $requete = "SHOW TABLES LIKE '".MULTIMEDIASPACE_FICHIEROVML."'";
    $idrequete = 0;
    $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    if (count($resultats) == 0) {
        $requete = "CREATE TABLE `".MULTIMEDIASPACE_FICHIEROVML."` (
                      `id` int(11) unsigned NOT NULL auto_increment,
                      `cheminfichierovml` varchar(255) NOT NULL,
                      `description` varchar(255) NOT NULL,
                      PRIMARY KEY  (`id`)
                    )";
        $idrequete = 0;
        $resultats = multimediaspace_sql($requete, $erreurs, $idrequete);
    }


    $addonName = 'multimediaspace';
    $addonInfo = bab_getAddonInfosInstance($addonName);
    $addonPhpPath = $addonInfo->getPhpPath();

    require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';

    $functionalities = new bab_functionalities();

    @bab_functionality::includefile('PortletBackend');
    if (class_exists('Func_PortletBackend')) {
        $functionalities = new bab_functionalities();
        $functionalities->registerClass('Func_PortletBackend_MultimediaSpace', $addonPhpPath . 'portletbackend.class.php');
    }


    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/multimediaspace/templates');
    $registry->changeDirectory('Youtube');
    $registry->setKeyValue('name', 'Youtube');
    $registry->setKeyValue('description', 'Iframe contenant une vid�o de Youtube');
    $registry->setKeyValue('body', '<iframe id="ytplayer___VIDEOID__" type="text/html" width="__WIDTH__" height="__HEIGHT__"
  src="http://www.youtube.com/embed/__VIDEOID__?autoplay=__AUTOPLAY__&wmode=transparent&webm=1" frameborder="0"></iframe>
<script type="text/javascript">
    var obj = document.getElementById("ytplayer___VIDEOID__");
    obj.aspectRatio = 3 / 4;
    console.debug(obj);
    jQuery.getJSON(
        "https://gdata.youtube.com/feeds/api/videos/__VIDEOID__?v=2&alt=json",
        function (data) {
            var obj = document.getElementById("ytplayer___VIDEOID__");
            var mediaGroup = data.entry.media$group;
            if (data.entry.media$group.yt$aspectRatio && data.entry.media$group.yt$aspectRatio.$t == "widescreen") {
                obj.aspectRatio = 9 / 16;
            }
            jQuery(obj).height(jQuery(obj).width() * obj.aspectRatio);
        }
    );
    jQuery(window).resize(function () {
        var obj = document.getElementById("ytplayer___VIDEOID__");
    console.debug(obj);
        jQuery(obj).height(jQuery(obj).width() * obj.aspectRatio);
    });
</script>');
    $registry->setKeyValue('parameters', '__VIDEOID__,Identifiant de la vid�o
__WIDTH__,Largeur en pixels
__HEIGHT__,Hauteur en pixels
__AUTOPLAY__,Jouer automatiquement');

    return true;
}




function multimediaspace_onPackageAddon() {
    return true;
}

function multimediaspace_onDeleteAddon() {
    global $babAddonHtmlPath;

    require_once $GLOBALS['babInstallPath']."utilit/eventincl.php";
    bab_removeEventListener(
        'bab_eventEditorFunctions',
        'multimediaspace_editeurwysiwyg_lien',
        $babAddonHtmlPath.'fonctions.php'
    );

    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
    $functionalities = new bab_functionalities();
    $functionalities->unregister('PortletBackend/MultimediaSpace');

    return true;
}
?>
