<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2008 by CANTICO ( http://www.cantico.fr )              *
 ************************************************************************/
include_once 'base.php';

//Chargement des fonctions
global $babAddonPhpPath;
global $babInstallPath;
require_once dirname(__FILE__).'/fonctionsgen.php';
require_once dirname(__FILE__).'/fonctions.php';

/* Gestion : arborescence des r�pertoires */
function multimediaspace_gestion(&$erreurs) {
	global $babBody;

	class multimediaspace_gestion
	{
		//Variables

		function multimediaspace_gestion($erreurs) {
			$this->tajoutersousrepertoires = multimediaspace_traduire('Add a subfolder');
			$this->tsupprimerrepertoire = multimediaspace_traduire('Delete the folder');
			$this->tlistevideos = multimediaspace_traduire('List of the videos');
			$this->tordonnersousrepertoires = multimediaspace_traduire('Order the subfolders');
			$this->urlimgajoutersousrepertoires = multimediaspace_urltemplatesmodule().'images/edit_add.gif';
			$this->urlimgsupprimerrepertoire = multimediaspace_urltemplatesmodule().'images/edit_remove.gif';
			$this->urlimglistevideos = multimediaspace_urltemplatesmodule().'images/video2.gif';
			$this->urlimgordonnersousrepertoires = multimediaspace_urltemplatesmodule().'images/z-a.gif';
			
			/* Lien fichiers OVML */
			$this->tlienfichiersovml = multimediaspace_traduire('Management of files OVML');
			global $babAddonUrl;
			$this->lienfichiersovml = $babAddonUrl.'user&amp;idx=fichiersovmlgestion';
			
			/* R�cup�re la liste des r�pertoires : sera r�cursif mais pour l'instant g�re seulement 5 niveaux */
			$this->repertoires = array();
			$this->optionsrepertoires = array();
			$this->niveauxrepertoires = array();
			$espacemultimedia = new Multimediaspace_EspaceMultimedia();
			/* R�pertoire parent */
			$this->repertoireracine = $espacemultimedia->repertoireparent();
			$this->repertoires[] = $espacemultimedia->repertoireparent();
			$this->niveauxrepertoires[] = 0;
			$this->optionsrepertoires[] = array(true, false, false, true, false, 0);
			$repertoiresniveau1 = $espacemultimedia->repertoires(false);
			for($i=0;$i<=count($repertoiresniveau1)-1;$i++) {
				$this->repertoires[] = $repertoiresniveau1[$i];
				$this->niveauxrepertoires[] = 1;
				if ($repertoiresniveau1[$i]->heritedesdroits == 1) {
					$icone = 1;
				} else {
					$icone = 2;
				}
				$this->optionsrepertoires[] = array(true, true, true, true, true, $icone);
				$repertoiresniveau2 = $repertoiresniveau1[$i]->sousrepertoires(false);
				for($j=0;$j<=count($repertoiresniveau2)-1;$j++) {
					$this->repertoires[] = $repertoiresniveau2[$j];
					$this->niveauxrepertoires[] = 2;
					if ($repertoiresniveau2[$j]->heritedesdroits == 1) {
						$icone = 1;
					} else {
						$icone = 2;
					}
					$this->optionsrepertoires[] = array(true, true, true, true, true, $icone);
					$repertoiresniveau3 = $repertoiresniveau2[$j]->sousrepertoires(false);
					for($k=0;$k<=count($repertoiresniveau3)-1;$k++) {
						$this->repertoires[] = $repertoiresniveau3[$k];
						$this->niveauxrepertoires[] = 3;
						if ($repertoiresniveau3[$k]->heritedesdroits == 1) {
							$icone = 1;
						} else {
							$icone = 2;
						}
						$this->optionsrepertoires[] = array(true, true, true, true, true, $icone);
						$repertoiresniveau4 = $repertoiresniveau3[$k]->sousrepertoires(false);
						for($l=0;$l<=count($repertoiresniveau4)-1;$l++) {
							$this->repertoires[] = $repertoiresniveau4[$l];
							$this->niveauxrepertoires[] = 4;
							if ($repertoiresniveau4[$l]->heritedesdroits == 1) {
								$icone = 1;
							} else {
								$icone = 2;
							}
							$this->optionsrepertoires[] = array(true, true, true, true, true, $icone);
							$repertoiresniveau5 = $repertoiresniveau4[$l]->sousrepertoires(false);
							for($m=0;$m<=count($repertoiresniveau5)-1;$m++) {
								$this->repertoires[] = $repertoiresniveau5[$m];
								$this->niveauxrepertoires[] = 5;
								if ($repertoiresniveau5[$m]->heritedesdroits == 1) {
									$icone = 1;
								} else {
									$icone = 2;
								}
								$this->optionsrepertoires[] = array(true, true, true, true, true, $icone);
							}
						}
					}
				}
			}
			$this->editionpossible = true;
			$this->compteur = 0;
		}
		
		function liste() {
			if ($this->compteur > count($this->repertoires) - 1) {
				return false;
			}
			$this->id = $this->repertoires[$this->compteur]->id;
			$this->nom = $this->repertoires[$this->compteur]->nom;
			$this->nbvideos = $this->repertoires[$this->compteur]->nbvideos();
			/* Options du r�pertoire */
			$this->afficherajoutersousrepertoires = $this->optionsrepertoires[$this->compteur][0];
			$this->affichersupprimerrepertoire = $this->optionsrepertoires[$this->compteur][1];
			$this->afficherlistevideos = $this->optionsrepertoires[$this->compteur][2];
			$this->afficherordonnersousrepertoires = $this->optionsrepertoires[$this->compteur][3];
			$this->afficherdroitsacces = $this->optionsrepertoires[$this->compteur][4];
			if ($this->optionsrepertoires[$this->compteur][5] == 2) {
				$this->urlimgdroitsacces = multimediaspace_urltemplatesmodule().'images/access.gif';
				$this->timgdroitsacces = multimediaspace_traduire('The access rights of the folder are unique');
				$this->tdroitsacces = multimediaspace_traduire('Access rights');
			} else {
				$this->urlimgdroitsacces = multimediaspace_urltemplatesmodule().'images/accessherite.gif';
				$this->timgdroitsacces = multimediaspace_traduire('The access rights of the folder are inherited');
				$this->tdroitsacces = multimediaspace_traduire('Inherited access rights');
			}
			$this->decalage = ''; /* Permet d'identifier l'arborescence */
			for ($i=0;$i<=$this->niveauxrepertoires[$this->compteur]-1;$i++) {
				$this->decalage .= '&nbsp;&nbsp;&nbsp;';
			}
			/* Nombre de videos */
			$this->nbvideos = $this->repertoires[$this->compteur]->nbvideos();
			if ($this->nbvideos > 1) {
				$this->tnbvideos = multimediaspace_traduire('videos');
			} else {
				$this->tnbvideos = multimediaspace_traduire('video');
			}
			global $babAddonUrl;
			$this->urlajoutersousrepertoires = $babAddonUrl.'user&amp;idx=ajoutrepertoire&amp;idrepertoireparent='.$this->id;
			$this->urlsupprimerrepertoire = $babAddonUrl.'user&amp;idx=suppressionrepertoire&amp;idrepertoire='.$this->id;
			$this->urllistevideos = $babAddonUrl.'user&amp;idx=listevideosgestion&amp;idrepertoire='.$this->id;
			$this->urlordonnersousrepertoires = $babAddonUrl.'user&amp;idx=ordonnersousrepertoires&amp;idrepertoireparent='.$this->id;
			$this->urldroitsacces = $babAddonUrl.'user&amp;idx=droitsaccesrepertoire&amp;idrepertoire='.$this->id;
			$this->urleditionrepertoire = $babAddonUrl.'user&amp;idx=editionrepertoire&amp;idrepertoire='.$this->id;
			if ($this->repertoireracine->id == $this->id) {
				$this->editionpossible = false;
			} else {
				$this->editionpossible = true;
			}			
			$this->compteur++;
			return true;
		}
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_gestion($erreurs);
	multimediaspace_template($a, "userpages.html", "gestion");
}

/* Liste des fichiers OVML � utiliser dans les fonctions de l'�diteur wysiwyg */
function multimediaspace_fichiersovmlgestion(&$erreurs) {
	global $babBody;

	class multimediaspace_fichiersovmlgestion
	{
		//Variables

		function multimediaspace_fichiersovmlgestion(&$erreurs) {
			$this->taide = multimediaspace_traduire('File OVML defined in this list will be proposed during the addition of a video since the wysiwyg editor.');
			$this->taide2 = multimediaspace_traduire('The following syntax will be added in the editor:');
			$this->taide3 = multimediaspace_traduire('$OVML(example.ovml,idvideo=5)');
			$this->taide4 = multimediaspace_traduire('Example of contents of a file OVML:');
			
			/* R�cup�ration des donn�es */
			$this->fichiersovml = multimediaspace_fichiersovml();
			global $babAddonUrl;
			$this->urlajoutfichierovml = $babAddonUrl.'user&amp;idx=ajoutfichierovmlgestion';
			$this->tajoutfichierovml = multimediaspace_traduire('Add a file OVML');
			$this->tnom = multimediaspace_traduire('Name');
			$this->tdescription = multimediaspace_traduire('Description');
			$this->tedition = multimediaspace_traduire('Edition');
			$this->tsuppression = multimediaspace_traduire('Deletion');
			$this->compteur = 0;
		}
		
		function liste() {
			if ($this->compteur > count($this->fichiersovml) - 1) {
				return false;
			}
			$this->id = $this->fichiersovml[$this->compteur]['id'];
			$this->cheminfichierovml = $this->fichiersovml[$this->compteur]['cheminfichierovml'];
			$this->description = $this->fichiersovml[$this->compteur]['description'];
			global $babAddonUrl;
			$this->edition = multimediaspace_traduire('Edit');
			$this->editionurl = $babAddonUrl.'user&amp;idx=editionfichierovmlgestion&amp;idfichierovml='.$this->id;
			$this->suppression = multimediaspace_traduire('Delete');
			$this->suppressionurl = $babAddonUrl.'user&amp;idx=suppressionfichierovmlgestion&amp;idfichierovml='.$this->id;
			$this->compteur++;
			return true;
		}
		
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_fichiersovmlgestion($erreurs);
	multimediaspace_template($a, "userpages.html", "fichiersovmlgestion");
}

/* Ajout d'un fichier OVML */
function multimediaspace_ajoutfichierovmlgestion(&$erreurs) {
	global $babBody;

	class multimediaspace_ajoutfichierovmlgestion
	{
		//Variables

		function multimediaspace_ajoutfichierovmlgestion(&$erreurs) {
			$this->aide = multimediaspace_traduire("This field must be informed");
			$this->enregistrer = multimediaspace_traduire("Save");
			$this->tchampnonrenseigne = multimediaspace_traduire("An obligatory field is not informed");
			
			$this->tcheminfichierovml = multimediaspace_traduire('Path and name of the file OVML (relative path to the folder OVML of the skin)');
			$this->tdescription = multimediaspace_traduire('Description');
			
			//Emp�che la saisie
			$this->antif5 = multimediaspace_antif5valeur();
		}
		
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_ajoutfichierovmlgestion($erreurs);
	multimediaspace_template($a, "userpages.html", "ajoutfichierovmlgestion");
}

/* Enregistrement de l'ajout d'un fichier ovml */
function multimediaspace_ajoutfichierovmlgestionenr($donnees, &$erreurs) {
	$cheminfichierovml = $donnees['cheminfichierovml'];
	$description = $donnees['description'];
	
	multimediaspace_echappesql($cheminfichierovml);
	multimediaspace_echappesql($description);
	
	$requete = "insert into `".MULTIMEDIASPACE_FICHIEROVML."` ( `cheminfichierovml` , `description` )
			  values ( '$cheminfichierovml' , '$description' )
			";
	$idrequete = 0;
	multimediaspace_sql($requete, $erreurs, $idrequete);
	if (count($erreurs) == 0) {
		$erreurs[] = "Save done";
	}
}

/* Suppression d'un fichier ovml */
function multimediaspace_suppressionfichierovmlgestion($donnees, &$erreurs) {
	global $babBody;

	class multimediaspace_suppressionfichierovmlgestion
	{
		//Variables

		function multimediaspace_suppressionfichierovmlgestion($donnees, &$erreurs) {
			$this->tcheminfichierovml = multimediaspace_traduire("Path and name of the file OVML (relative path to the folder OVML of the skin)");
			$this->tsupprimer = multimediaspace_traduire("Delete the reference to the file OVML");
			$this->tdescription = multimediaspace_traduire("Description");
			//R�cup�ration des donn�es 
			$fichiersovml = multimediaspace_fichiersovml();
			$this->idfichierovml = '';
			$this->cheminfichierovml = '';
			$this->description = '';
			for ($i=0;$i<=count($fichiersovml)-1;$i++) {
				if ($fichiersovml[$i]['id'] == $donnees['idfichierovml']) {
					$this->idfichierovml = $fichiersovml[$i]['id'];
					$this->cheminfichierovml = $fichiersovml[$i]['cheminfichierovml'];
					$this->description = $fichiersovml[$i]['description'];
				}
			}
			
			//Emp�che la saisie
			$this->antif5 = multimediaspace_antif5valeur();
		}
		
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_suppressionfichierovmlgestion($donnees, $erreurs);
	multimediaspace_template($a, "userpages.html", "suppressionfichierovmlgestion");
}

/* Enregistrement de la suppression du fichier ovml */
function multimediaspace_suppressionfichierovmlgestionenr($donnees, &$erreurs) {
	$idfichierovml = $donnees['idfichierovml'];
	$requete = "delete from `".MULTIMEDIASPACE_FICHIEROVML."` where id='".$idfichierovml."'";
	$idrequete = 0;
	$erreurs = array();
	$res = multimediaspace_sql($requete, $erreurs, $idrequete);
	$erreurs[] = multimediaspace_traduire("Deletion done");
}

/* Edition d'un fichier ovml */
function multimediaspace_editionfichierovmlgestion($donnees, &$erreurs) {
	global $babBody;

	class multimediaspace_editionfichierovmlgestion
	{
		//Variables
		
		function multimediaspace_editionfichierovmlgestion($donnees, &$erreurs) {
			$this->aide = multimediaspace_traduire("This field must be informed");
			$this->enregistrer = multimediaspace_traduire("Save");
			$this->tchampnonrenseigne = multimediaspace_traduire("An obligatory field is not informed");
			$this->tcheminfichierovml = multimediaspace_traduire("Path and name of the file OVML (relative path to the folder OVML of the skin)");
			$this->tdescription = multimediaspace_traduire("Description");
			
			/* Donn�es */
			$fichiersovml = multimediaspace_fichiersovml();
			$this->idfichierovml = $donnees['idfichierovml'];
			$this->cheminfichierovml = '';
			$this->description = '';
			for ($i=0;$i<=count($fichiersovml)-1;$i++) {
				if ($fichiersovml[$i]['id'] == $donnees['idfichierovml']) {
					$this->cheminfichierovml = $fichiersovml[$i]['cheminfichierovml'];
					$this->description = $fichiersovml[$i]['description'];
				}
			}
			
			//Emp�che la saisie
			$this->antif5 = multimediaspace_antif5valeur();
		}
		
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_editionfichierovmlgestion($donnees, $erreurs);
	multimediaspace_template($a, "userpages.html", "editionfichierovmlgestion");
}

/* Enregistrement de la modification du fichier ovml */
function multimediaspace_editionfichierovmlgestionenr($donnees, &$erreurs) {
	$idfichierovml = $donnees['idfichierovml'];
	$cheminfichierovml = $donnees['cheminfichierovml'];
	$description = $donnees['description'];
	
	multimediaspace_echappesql($idfichierovml);
	multimediaspace_echappesql($cheminfichierovml);
	multimediaspace_echappesql($description);
	
	$requete = "update `".MULTIMEDIASPACE_FICHIEROVML."` set `cheminfichierovml`='".$cheminfichierovml."', `description`='".$description."' where id='".$idfichierovml."'";
	$idrequete = 0;
	$erreurs = array();
	$res = multimediaspace_sql($requete, $erreurs, $idrequete);
	$erreurs[] = 'Save done';
}

/* Ajout d'un repertoire */
function multimediaspace_ajoutrepertoire($donnees, &$erreurs) {
	global $babBody;

	class multimediaspace_ajoutrepertoire
	{
		//Variables
		
		function multimediaspace_ajoutrepertoire($donnees, &$erreurs) {
			$this->aide = multimediaspace_traduire("This field must be informed");
			$this->enregistrer = multimediaspace_traduire("Save");
			$this->tchampnonrenseigne = multimediaspace_traduire("An obligatory field is not informed");
			$this->trepertoiresparents = multimediaspace_traduire("Related folders");
			$this->tnom = multimediaspace_traduire("Name");
			$this->theritedesdroits = multimediaspace_traduire("Does the folder inherit of related access rights?");
			$this->toui = multimediaspace_traduire('Yes');
			$this->tnon = multimediaspace_traduire('No');
			
			$this->idrepertoireparent = $donnees['idrepertoireparent'];
			$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoireparent']);
			$repertoire->charge();
			
			/* Liste des r�pertoires parents du r�pertoire s�lectionn� */
			$this->repertoiresparents = '';
			$repertoiresparents = $repertoire->repertoiresparents(true);
			/* On ne propose pas de choisir si on herite des droits ou pas, dans le cas o� on se trouve au niveau 1 des r�pertoires */
			if (count($repertoiresparents) >= 1) {
				$this->afficheheritedesdroits = true;
			} else {
				$this->afficheheritedesdroits = false;
			}
			/* Ajoute le r�pertoire s�lectionn� */
			$repertoiresparents[] = $repertoire;
			$decalage = '';
			for ($i=0;$i<=count($repertoiresparents)-1;$i++) {
				if ($i == 0) {
					$this->repertoiresparents .= $repertoiresparents[$i]->nom;
				} else {
					$this->repertoiresparents .= '<br />'.$decalage.$repertoiresparents[$i]->nom;
				}
				$decalage .= '&nbsp;';
			}
			
			//Emp�che la saisie
			$this->antif5 = multimediaspace_antif5valeur();
		}
		
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_ajoutrepertoire($donnees, $erreurs);
	multimediaspace_template($a, "userpages.html", "ajoutrepertoire");
}

/* Enregistrement d'un nouveau repertoire */
function multimediaspace_ajoutrepertoireenr($donnees, &$erreurs) {
	$idrepertoireparent = $donnees['idrepertoireparent'];
	$nom = $donnees['nom'];
	$heritedesdroits = $donnees['heritedesdroits'];
	
	$repertoire = new Multimediaspace_Repertoire('', $nom, '', '', $heritedesdroits);
	$repertoire->enregistre($idrepertoireparent);
}

/* Suppression d'un repertoire */
function multimediaspace_suppressionrepertoire($donnees, &$erreurs) {
	global $babBody;

	class multimediaspace_suppressionrepertoire
	{
		//Variables

		function multimediaspace_suppressionrepertoire($donnees, &$erreurs) {
			$this->tnom = multimediaspace_traduire("Name");
			$this->trepertoiresparents = multimediaspace_traduire("Related folders");
			$this->tsupprimer = multimediaspace_traduire("Delete the folder");
			$this->tdescription = multimediaspace_traduire("The deletion of the folder will also suppress the subfolders<br /> and the videos that it contained");
			//R�cup�ration des donn�es 
			$this->idrepertoire = $donnees['idrepertoire'];
			$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
			$repertoire->charge();
			
			/* Liste des r�pertoires parents du r�pertoire s�lectionn� */
			$this->repertoiresparents = '';
			$repertoiresparents = $repertoire->repertoiresparents(true);
			/* Ajoute le r�pertoire s�lectionn� */
			$repertoiresparents[] = $repertoire;
			$decalage = '';
			for ($i=0;$i<=count($repertoiresparents)-1;$i++) {
				if ($i == 0) {
					$this->repertoiresparents .= $repertoiresparents[$i]->nom;
				} else {
					$this->repertoiresparents .= '<br />'.$decalage.$repertoiresparents[$i]->nom;
				}
				$decalage .= '&nbsp;';
			}
			$this->idrepertoire = $repertoire->id;
			$this->nom = $repertoire->nom;
			
			//Emp�che la saisie
			$this->antif5 = multimediaspace_antif5valeur();
		}
		
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_suppressionrepertoire($donnees, $erreurs);
	multimediaspace_template($a, "userpages.html", "suppressionrepertoire");
}

/* Enregistrement de la suppression du repertoire */
function multimediaspace_suppressionrepertoireenr($donnees, &$erreurs) {
	$idrepertoire = $donnees['idrepertoire'];
	$repertoire = new Multimediaspace_Repertoire($idrepertoire);
	$repertoire->charge();
	$repertoire->supprime();
	$erreurs[] = multimediaspace_traduire("Deletion done");
}





/* Edition d'un repertoire */
function multimediaspace_editionrepertoire($donnees, &$erreurs)
{
	global $babBody;

	class multimediaspace_editionrepertoire
	{
		//Variables

		function multimediaspace_editionrepertoire($donnees, &$erreurs) {
			$this->aide = multimediaspace_traduire("This field must be informed");
			$this->enregistrer = multimediaspace_traduire("Save");
			$this->tchampnonrenseigne = multimediaspace_traduire("An obligatory field is not informed");
			$this->trepertoiresparents = multimediaspace_traduire("Related folders");
			$this->tnom = multimediaspace_traduire("Name");
			$this->theritedesdroits = multimediaspace_traduire("Does the folder inherit of related access rights?");
			$this->toui = multimediaspace_traduire('Yes');
			$this->tnon = multimediaspace_traduire('No');
			
			$this->idrepertoire = $donnees['idrepertoire'];
			$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
			$repertoire->charge();
			if ($repertoire->heritedesdroits == 2) {
				$this->heritedesdroits = false;
			} else {
				$this->heritedesdroits = true;
			}
			$this->nom = $repertoire->nom;
			
			/* Liste des r�pertoires parents du r�pertoire s�lectionn� */
			$this->repertoiresparents = '';
			$repertoiresparents = $repertoire->repertoiresparents(true);
			
			/* On ne propose pas de choisir si on herite des droits ou pas, dans le cas o� on se trouve au niveau 1 des r�pertoires */
			if (count($repertoiresparents) >= 1) {
				$this->afficheheritedesdroits = true;
			} else {
				$this->afficheheritedesdroits = false;
			}
			/* Si on �dite le r�pertoire racine, on n'affiche pas d'options */
			$espacemultimedia = new Multimediaspace_EspaceMultimedia();
			$repertoireracine = $espacemultimedia->repertoireparent();
			if ($repertoireracine->id == $donnees['idrepertoire']) {
				$this->afficheheritedesdroits = false;
			}
			
			$decalage = '';
			for ($i=0;$i<=count($repertoiresparents)-1;$i++) {
				if ($i == 0) {
					$this->repertoiresparents .= $repertoiresparents[$i]->nom;
				} else {
					$this->repertoiresparents .= '<br />'.$decalage.$repertoiresparents[$i]->nom;
				}
				$decalage .= '&nbsp;';
			}
			
			//Emp�che la saisie
			$this->antif5 = multimediaspace_antif5valeur();
		}
		
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_editionrepertoire($donnees, $erreurs);
	
	multimediaspace_template($a, 'userpages.html', 'editionrepertoire');
}







/* Enregistrement de la modification du repertoire */
function multimediaspace_editionrepertoireenr($donnees, &$erreurs) {
	$idrepertoire = $donnees['idrepertoire'];
	$repertoire = new Multimediaspace_Repertoire($idrepertoire);
	$repertoire->charge();
	$repertoire->nom = $donnees['nom'];
	$repertoire->heritedesdroits = $donnees['heritedesdroits'];
	$repertoire->enregistre();
}

/* Droits d'acc�s d'un repertoire */
function multimediaspace_droitsaccesrepertoire($donnees, &$erreurs) {
	global $babBody;

	class multimediaspace_droitsaccesrepertoire
	{
		//Variables

		function multimediaspace_droitsaccesrepertoire($donnees, &$erreurs) {
			/* V�rifie si les droits sont h�rit�s ou pas */
			$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
			$repertoire->charge();
			if ($repertoire->heritedesdroits == 2) {
				$this->heritedesdroits = false;
				/* ACL */
				$filtres = array(0,0,0,0,0,array());
				global $babAddonTarget;
				$tg = $babAddonTarget.'/user';
				$idx = 'droitsaccesrepertoireenr';
				multimediaspace_acl($tg, $idx, $repertoire->id, MULTIMEDIASPACE_DROITSLECTUREREPERTOIRE, 'What are the users who can see the videos of this folder?', $filtres);
			} else {
				$this->heritedesdroits = true;
			}
			$this->nom = $repertoire->nom;
			$this->tnom = multimediaspace_traduire('Name of the folder');
			$this->therite = multimediaspace_traduire('The folder inherit of related access rights');
			
			//Emp�che la saisie
			$this->antif5 = multimediaspace_antif5valeur();
		}
		
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_droitsaccesrepertoire($donnees, $erreurs);
	multimediaspace_template($a, "userpages.html", "droitsaccesrepertoire");
}

/* Liste des vid�os d'un repertoire (gestion) */
function multimediaspace_listevideosgestion($donnees, &$erreurs) {
	global $babBody;

	class multimediaspace_listevideosgestion
	{
		//Variables

		public function __construct($donnees, &$erreurs) {
			/* R�cup�ration des donn�es */
			$this->idrepertoire = $donnees['idrepertoire'];
			$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
			$repertoire->charge();
			$this->nomrepertoire = $repertoire->nom;
			$this->tnomrepertoire = multimediaspace_traduire('Folder');
			
			$this->videos = $repertoire->videos();
			global $babAddonUrl;
			$this->urlajoutvideo = $babAddonUrl.'user&amp;idx=ajoutvideogestion&amp;idrepertoire='.$donnees['idrepertoire'];
			$this->tajoutvideo = multimediaspace_traduire('Add a video');
			$this->tnom = multimediaspace_traduire('Name');
			$this->ttype = multimediaspace_traduire('Type');
			$this->tedition = multimediaspace_traduire('Edition');
			$this->tsuppression = multimediaspace_traduire('Deletion');
			$this->compteur = 0;
		}
		

		public function liste()
		{
			if ($this->compteur > count($this->videos) - 1) {
				return false;
			}
			$this->id = $this->videos[$this->compteur]->id;
			$this->nom = $this->videos[$this->compteur]->nom;
			$this->type = '';
			switch($this->videos[$this->compteur]->type) {
				
				case 'flv':
					$this->type = multimediaspace_traduire('File FLV');
					break;
				case 'url':
					$this->type = multimediaspace_traduire('URL extern');
					break;
				case 'videointegree':
					$this->type = multimediaspace_traduire('Integrated video');
					break;
				case 'template':
					$this->type = multimediaspace_traduire('Template');
					break;
			}
			global $babAddonUrl;
			$this->edition = multimediaspace_traduire('Edit');
			$this->editionurl = $babAddonUrl.'user&amp;idx=editionvideo&amp;idvideo='.$this->id;
			$this->suppression = multimediaspace_traduire('Delete');
			$this->suppressionurl = $babAddonUrl.'user&amp;idx=suppressionvideo&amp;idvideo='.$this->id;
			$this->compteur++;
			return true;
		}
		
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_listevideosgestion($donnees, $erreurs);
	multimediaspace_template($a, "userpages.html", "listevideosgestion");
}

/* Ajout d'une vid�o dans un repertoire (gestion) : �tape 1 */
function multimediaspace_ajoutvideogestion($donnees, &$erreurs) {
	global $babBody;

	class multimediaspace_ajoutvideogestion
	{
		//Variables

		public function __construct($donnees, &$erreurs)
		{
			/* R�cup�ration des donn�es */

			$this->idrepertoire = $donnees['idrepertoire'];
			$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
			$repertoire->charge();
			$this->nomrepertoire = $repertoire->nom;
			
			$this->tselectionmethode = multimediaspace_traduire('Select the method of integration of the video');
			$this->ttypeflv = multimediaspace_traduire('File FLV');
			$this->ttypeflv2 = multimediaspace_traduire('The video file is deposited via the interface of the addon. Its format is FLV (Flash Video). The video is read inside the site.');
			$this->ttypeflv3 = multimediaspace_traduire('Remark:');
			$this->ttypeflv4 = multimediaspace_traduire('The following software allows the conversion of files videos in format FLV:');
			$this->ttypevideointegree = multimediaspace_traduire('Integrated video');
			$this->ttypevideointegree2 = multimediaspace_traduire('The video file is not deposited via the interface of the addon. The HTML code for the display of the video is supplied. The video is read inside the site.');
			$this->ttypeurl = multimediaspace_traduire('URL extern');
			$this->ttypeurl2 = multimediaspace_traduire('The video file is not deposited via the interface of the addon. A click on the image of the video will send back on the address extern. The video is not read inside the site.');
			
			global $babAddonUrl;
			$this->urltypeflv = $babAddonUrl.'user&amp;idx=ajoutvideogestion2&amp;idrepertoire='.$this->idrepertoire.'&amp;type=flv';
			$this->urltypevideointegree = $babAddonUrl.'user&amp;idx=ajoutvideogestion2&amp;idrepertoire='.$this->idrepertoire.'&amp;type=videointegree';
			$this->urltypeurl = $babAddonUrl.'user&amp;idx=ajoutvideogestion2&amp;idrepertoire='.$this->idrepertoire.'&amp;type=url';
			
			//Emp�che la saisie
			$this->antif5 = multimediaspace_antif5valeur();
			
			
			$templates = multimediaspace_getVideoTemplates();
			$this->templates = array();
			foreach ($templates as $templateName) {
			
				$template = multimediaspace_getVideoTemplate($templateName);
				$this->templates[] = $template;
			}

			$this->t_template = multimediaspace_traduire('Video template');
		}
		
		
		public function listTemplates()
		{
			list(,$template) = each($this->templates);
			if (!$template) {
				reset($this->templates);
				return false;
			}
			$this->template_name = bab_toHtml($template['name']);
			$this->template_description = bab_toHtml($template['description']);
			$this->urltypevideotemplate = bab_toHtml($GLOBALS['babAddonUrl'] . 'user&idx=ajoutvideogestion2&idrepertoire=' . $this->idrepertoire . '&type=template&template=' . $template['name']);
			return true;
		}
	}

	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_ajoutvideogestion($donnees, $erreurs);
	multimediaspace_template($a, 'userpages.html', 'ajoutvideogestion');
}






/* Ajout d'une vid�o dans un repertoire (gestion) : �tape 2 */
function multimediaspace_ajoutvideogestion2($donnees, &$erreurs)
{
	global $babBody;

	class multimediaspace_ajoutvideogestion2
	{
		//Variables
		public $templateName = null;

		function __construct($donnees, &$erreurs)
		{
			/* R�cup�ration des donn�es */
			$this->idrepertoire = $donnees['idrepertoire'];
			$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
			$repertoire->charge();
			$this->nomrepertoire = $repertoire->nom;
			
			/* Type de la vid�o */
			$this->type = $donnees['type'];

			if ($this->type === 'template') {

				$this->templateName = $donnees['template'];
				
				$template = multimediaspace_getVideoTemplate($donnees['template']);
				$this->parameters = multimediaspace_extractVideoTemplateParameters($template['parameters']);

			} elseif ($this->type != 'flv' && $this->type != 'videointegree' && $this->type != 'url') {
				$this->type = 'flv';
			}
	
			$this->aide = multimediaspace_traduire("This field must be informed");
			$this->enregistrer = multimediaspace_traduire("Save");
			$this->tchampnonrenseigne = multimediaspace_traduire("An obligatory field is not informed");
			$this->tchampnonentier = multimediaspace_traduire("The value must be an integer");
			$this->trepertoire = multimediaspace_traduire('Folder');
			$this->tvideo = multimediaspace_traduire('Video');
			$this->tnom = multimediaspace_traduire('Name');
			$this->tdescription = multimediaspace_traduire('Description');
			$this->tlargeurlecteur = multimediaspace_traduire('Width of the video player in pixels');
			$this->thauteurlecteur = multimediaspace_traduire('Height of the video player in pixels');
			$this->timage = multimediaspace_traduire('Associated image');
			$this->toptionnelle = multimediaspace_traduire('(optional)');
			$this->tvaleurvideointegree = multimediaspace_traduire('Html/Javascript code');
			$this->tvaleururl = multimediaspace_traduire('URL extern');
			$this->tjouerautomatiquement = multimediaspace_traduire('Play automatically the video after its load');
			$this->tnon = multimediaspace_traduire('No');
			$this->toui = multimediaspace_traduire('Yes');
			
			//Emp�che la saisie
			$this->antif5 = multimediaspace_antif5valeur();
		}
		
		public function nextParameter()
		{
			$parameter = each($this->parameters);
			if (!$parameter) {
				reset($this->parameters);
				return false;
			}
			$this->parameter_name = bab_toHtml('valeur[' . $parameter['key'] . ']');
			$this->parameter_description = bab_toHtml($parameter['value']);
			return true;
		}
	}

	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_ajoutvideogestion2($donnees, $erreurs);
	multimediaspace_template($a, 'userpages.html', 'ajoutvideogestion2');
}




/* Enregistrement de l'ajout d'une vid�o (gestion) */
function multimediaspace_ajoutvideogestionenr($donnees, &$erreurs)
{
	
// 	var_dump($donnees);
// 	die;
	/* Cr�ation des r�pertoires de destination s'ils n'existent pas */
	$resc = array(); /* R�sultats de cr�ation des r�pertoires */
	$rescchemin = array();
	$resc[] = multimediaspace_creationrepertoire(multimediaspace_cheminuploadmodule());
	$rescchemin[] = multimediaspace_cheminuploadmodule();
	$resc[] = multimediaspace_creationrepertoire(multimediaspace_cheminuploadmodule().'images');
	$rescchemin[] = multimediaspace_cheminuploadmodule().'images';
	$resc[] = multimediaspace_creationrepertoire(multimediaspace_chemintemplatesmodule().'videos');
	$rescchemin[] = multimediaspace_chemintemplatesmodule().'videos';
	/* R�pertoire de destination */
	$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
	$repertoire->charge();
	$ssrep = $repertoire->repertoiresparents();
	for ($i=0;$i<=count($ssrep)-1;$i++) {
		/* R�pertoires d'upload des r�pertoires parents */
		$cheminfinalvideo = $ssrep[$i]->cheminrepertoirevideos();
		$cheminfinalimage = $ssrep[$i]->cheminrepertoireimages();
		$resc[] = multimediaspace_creationrepertoire($cheminfinalvideo);
		$rescchemin[] = $cheminfinalvideo;
		$resc[] = multimediaspace_creationrepertoire($cheminfinalimage);
		$rescchemin[] = $cheminfinalimage;
	}
	/* R�pertoire de destination */
	$cheminfinalvideo = $repertoire->cheminrepertoirevideos();
	$cheminfinalimage = $repertoire->cheminrepertoireimages();
	$resc[] = multimediaspace_creationrepertoire($cheminfinalvideo);
	$rescchemin[] = $cheminfinalvideo;
	$resc[] = multimediaspace_creationrepertoire($cheminfinalimage);
	$rescchemin[] = $cheminfinalimage;
	$erreur = false;
	for ($i=0;$i<=count($resc)-1;$i++) {
		if (!$resc[$i]) {
			$erreur = true;
		}
	}
	if ($erreur) {
		bab_debug('Les r�pertoires de destination de la vid�o et de l\'image associ�e n\'ont pas �t� cr��s');
	}
	/* Ajoute un fichier index.html dans le r�pertoire afin d'�viter que le contenu du r�pertoire soit lu */
	if (!multimediaspace_existefichier($cheminfinalimage.'/index.html')) {
		@fopen($cheminfinalimage.'/index.html', 'x');
	}
	if (!multimediaspace_existefichier($cheminfinalvideo.'/index.html')) {
		@fopen($cheminfinalvideo.'/index.html', 'x');
	}
	
	/* Type de la vid�o */
	$type = $donnees['type'];
	if ($type != 'template' && $type != 'flv' && $type != 'videointegree' && $type != 'url') {
		$type = 'flv';
	}
	
	/* Type FLV : on r�cup�re le fichier vid�o */
	$nomfichiervideo = '';
	if ($type == 'flv') {
		if (multimediaspace_fichierdepose('video')) {
			$res = array();
			if($res = multimediaspace_deplacefichiertmp('video', $cheminfinalvideo.'/', true)) {
				if ($res['erreur']) {
					$erreurs[] = multimediaspace_traduire("The file don't have can be attached");
				} else {			
					/* R�cup�re les donn�es de la nouvelle video */
					$nomfichiervideo = $res['nomfichier'];
					/* On renomme le fichier s'il contient des accents */
					$nomfichiervideosansaccents = multimediaspace_chainesansaccents($nomfichiervideo);
					if ($nomfichiervideo != $nomfichiervideosansaccents) {
						$resrenomme = multimediaspace_renommefichier($cheminfinalvideo, $nomfichiervideo, $nomfichiervideosansaccents, true);
						if ($resrenomme['erreur'] != false) {
							$nomfichiervideo = $resrenomme['nomfichier'];
						}
					}
				}
			}
		}
	}
	
	$valeur = '';	
	/* Vid�o int�gr�e, on r�cup�re le code HTML/Javascript */
	if ($type == 'videointegree') {
		$valeur = $donnees['valeur'];
	}

	/* Type URL : on r�cup�re l'url */
	if ($type == 'url') {
		$valeur = trim($donnees['valeur']); //trim supprime les espaces en d�but et fin de cha�ne
		/* Traitement de l'url dans le cas o� http:// n'existe pas */
		if (substr($valeur, 0, 3) == 'www') {
			/* On ajoute http:// pour ces adresses */
			$valeur = 'http://'.$valeur;
		} else {
			if (substr($valeur, 0, 4) != 'http') {
				/* On ajoute http:// */
				$valeur = 'http://'.$valeur;
			}
		}
	}

	if ($type == 'template') {
		$donnees['valeur']['template'] = $donnees['template'];
		$valeur = serialize($donnees['valeur']);
	}
	
	
	/* Option : jouer automatiquement */
	if ($donnees['jouerautomatiquement'] == 2) {
		$jouerautomatiquement = 2;
	} else {
		$jouerautomatiquement = 1;
	}
	
	/* R�cup�re le fichier image */
	$nomfichierminiature = '';
	$typeminiature = '';
	$largeurminiature = '';
	$hauteurminiature = '';	
	if (multimediaspace_fichierdepose('image')) {
		$res = array();
		if($res = multimediaspace_deplacefichiertmp('image', $cheminfinalimage.'/', true)) {
			if ($res['erreur']) {
				$erreurs[] = multimediaspace_traduire("The file don't have can be attached");
			} else {			
				/* R�cup�re les donn�es de la nouvelle video */
				$donneesimg = multimediaspace_donneesfichierdepose('image');
				$nomfichierminiature = $res['nomfichier'];
				$taille = multimediaspace_tailleimage($cheminfinalimage.'/'.$nomfichierminiature);
				$typeminiature = multimediaspace_typeimage($cheminfinalimage.'/'.$nomfichierminiature);
				$largeurminiature = $taille['largeur'];
				$hauteurminiature = $taille['hauteur'];
			}
		}
	}
	
	$video = new Multimediaspace_Video('', $donnees['idrepertoire'], '',$donnees['nom'],$donnees['description'],'',$nomfichiervideo,$type,$donnees['largeur'], $donnees['hauteur'],$donnees['largeur'], $donnees['hauteur'], $nomfichierminiature, $typeminiature,$largeurminiature, $hauteurminiature,0,'', $valeur, $jouerautomatiquement);
	$video->enregistre();
}

/* Suppression d'une vid�o */
function multimediaspace_suppressionvideo($donnees, &$erreurs) {
	global $babBody;

	class multimediaspace_suppressionvideo
	{
		//Variables

		function multimediaspace_suppressionvideo($donnees, &$erreurs) {
			$this->tnom = multimediaspace_traduire("Name");
			$this->trepertoiresparents = multimediaspace_traduire("Related folders");
			$this->tsupprimer = multimediaspace_traduire("Delete the video");
			$this->tdescription = multimediaspace_traduire("Description");
			//R�cup�ration des donn�es 
			$this->idvideo = $donnees['idvideo'];
			$video = new Multimediaspace_Video($donnees['idvideo']);
			$video->charge();
			
			$this->idvideo = $video->id;
			$this->nom = $video->nom;
			$this->description = nl2br($video->description);
			
			//Emp�che la saisie
			$this->antif5 = multimediaspace_antif5valeur();
		}
		
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_suppressionvideo($donnees, $erreurs);
	multimediaspace_template($a, "userpages.html", "suppressionvideo");
}

/* Enregistrement de la suppression de la vid�o */
function multimediaspace_suppressionvideoenr($donnees, &$erreurs) {
	$idvideo = $donnees['idvideo'];
	$video = new Multimediaspace_Video($idvideo);
	$video->charge();
	$video->supprime();
	$erreurs[] = multimediaspace_traduire("Deletion done");
}






/* Edition d'une vid�o */
function multimediaspace_editionvideo($donnees, &$erreurs)
{
	global $babBody;

	class multimediaspace_editionvideo
	{
		//Variables
		
		public function __construct($donnees, &$erreurs)
		{
			$this->aide = multimediaspace_traduire("This field must be informed");
			$this->enregistrer = multimediaspace_traduire("Save");
			$this->tchampnonrenseigne = multimediaspace_traduire("An obligatory field is not informed");
			$this->tnom = multimediaspace_traduire("Name");
			
			$this->idvideo = $donnees['idvideo'];
			$video = new Multimediaspace_Video($donnees['idvideo']);
			$video->charge();
			$this->valeur = $video->valeur;
			$this->nom = $video->nom;
			$this->tnomfichierflv = multimediaspace_traduire("Name of the video file");
			$this->nomfichierflv = '';
			/* Taille de la vid�o */
			if ($video->type == 'flv') {
				$this->nomfichierflv = multimediaspace_nomfichier($video->cheminvideo());
				$this->nomfichierflv .= ' ('.multimediaspace_taillefichier($video->cheminvideo(), 'Mo').' '.multimediaspace_traduire('Mo').')';
			}
			$this->description = $video->description;
			$this->largeurlecteur = $video->largeurlecteur;
			$this->hauteurlecteur = $video->hauteurlecteur;
			if ($video->nomfichierminiature != '') {
				$this->imageexiste = true;
			} else {
				$this->imageexiste = false;
			}
			$this->urlimage = $video->urlminiature;
			$this->ttype = multimediaspace_traduire('Type of the video');
			$this->ttype2 = multimediaspace_traduire('File FLV');
			switch($video->type) {
				case 'videointegree':
					$this->ttype2 = multimediaspace_traduire('Integrated video');
					break;
				case 'url';
					$this->ttype2 = multimediaspace_traduire('URL extern');
					break;
				case 'template':
					$this->ttype2 = multimediaspace_traduire('Template');
					break;
			}
			$this->timageajouter = multimediaspace_traduire("Add a new image");
			$this->tsupprimerimage = multimediaspace_traduire("Delete the image");
			
			/* R�cup�ration des donn�es */
			$repertoire = new Multimediaspace_Repertoire($video->idrepertoire);
			$repertoire->charge();
			$this->nomrepertoire = $repertoire->nom;
			
// 			/* Type de la vid�o */
// 			$this->type = $video->type;
// 			if ($this->type != 'flv' && $this->type != 'videointegree' && $this->type != 'url') {
// 				$this->type = 'flv';
// 			}
			
			/* Type de la vid�o */
			$this->type = $video->type;
			
			if ($this->type === 'template') {
			
				$this->values = unserialize($video->valeur);
				$template = multimediaspace_getVideoTemplate($this->values['template']);
				$this->parameters = multimediaspace_extractVideoTemplateParameters($template['parameters']);
			
			} elseif ($this->type != 'flv' && $this->type != 'videointegree' && $this->type != 'url') {
				$this->type = 'flv';
			}
			
			
			
			$this->jouerautomatiquement = $video->jouerautomatiquement;
			
			$this->aide = multimediaspace_traduire("This field must be informed");
			$this->enregistrer = multimediaspace_traduire("Save");
			$this->tchampnonrenseigne = multimediaspace_traduire("An obligatory field is not informed");
			$this->tchampnonentier = multimediaspace_traduire("The value must be an integer");
			$this->trepertoire = multimediaspace_traduire('Folder');
			$this->tvideo = multimediaspace_traduire('Video');
			$this->tnom = multimediaspace_traduire('Name');
			$this->tdescription = multimediaspace_traduire('Description');
			$this->tlargeurlecteur = multimediaspace_traduire('Width of the video player in pixels');
			$this->thauteurlecteur = multimediaspace_traduire('Height of the video player in pixels');
			$this->timage = multimediaspace_traduire('Associated image');
			$this->toptionnelle = multimediaspace_traduire('(optional)');
			$this->tvaleurvideointegree = multimediaspace_traduire('Html/Javascript code');
			$this->tvaleururl = multimediaspace_traduire('URL extern');
			$this->tjouerautomatiquement = multimediaspace_traduire('Play automatically the video after its load');
			$this->tnon = multimediaspace_traduire('No');
			$this->toui = multimediaspace_traduire('Yes');
			
			//Emp�che la saisie
			$this->antif5 = multimediaspace_antif5valeur();
		}

		
		public function nextParameter()
		{
			$parameter = each($this->parameters);
			if (!$parameter) {
				reset($this->parameters);
				return false;
			}

			$this->parameter_name = bab_toHtml('valeur[' . $parameter['key'] . ']');
			$this->parameter_description = bab_toHtml($parameter['value']);
			$this->parameter_value = bab_toHtml($this->values[$parameter['key']]);
			return true;
		}

	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_editionvideo($donnees, $erreurs);
	multimediaspace_template($a, 'userpages.html', 'editionvideo');
}




/* Enregistrement de la modification de la vid�o */
function multimediaspace_editionvideoenr($donnees, &$erreurs)
{
	$idvideo = $donnees['idvideo'];
	$video = new Multimediaspace_Video($idvideo);
	$video->charge();
	$video->nom = $donnees['nom'];
	$video->description = $donnees['description'];
	/* Type vid�o int�gr�e */
	if ($video->type == 'videointegree') {
		$video->valeur = $donnees['valeur'];
	}
	/* Type URL : on r�cup�re l'url */
	if ($video->type == 'url') {
		$video->valeur = trim($donnees['valeur']); //trim supprime les espaces en d�but et fin de cha�ne
		/* Traitement de l'url dans le cas o� http:// n'existe pas */
		if (substr($video->valeur, 0, 3) == 'www') {
			/* On ajoute http:// pour ces adresses */
			$video->valeur = 'http://'.$video->valeur;
		} else {
			if (substr($video->valeur, 0, 4) != 'http') {
				/* On ajoute http:// */
				$video->valeur = 'http://'.$video->valeur;
			}
		}
	}
	
	/* Cr�ation des r�pertoires de destination s'ils n'existent pas */
	$resc = array(); /* R�sultats de cr�ation des r�pertoires */
	$rescchemin = array();
	$resc[] = multimediaspace_creationrepertoire(multimediaspace_cheminuploadmodule());
	$rescchemin[] = multimediaspace_cheminuploadmodule();
	$resc[] = multimediaspace_creationrepertoire(multimediaspace_cheminuploadmodule().'images');
	$rescchemin[] = multimediaspace_cheminuploadmodule().'images';
	$resc[] = multimediaspace_creationrepertoire(multimediaspace_chemintemplatesmodule().'videos');
	$rescchemin[] = multimediaspace_chemintemplatesmodule().'videos';
	/* R�pertoire de destination */
	$repertoire = new Multimediaspace_Repertoire($video->idrepertoire);
	$repertoire->charge();
	$ssrep = $repertoire->repertoiresparents();
	for ($i=0;$i<=count($ssrep)-1;$i++) {
		/* R�pertoires d'upload des r�pertoires parents */
		$cheminfinalvideo = $ssrep[$i]->cheminrepertoirevideos();
		$cheminfinalimage = $ssrep[$i]->cheminrepertoireimages();
		$resc[] = multimediaspace_creationrepertoire($cheminfinalvideo);
		$rescchemin[] = $cheminfinalvideo;
		$resc[] = multimediaspace_creationrepertoire($cheminfinalimage);
		$rescchemin[] = $cheminfinalimage;
	}
	/* R�pertoire de destination */
	$cheminfinalvideo = $repertoire->cheminrepertoirevideos();
	$cheminfinalimage = $repertoire->cheminrepertoireimages();
	$resc[] = multimediaspace_creationrepertoire($cheminfinalvideo);
	$rescchemin[] = $cheminfinalvideo;
	$resc[] = multimediaspace_creationrepertoire($cheminfinalimage);
	$rescchemin[] = $cheminfinalimage;
	$erreur = false;
	for ($i=0;$i<=count($resc)-1;$i++) {
		if (!$resc[$i]) {
			$erreur = true;
		}
	}
	if ($erreur) {
		bab_debug('Les r�pertoires de destination de la vid�o et de l\'image associ�e n\'ont pas �t� cr��s');
	}
	/* Ajoute un fichier index.html dans le r�pertoire afin d'�viter que le contenu du r�pertoire soit lu */
	if (!multimediaspace_existefichier($cheminfinalimage.'/index.html')) {
		@fopen($cheminfinalimage.'/index.html', 'x');
	}
	if (!multimediaspace_existefichier($cheminfinalvideo.'/index.html')) {
		@fopen($cheminfinalvideo.'/index.html', 'x');
	}
	
	/* Type FLV : on r�cup�re le fichier vid�o */
	$nomfichiervideo = '';
	if ($video->type == 'flv') {
		if (multimediaspace_fichierdepose('video')) {
			$res = array();
			if($res = multimediaspace_deplacefichiertmp('video', $cheminfinalvideo.'/', true)) {
				if ($res['erreur']) {
					$erreurs[] = multimediaspace_traduire("The file don't have can be attached");
				} else {
					/* On supprime l'ancienne vid�o si y'en avait une */
					$video->supprimevideo();
					/* R�cup�re les donn�es de la nouvelle video */
					$nomfichiervideo = $res['nomfichier'];		
					$video->nomfichiervideo = $res['nomfichier'];
					/* On renomme le fichier s'il contient des accents */
					$nomfichiervideosansaccents = multimediaspace_chainesansaccents($nomfichiervideo);
					if ($nomfichiervideo != $nomfichiervideosansaccents) {
						$resrenomme = multimediaspace_renommefichier($cheminfinalvideo, $nomfichiervideo, $nomfichiervideosansaccents, true);
						if ($resrenomme['erreur'] != false) {
							$video->nomfichiervideo = $resrenomme['nomfichier'];
						}
					}
				}
			}
		}
	}
	
	if ($video->type == 'flv') {
		/* On r�cup�re la largeur et la hauteur du lecteur */
		$video->largeur = $donnees['largeur'];
		$video->hauteur = $donnees['hauteur'];
		$video->largeurlecteur = $donnees['largeur'];
		$video->hauteurlecteur = $donnees['hauteur'];
	}

	if ($video->type == 'template') {
		$valeur = unserialize($video->valeur);
		
		foreach ($donnees['valeur'] as $valueKey => $val) {
			$valeur[$valueKey] = $val;
		}
		$video->valeur = serialize($valeur);
	}
	
	

	/* Option jouer automatiquement */
	if ($donnees['jouerautomatiquement'] == 2) {
		$video->jouerautomatiquement = 2;
	} else {
		$video->jouerautomatiquement = 1;
	}
	
	/* R�cup�re le fichier image */
	if (multimediaspace_fichierdepose('image')) {
		$res = array();
		if($res = multimediaspace_deplacefichiertmp('image', $cheminfinalimage.'/', true)) {
			if ($res['erreur']) {
				$erreurs[] = multimediaspace_traduire("The file don't have can be attached");
			} else {			
				/* R�cup�re les donn�es de la nouvelle video */
				$donneesimg = multimediaspace_donneesfichierdepose('image');
				$nomfichierminiature = $res['nomfichier'];
				$taille = multimediaspace_tailleimage($cheminfinalimage.'/'.$nomfichierminiature);
				$typeminiature = multimediaspace_typeimage($cheminfinalimage.'/'.$nomfichierminiature);
				$largeurminiature = $taille['largeur'];
				$hauteurminiature = $taille['hauteur'];
				/* On supprime l'ancienne image si y'en avait une */
				$video->supprimeminiature();
				/* On ajoute la nouvelle image */
				$video->nomfichierminiature = $nomfichierminiature;
				$video->typeminiature = $typeminiature;
				$video->largeurminiature = $largeurminiature;
				$video->hauteurminiature = $hauteurminiature;				
			}
		}
	} else {
		/* On v�rifie si la case � cocher Supprimer l'image a �t� coch�e */
		if ($donnees['supprimerimage'] == 'supprimer') {
			$video->supprimeminiature();
		}
	}
	
	$video->enregistre();
	$erreurs[] = 'Save done';
}

/* Affiche la liste des r�pertoires parents */
function multimediaspace_accueilespacemultimedia(&$erreurs) {
	global $babBody;

	class multimediaspace_accueilespacemultimedia
	{
		//Variables

		function multimediaspace_accueilespacemultimedia($erreurs) {
			/* Liste des sous-r�pertoires */
			$espacemultimedia = new Multimediaspace_EspaceMultimedia();
			$this->repertoirestab = $espacemultimedia->repertoires();
			$this->repertoires = array();
			for($i=0;$i<=count($this->repertoirestab)-1;$i++) {
				if ($this->repertoirestab[$i]->adroitslecture(true)) {
					$this->repertoires[] = $this->repertoirestab[$i];
				}
			}
			$this->compteur = 0;
		}
		
		function liste() {
			if ($this->compteur > count($this->repertoires) - 1) {
				return false;
			}
			$this->id = $this->repertoires[$this->compteur]->id;
			$this->nom = $this->repertoires[$this->compteur]->nom;
			$this->nbvideos = $this->repertoires[$this->compteur]->nbvideos(true);
			global $babAddonUrl;
			$this->url = $babAddonUrl.'user&amp;idx=accesrepertoire&amp;idrepertoire='.$this->id;
			$this->compteur++;
			return true;
		}
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_accueilespacemultimedia($erreurs);
	multimediaspace_template($a, "userpages.html", "accueilespacemultimedia");
}

/* Affiche la liste des vid�os d'un r�pertoire */
function multimediaspace_accesrepertoire($donnees, &$erreurs) {
	global $babBody;

	class multimediaspace_accesrepertoire
	{
		//Variables

		function multimediaspace_accesrepertoire($donnees, $erreurs) {
			$this->tpar = multimediaspace_traduire('by');
			$this->tnblecture = multimediaspace_traduire('Viewed:');
			$this->tnblecture2 = multimediaspace_traduire('times');
			
			$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
			$repertoire->charge();
			
			/* Liste des r�pertoires parents */
			global $babAddonUrl;
			$this->chemin = '<a href="'.$babAddonUrl.'user&amp;idx=utilisateur'.'">'.multimediaspace_traduire('Multimedia space').'</a>';
			$repertoiresparents = $repertoire->repertoiresparents(false);
			for($i=0;$i<=count($repertoiresparents)-1;$i++) {
				$this->chemin .= '&nbsp;/&nbsp;<a href="'.$repertoiresparents[$i]->url.'">'.$repertoiresparents[$i]->nom.'</a>';
			}
			$espacemultimedia = new Multimediaspace_EspaceMultimedia();
			$repertoireracine = $espacemultimedia->repertoireparent();
			if ($repertoire->id != $repertoireracine->id) {
				$this->chemin .= '&nbsp;/&nbsp;'.$repertoire->nom;
			}
			
			/* Liste des sous-r�pertoires */
			$this->sousrepertoirestab = $repertoire->sousrepertoires(false);
			$this->sousrepertoires = array();
			for($i=0;$i<=count($this->sousrepertoirestab)-1;$i++) {
				if ($this->sousrepertoirestab[$i]->adroitslecture(true)) {
					$this->sousrepertoires[] = $this->sousrepertoirestab[$i];
				}
			}
			$this->compteursousrepertoires = 0;
			
			/* Liste des vid�os du r�pertoire */
			$this->videos = array();
			if ($repertoire->adroitslecture()) {
				$this->videos = $repertoire->videos();
			}
			$this->compteurvideos = 0;
		}
		
		function listesousrepertoires() {
			if ($this->compteursousrepertoires > count($this->sousrepertoires) - 1) {
				return false;
			}
			$this->id = $this->sousrepertoires[$this->compteursousrepertoires]->id;
			$this->nom = $this->sousrepertoires[$this->compteursousrepertoires]->nom;
			$this->nbvideos = $this->sousrepertoires[$this->compteursousrepertoires]->nbvideos(true);
			$this->url = $this->sousrepertoires[$this->compteursousrepertoires]->url;
			$this->compteursousrepertoires++;
			return true;
		}
		
		function listevideos() {
			if ($this->compteurvideos > count($this->videos) - 1) {
				return false;
			}
			$this->id = $this->videos[$this->compteurvideos]->id;
			$this->nom = $this->videos[$this->compteurvideos]->nom;
			$this->description = nl2br($this->videos[$this->compteurvideos]->description);
			$this->date = multimediaspace_formatedate($this->videos[$this->compteurvideos]->date);
			$idauteur = $this->videos[$this->compteurvideos]->auteur;
			$ficheannuaire = multimediaspace_ficheannuaire($idauteur);
			$this->auteur = $ficheannuaire['givenname'].' '.$ficheannuaire['sn'];
			$this->urlminiature = $this->videos[$this->compteurvideos]->urlminiature;
			/* Si type url ou videointegree, on affiche la page d�di�e sinon un clic sur l'image renvoit sur l'url externe */
			if ($this->videos[$this->compteurvideos]->type == 'url') {
				$this->lienennouvellefenetre = true;
				$this->url = $this->videos[$this->compteurvideos]->valeur;
			} else {
				$this->lienennouvellefenetre = false;
				$this->url = $this->videos[$this->compteurvideos]->url;
			}
			$this->nblecture = $this->videos[$this->compteurvideos]->nblecture;
			$repertoireparent = $this->videos[$this->compteurvideos]->repertoire();
			if ($this->videos[$this->compteurvideos]->type == 'url') {
				$this->affichernblecture = false;
			} else {
				$this->affichernblecture = true;
			}
			$this->compteurvideos++;
			return true;
		}
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_accesrepertoire($donnees, $erreurs);
	multimediaspace_template($a, "userpages.html", "accesrepertoire");
}

/* Affiche la miniature d'une vid�o */
function multimediaspace_voirminiature($donnees, &$erreurs) {
	$video = new Multimediaspace_Video($donnees['idvideo']);
	$video->charge();
	$largeurvoulue = 100;
	$hauteurvoulue = 100;
	if ($donnees['largeurvoulue'] != '' && $donnees['hauteurvoulue'] != '') {
		$largeurvoulue = $donnees['largeurvoulue'];
		$hauteurvoulue = $donnees['hauteurvoulue'];
	}
	if ($video->nomfichierminiature != '') {
		multimediaspace_afficheimage($video->cheminminiature(), $video->largeurminiature, $video->hauteurminiature, $largeurvoulue, $hauteurvoulue, false, '');
	} else {
		/* Il n'y a pas d'image associ�e � la vid�o */
		$cheminimage = multimediaspace_urltemplatesmodule().'images/miniaturegenerique.jpg';
		multimediaspace_afficheimage($cheminimage, 150, 120, $largeurvoulue, $hauteurvoulue, false, 'jpeg');
	}
}

/* Affiche une vid�o */
function multimediaspace_voirvideo($donnees, &$erreurs) {
	global $babBody;

	class multimediaspace_voirvideo
	{
		//Variables

		function multimediaspace_voirvideo($donnees, $erreurs) {
			$this->tpar = multimediaspace_traduire('by');
			$this->tnblecture = multimediaspace_traduire('Viewed:');
			$this->tnblecture2 = multimediaspace_traduire('times');
			$this->ttelechargeflashplayer = multimediaspace_traduire('Get Flash Player');
			
			/* R�cup�ration des donn�es de la vid�o */
			$video = new Multimediaspace_Video($donnees['idvideo']);
			$video->charge();
			
			$this->id = $video->id;
			$this->nomvideo = $video->nom;
			$this->description = nl2br($video->description);
			$this->date = multimediaspace_formatedate($video->date);
			$idauteur = $video->auteur;
			$ficheannuaire = multimediaspace_ficheannuaire($idauteur);
			$this->auteur = $ficheannuaire['givenname'].' '.$ficheannuaire['sn'];
			$this->nblecture = $video->nblecture;
			$this->codehtmlvideo = $video->codehtml();
			
			$repertoire = new Multimediaspace_Repertoire($video->idrepertoire);
			$repertoire->charge();
			
			/* Liste des r�pertoires parents */
			$this->chemin = '';
			$repertoiresparents = $repertoire->repertoiresparents(false);
			for($i=0;$i<=count($repertoiresparents)-1;$i++) {
				if ($i == 0) {
					$this->chemin .= '<a href="'.$repertoiresparents[$i]->url.'">'.$repertoiresparents[$i]->nom.'</a>';
				} else {
					$this->chemin .= '&nbsp;/&nbsp;<a href="'.$repertoiresparents[$i]->url.'">'.$repertoiresparents[$i]->nom.'</a>';
				}
			}
			if ($this->chemin != '') {
				$this->chemin .= '&nbsp;/&nbsp;<a href="'.$repertoire->url.'">'.$repertoire->nom.'</a>';
			} else {
				$this->chemin .= '<a href="'.$repertoire->url.'">'.$repertoire->nom.'</a>';
			}
			
			/* Lien retour */
			$this->tretour = multimediaspace_traduire('Back');
			$this->urlretour = $repertoire->url;
			
			/* Liste des sous-r�pertoires */
			$this->sousrepertoirestab = $repertoire->sousrepertoires(false);
			$this->sousrepertoires = array();
			for($i=0;$i<=count($this->sousrepertoirestab)-1;$i++) {
				if ($this->sousrepertoirestab[$i]->adroitslecture(true)) {
					$this->sousrepertoires[] = $this->sousrepertoirestab[$i];
				}
			}
			$this->compteursousrepertoires = 0;
			
			/* Met � jour les statistiques */
			$video->nblecture += 1;
			$video->enregistre();
		}
		
		function listesousrepertoires() {
			if ($this->compteursousrepertoires > count($this->sousrepertoires) - 1) {
				return false;
			}
			$this->id = $this->sousrepertoires[$this->compteursousrepertoires]->id;
			$this->nom = $this->sousrepertoires[$this->compteursousrepertoires]->nom;
			$this->nbvideos = $this->sousrepertoires[$this->compteursousrepertoires]->nbvideos(true);
			$this->url = $this->sousrepertoires[$this->compteursousrepertoires]->url;
			$this->compteursousrepertoires++;
			return true;
		}
		
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_voirvideo($donnees, $erreurs);
	multimediaspace_template($a, "userpages.html", "voirvideo");
}

/* T�l�charge une vid�o */
function multimediaspace_telechargevideo($donnees, &$erreurs) {			
	/* R�cup�ration des donn�es de la vid�o */
	$video = new Multimediaspace_Video($donnees['idvideo']);
	$video->charge();
	multimediaspace_telechargefichier($video->cheminvideo());
}

/* Affiche la liste des r�pertoires dans la popup des fonctions d'Ovidentia dans l'�diteur wysiwyg */
function multimediaspace_editeurwysiwyglisterepertoires(&$erreurs) {
	global $babBody;

	class multimediaspace_editeurwysiwyglisterepertoires
	{
		//Variables

		function multimediaspace_editeurwysiwyglisterepertoires($erreurs) {
			$this->tselection = multimediaspace_traduire('Select the folder in which is the video');
			$this->tretour = multimediaspace_traduire('Back');
			
			/* R�cup�re la liste des r�pertoires : sera r�cursif mais pour l'instant g�re seulement 5 niveaux */
			$this->repertoires = array();
			$this->optionsrepertoires = array();
			$this->niveauxrepertoires = array();
			$espacemultimedia = new Multimediaspace_EspaceMultimedia();
			/* R�pertoire parent */
			$this->repertoires[] = $espacemultimedia->repertoireparent();
			$this->niveauxrepertoires[] = 0;
			$this->optionsrepertoires[] = array(true, false, false, true, false, 0);
			$repertoiresniveau1 = $espacemultimedia->repertoires(false);
			for($i=0;$i<=count($repertoiresniveau1)-1;$i++) {
				$this->repertoires[] = $repertoiresniveau1[$i];
				$this->niveauxrepertoires[] = 1;
				$repertoiresniveau2 = $repertoiresniveau1[$i]->sousrepertoires(false);
				for($j=0;$j<=count($repertoiresniveau2)-1;$j++) {
					$this->repertoires[] = $repertoiresniveau2[$j];
					$this->niveauxrepertoires[] = 2;
					$repertoiresniveau3 = $repertoiresniveau2[$j]->sousrepertoires(false);
					for($k=0;$k<=count($repertoiresniveau3)-1;$k++) {
						$this->repertoires[] = $repertoiresniveau3[$k];
						$this->niveauxrepertoires[] = 3;
						$repertoiresniveau4 = $repertoiresniveau3[$k]->sousrepertoires(false);
						for($l=0;$l<=count($repertoiresniveau4)-1;$l++) {
							$this->repertoires[] = $repertoiresniveau4[$l];
							$this->niveauxrepertoires[] = 4;
							$repertoiresniveau5 = $repertoiresniveau4[$l]->sousrepertoires(false);
							for($m=0;$m<=count($repertoiresniveau5)-1;$m++) {
								$this->repertoires[] = $repertoiresniveau5[$m];
								$this->niveauxrepertoires[] = 5;
							}
						}
					}
				}
			}
			$this->compteur = 0;
		}
		
		function liste() {
			if ($this->compteur > count($this->repertoires) - 1) {
				return false;
			}
			$this->id = $this->repertoires[$this->compteur]->id;
			$this->nom = $this->repertoires[$this->compteur]->nom;
			$this->nbvideos = $this->repertoires[$this->compteur]->nbvideos();
			$this->decalage = ''; /* Permet d'identifier l'arborescence */
			for ($i=0;$i<=$this->niveauxrepertoires[$this->compteur]-1;$i++) {
				$this->decalage .= '&nbsp;&nbsp;&nbsp;';
			}
			/* Nombre de videos */
			$this->nbvideos = $this->repertoires[$this->compteur]->nbvideos();
			if ($this->nbvideos > 1) {
				$this->tnbvideos = multimediaspace_traduire('videos');
			} else {
				$this->tnbvideos = multimediaspace_traduire('video');
			}
			/* Lien sur le nom du r�pertoire */
			global $babAddonUrl;
			$this->urlselectionrepertoire = $babAddonUrl.'user&amp;idx=editeurwysiwyglistevideos&amp;idrepertoire='.$this->id;
			$this->compteur++;
			return true;
		}
		
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_editeurwysiwyglisterepertoires($erreurs);
	multimediaspace_template($a, "userpages.html", "editeurwysiwyglisterepertoires", true);
}

/* Affiche la liste des vid�os d'un r�pertoire dans la popup des fonctions d'Ovidentia dans l'�diteur wysiwyg */
function multimediaspace_editeurwysiwyglistevideos($donnees, &$erreurs) {
	global $babBody;

	class multimediaspace_editeurwysiwyglistevideos
	{
		//Variables

		function multimediaspace_editeurwysiwyglistevideos($donnees, $erreurs) {
			$this->tselection = multimediaspace_traduire('Select the video');
			$repertoire = new Multimediaspace_Repertoire($donnees['idrepertoire']);
			$repertoire->charge();
			$this->videos = $repertoire->videos('date','croissant');
			if (count($this->videos) == 0) {
				$this->afficheaucunevideo = true;
			} else {
				$this->afficheaucunevideo = false;
			}
			$this->taucunevideo = multimediaspace_traduire('The folder does not contain videos');
			/* Lien de retour aux r�pertoires */
			$this->tretour = multimediaspace_traduire('Back');
			global $babAddonUrl;
			$this->urlretour = $babAddonUrl.'user&amp;idx=editeurwysiwyglisterepertoires';
			$this->compteur = 0;
		}
		
		function liste() {
			if ($this->compteur > count($this->videos) - 1) {
				return false;
			}
			$this->id = $this->videos[$this->compteur]->id;
			$this->nom = $this->videos[$this->compteur]->nom;
			$this->date = multimediaspace_formatedatetime($this->videos[$this->compteur]->date);
			$this->type = '';
			switch($this->videos[$this->compteur]->type) {
				case 'flv':
					$this->type = multimediaspace_traduire('File FLV');
					break;
				case 'url':
					$this->type = multimediaspace_traduire('URL extern');
					break;
				case 'videointegree':
					$this->type = multimediaspace_traduire('Integrated video');
					break;
			}
			global $babAddonUrl;
			$this->urlvideo = $babAddonUrl.'user&amp;idx=editeurwysiwygvideo&amp;idvideo='.$this->id;
			$this->urlimage = $babAddonUrl.'user&idx=voirminiature&idvideo='.$this->id.'&largeurvoulue=60&hauteurvoulue=60';
			$this->compteur++;
			return true;
		}
		
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_editeurwysiwyglistevideos($donnees, $erreurs);
	multimediaspace_template($a, "userpages.html", "editeurwysiwyglistevideos", true);
}

/* Affiche les possibilit�s d'ajout de la vid�o dans la popup des fonctions d'Ovidentia dans l'�diteur wysiwyg */
function multimediaspace_editeurwysiwygvideo($donnees, &$erreurs) {
	global $babBody;

	class multimediaspace_editeurwysiwygvideo
	{
		//Variables

		function multimediaspace_editeurwysiwygvideo($donnees, $erreurs) {
			$this->tselection = multimediaspace_traduire('Select the method of integration of the video');
			$video = new Multimediaspace_Video($donnees['idvideo']);
			$video->charge();
			$repertoire = new Multimediaspace_Repertoire($video->idrepertoire);
			$repertoire->charge();
			
			$this->tmodeimage = multimediaspace_traduire('Image mode');
			$this->tmodeimage2 = multimediaspace_traduire('The image associated with the video is deposited in the wysiwyg editor.
In the display of the contents, the image will be clickable and will send towards the reading of the video.');
			$this->tmodevideo = multimediaspace_traduire('Video mode');
			$this->tmodevideo2 = multimediaspace_traduire('The video is deposited in the wysiwyg editor. In the display of the contents, the reader of the video will appear.');
			$this->tmodeovml = multimediaspace_traduire('OVML mode');
			$this->tmodeovml2 = multimediaspace_traduire('This mode inserts the call to a code OVML into the editor. The file OVML personalizes the display of the video in the contents.<br />The following parameters are spent in the file OVML:<br />idvideo: identifier (integer) of the video');
			
			/* Code html pour int�grer l'image */
			global $babAddonUrl;
			$urlimage = multimediaspace_urlsansnomdedomaine($babAddonUrl.'user&idx=voirminiature&idvideo='.$video->id.'&largeurvoulue=150&hauteurvoulue=150');
			
			$this->codevideoimage = '<br /><div><a href="'.$video->url.'"><img src="'.$urlimage.'" /></a></div>';
			$this->codevideoimage = str_replace("'", "\'", $this->codevideoimage);
			$this->codevideoimage = str_replace("\\", "\\\\", $this->codevideoimage);
			
			$this->codevideovideo = '';
			if ($video->type == 'flv' || $video->type == 'videointegree') {
				$code = '<br /><div>'.$video->codehtml().'</div>';
				/* Suppression des retours � la ligne pour l'ajout dans une cha�ne javascript */
				$code2 = explode("\n", $code);
				foreach ($code2 as $texte) {
					$this->codevideovideo .= trim($texte);
				}
			}
			$this->codevideovideo = str_replace("'", "\'", $this->codevideovideo);
			$this->codevideovideo = str_replace("\\", "\\\\", $this->codevideovideo);
			
			if ($video->type == 'url') {
				$this->affichemodevideo = false;
			} else {
				$this->affichemodevideo = true;
			}
			
			/* Mode OVML */
			$this->affichemodeovml = false;
			$this->fichiersovml = multimediaspace_fichiersovml();
			if (count($this->fichiersovml) > 0) {
				$this->affichemodeovml = true;
			}
			$this->codevideoovml = '';
			$this->idvideo = $video->id;
			$this->idrepertoire = $video->idrepertoire;
			$this->compteur = 0;
			
			/* Lien de retour aux r�pertoires */
			$this->tretour = multimediaspace_traduire('Back');
			global $babAddonUrl;
			$this->urlretour = $babAddonUrl.'user&amp;idx=editeurwysiwyglistevideos&amp;idrepertoire='.$video->idrepertoire;
		}
		
		function liste() {
			if ($this->compteur > count($this->fichiersovml) - 1) {
				$this->compteur = 0;
				return false;
			}
			$this->id = $this->fichiersovml[$this->compteur]['id'];
			$this->nomfichierovml = basename($this->fichiersovml[$this->compteur]['cheminfichierovml']);
			$this->description = $this->fichiersovml[$this->compteur]['description'];
			$this->codevideoovml = '$OVML('.$this->fichiersovml[$this->compteur]['cheminfichierovml'].',idvideo='.$this->idvideo.')';
			$this->codevideoovml = str_replace("'", "\'", $this->codevideoovml);
			$this->codevideoovml = str_replace("\\", "\\\\", $this->codevideoovml);
			$this->compteur++;
			return true;
		}
		
	}
	//Cr�ation de l'objet et appel de la fonction babPrintTemplate
	$a = new multimediaspace_editeurwysiwygvideo($donnees, $erreurs);
	multimediaspace_template($a, "userpages.html", "editeurwysiwygvideo", true);
}

