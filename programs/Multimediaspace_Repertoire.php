<?php
class Multimediaspace_Repertoire {
	
	/* Variables */
	var $id; /* Id dans la base de donn�es */
	var $nom;
	var $chemin; /* valeur g�n�r�e : elle n'est pas enregistr�e en base de donn�es : chemin dans le syst�me de fichiers par rapport au r�pertoire d'upload du module */
	var $bornegauche; /* borne gauche dans la repr�sentation intervallaire des r�pertoires (une feuille est aussi un r�pertoire) */
	var $bornedroite; /* borne droite dans la repr�sentation intervallaire des r�pertoires (une feuille est aussi un r�pertoire) */
	var $heritedesdroits; /* 1 (oui) ou 2 (non) */
	var $commentairesactifs; /* 1 (actif) ou 2 (inactif) */
	var $url; /* valeur g�n�r�e : elle n'est pas enregistr�e en base de donn�es */
	var $idrepertoire;
	
	/* Constructeur */
	function __construct($id='', $nom='', $bornegauche='', $bornedroite=0, $heritedesdroits=2, $commentairesactifs=2) {
		$this->id = $id;
		$this->nom = $nom;
		$this->bornegauche = $bornegauche;
		$this->bornedroite = $bornedroite;
		$this->heritedesdroits = $heritedesdroits;
		if ($this->heritedesdroits != 1 && $this->heritedesdroits != 2) {
			$this->heritedesdroits = 2;
		}
		$this->commentairesactifs = $commentairesactifs;
		if ($this->commentairesactifs != 1 && $this->commentairesactifs != 2) {
			$this->commentairesactifs = 2;
		}
		if ($this->id != '') {
			global $babAddonUrl;
			$this->url = $babAddonUrl.'user&amp;idx=accesrepertoire&amp;idrepertoire='.$this->id;
		}
		$this->chemin = '';
		/* Si c'est le r�pertoire parent, on traduit son nom */
		if ($this->bornegauche == 1) {
			$this->nom = multimediaspace_traduire('Multimedia space');
		}
	}
	
	/* Charge les donn�es enregistr�es dans la table de donn�es dans l'objet */
	function charge() {
		$requete = "select `id`, `nom`, `bornegauche`, `bornedroite`, `heritedesdroits`, `commentairesactifs` from `".MULTIMEDIASPACE_REPERTOIRE."` where id='".$this->id."'";
		$idrequete = 0;
		$erreurs = array();
		$res = multimediaspace_sql($requete, $erreurs, $idrequete);
		if (count($res) == 1) {
			$this->nom = $res[0]['nom'];
			$this->bornegauche = $res[0]['bornegauche'];
			$this->bornedroite = $res[0]['bornedroite'];
			$this->heritedesdroits = $res[0]['heritedesdroits'];
			if ($this->heritedesdroits != 1 && $this->heritedesdroits != 2) {
				$this->heritedesdroits = 2;
			}
			$this->commentairesactifs = $res[0]['commentairesactifs'];
			if ($this->commentairesactifs != 1 && $this->commentairesactifs != 2) {
				$this->commentairesactifs = 2;
			}
			global $babAddonUrl;
			$this->url = $babAddonUrl.'user&amp;idx=accesrepertoire&amp;idrepertoire='.$this->id;
		}
	}
	
	/* Enregistre/Modifie le r�pertoire */
	function enregistre($idrepertoireparent='') { //$idrepertoireparent utilis� pour la cr�ation du r�pertoire
		if ($this->id == '') {
			/* On cr�� le nouveau r�pertoire */
			$nom = $this->nom;
			$heritedesdroits = $this->heritedesdroits;
			$commentairesactifs = $this->commentairesactifs;
			if ($heritedesdroits != 1 && $heritedesdroits != 2) {
				$heritedesdroits = 2;
				$this->heritedesdroits = 2;
			}
			if ($commentairesactifs != 1 && $commentairesactifs != 2) {
				$commentairesactifs = 2;
				$this->commentairesactifs = 2;
			}
			/* Calcul des bornes gauche et droite */
			/* Aide : 35 est la borne droite du p�re
				UPDATE NEW_FAMILLE SET NFM_BD = NFM_BD + 2 WHERE NFM_BD >= 35
				UPDATE NEW_FAMILLE SET NFM_BG = NFM_BG + 2 WHERE NFM_BG >= 35
				INSERT INTO NEW_FAMILLE (NFM_BG, NFM_BD, NFM_LIB) VALUES (35, 36, 'Roller')
			*/
			$repertoireparent = new Multimediaspace_Repertoire($idrepertoireparent);
			$repertoireparent->charge();
			$bornegauche = $repertoireparent->bornedroite;
			$bornedroite = $bornegauche + 1;
			multimediaspace_echappesql($nom);
			multimediaspace_echappesql($heritedesdroits);
			multimediaspace_echappesql($commentairesactifs);
			multimediaspace_echappesql($bornegauche);
			multimediaspace_echappesql($bornedroite);
			$requete = "update `".MULTIMEDIASPACE_REPERTOIRE."` set `bornedroite` = `bornedroite` + 2 where `bornedroite` >= ".$bornegauche;
			$idrequete = 0;
			multimediaspace_sql($requete, $erreurs, $idrequete);
			if (count($erreurs) == 0) {
				$requete = "update `".MULTIMEDIASPACE_REPERTOIRE."` set `bornegauche` = `bornegauche` + 2 where `bornegauche` >= ".$bornegauche;
				$idrequete = 0;
				multimediaspace_sql($requete, $erreurs, $idrequete);
				if (count($erreurs) == 0) {
					$requete = "insert into `".MULTIMEDIASPACE_REPERTOIRE."` ( `nom` , `bornegauche` , `bornedroite` , `heritedesdroits` , `commentairesactifs` )
							  values ( '$nom' , '$bornegauche' , '$bornedroite' , '$heritedesdroits' , '$commentairesactifs' )
							";
					$idrequete = 0;
					multimediaspace_sql($requete, $erreurs, $idrequete);
					if (count($erreurs) == 0) {
						$this->id = multimediaspace_sqlid($idrequete);
						$erreurs[] = "Save done";
					}
				}
			}
		} else {
			/* On met � jour : on ne fait pas de d�placement, on modifie seulement le nom et les options */
			$id = $this->id;
			$nom = $this->nom;
			$heritedesdroits = $this->heritedesdroits;
			$commentairesactifs = $this->commentairesactifs;
			if ($heritedesdroits != 1 && $heritedesdroits != 2) {
				$heritedesdroits = 2;
				$this->heritedesdroits = 2;
			}
			if ($commentairesactifs != 1 && $commentairesactifs != 2) {
				$commentairesactifs = 2;
				$this->commentairesactifs = 2;
			}
			multimediaspace_echappesql($id);
			multimediaspace_echappesql($nom);
			multimediaspace_echappesql($heritedesdroits);
			multimediaspace_echappesql($commentairesactifs);
			
			$requete = "update `".MULTIMEDIASPACE_REPERTOIRE."` set `nom`='".$nom."', `heritedesdroits`='".$heritedesdroits."', `commentairesactifs`='".$commentairesactifs."' where id='".$id."'";
			multimediaspace_sql($requete, $erreurs, $idrequete="");
			if (count($erreurs) == 0) {
				$erreurs[] = "Save done";
			}
		}
	}
	
	/* Renvoie la liste des sous-r�pertoires */
	function sousrepertoires($prendreencomptelessousniveaux=false) {
		/*
		Aide :
		- Tous les �l�ments d�pendant d'un �l�ment de r�f�rence (sous arbre) :
			select * from   NEW_FAMILLE where  NFM_BG > 22   and NFM_BD < 35 order by NFM_BG asc
		- Tous les p�res d'un �l�ment de r�f�rence :
			select * from   NEW_FAMILLE where  NFM_BG < 29   and NFM_BD > 34 order by NFM_BG asc
		*/
		
		$ssrep = array();
		/* R�cup�re tous les r�pertoires enfants (sous-r�pertoires et sous-sous-r�pertoires...) */
		$requete = "select * from `".MULTIMEDIASPACE_REPERTOIRE."` where `bornegauche` > '".$this->bornegauche."' and `bornedroite` < '".$this->bornedroite."' order by bornegauche asc";
		$idrequete = 0;
		$erreurs = array();
		$res = multimediaspace_sql($requete, $erreurs, $idrequete);
		for($i=0;$i<=count($res)-1;$i++) {
			$repertoire = new Multimediaspace_Repertoire($res[$i]['id'],$res[$i]['nom'],$res[$i]['bornegauche'],$res[$i]['bornedroite'],$res[$i]['heritedesdroits'],$res[$i]['commentairesactifs']);
			$ssrep[] = $repertoire;
		}
		
		if (!$prendreencomptelessousniveaux) {
			/* Parcourt les r�sultats et recherche des noeuds (pas des feuilles). Pour avoir uniquement les r�pertoires
			   du niveau juste en-dessous, on supprime les enfants de ses sous-r�pertoires */
			$trouve = true;
			while ($trouve) { /* Tant qu'on trouve des r�pertoires � supprimer */
				$trouve = false;
				for($i=0;$i<=count($ssrep)-1;$i++) {
					/* R�cup�re les noeuds */
					if ($ssrep[$i]->bornedroite - $ssrep[$i]->bornegauche > 1) {
						for($j=0;$j<=count($ssrep)-1;$j++) {
							if ($ssrep[$j]->bornegauche > $ssrep[$i]->bornegauche && $ssrep[$j]->bornedroite < $ssrep[$i]->bornedroite) {
								/* Supprime le r�pertoire du tableau */
								array_splice($ssrep, $j, 1);
								$trouve = true;
								break(2); /* Sort des 2 boucles for */
							}
						}
					}
				}
			}
		}
		return $ssrep;
	}
	
	/* Renvoie la liste des r�pertoires parents */
	function repertoiresparents($prendencomptelerepertoireracine=false) {
		/*
		Aide :
		- Tous les �l�ments d�pendant d'un �l�ment de r�f�rence (sous arbre) :
			select * from   NEW_FAMILLE where  NFM_BG > 22   and NFM_BD < 35 order by NFM_BG asc
		- Tous les p�res d'un �l�ment de r�f�rence :
			select * from   NEW_FAMILLE where  NFM_BG < 29   and NFM_BD > 34 order by NFM_BG asc
		*/
		
		$repparents = array();
		/* R�cup�re tous les r�pertoires enfants (sous-r�pertoires et sous-sous-r�pertoires...) */
		$requete = "select * from `".MULTIMEDIASPACE_REPERTOIRE."` where `bornegauche` < '".$this->bornegauche."' and `bornedroite` > '".$this->bornedroite."' order by bornegauche asc";
		$idrequete = 0;
		$erreurs = array();
		$res = multimediaspace_sql($requete, $erreurs, $idrequete);
		for($i=0;$i<=count($res)-1;$i++) {
			if ($prendencomptelerepertoireracine) {
				$repertoire = new Multimediaspace_Repertoire($res[$i]['id'],$res[$i]['nom'],$res[$i]['bornegauche'],$res[$i]['bornedroite'],$res[$i]['heritedesdroits'],$res[$i]['commentairesactifs']);
				$repparents[] = $repertoire;
			} else {
				if ($res[$i]['bornegauche'] != 1) { //Ne prend pas en compte le noeud parent
					$repertoire = new Multimediaspace_Repertoire($res[$i]['id'],$res[$i]['nom'],$res[$i]['bornegauche'],$res[$i]['bornedroite'],$res[$i]['heritedesdroits'],$res[$i]['commentairesactifs']);
					$repparents[] = $repertoire;
				}
			}
			
		}
		return $repparents;
	}
	
	/* Renvoie la liste des vid�os du r�pertoire */
	function videos($nomchamppourlordre='date', $ordrealphabetique='croissant')
	{
		switch($nomchamppourlordre) {
			case 'date':
			case 'id':
			case 'nom':
			case 'type':
			case 'largeur':
			case 'hauteur':
			case 'auteur':
				break;
			default:
				$nomchamppourlordre = 'date';
				break;
		}
		$videos = array();
		$requete = "select `id` from `".MULTIMEDIASPACE_VIDEO."` where idrepertoire='".$this->id."' order by ".$nomchamppourlordre." ";
		if ($ordrealphabetique == 'croissant') {
			$requete .= 'asc';
		} else {
			$requete .= 'desc';
		}
		$idrequete = 0;
		$erreurs = array();
		$res = multimediaspace_sql($requete, $erreurs, $idrequete);
		for($i=0;$i<=count($res)-1;$i++) {
			$video = new Multimediaspace_Video($res[$i]['id']);
			$video->charge();
			$videos[] = $video;
		}
		return $videos;
	}
	
	/* Renvoie le nombre de vid�os du r�pertoire */
	function nbvideos($tientcomptedessousrepertoires=false) {
		if (!$tientcomptedessousrepertoires) {
			$nb = 0;
			$requete = "select `id` from `".MULTIMEDIASPACE_VIDEO."` where idrepertoire='".$this->id."'";
			$idrequete = 0;
			$erreurs = array();
			$res = multimediaspace_sql($requete, $erreurs, $idrequete);
			return count($res);
		} else {
			/* On tient compte des sous-r�pertoires */
			$nb = 0;
			$requete = "select `id` from `".MULTIMEDIASPACE_VIDEO."` where idrepertoire='".$this->id."'";
			$idrequete = 0;
			$erreurs = array();
			$res = multimediaspace_sql($requete, $erreurs, $idrequete);
			$nb += count($res);
			
			$ssrep = $this->sousrepertoires(true);
			for($i=0;$i<=count($ssrep)-1;$i++) {
				$nb += $ssrep[$i]->nbvideos(false);
			}
			return $nb;
		}
	}
	
	/* V�rifie si l'utilisateur courant a les droits de lecture sur le r�pertoire */
	function adroitslecture($prendreencomptelessousniveaux=false) {
		if (!$prendreencomptelessousniveaux) {
			/* On v�rifie uniquement les droits dans ce r�pertoire */
			if ($this->heritedesdroits == 2) {
				/* Le r�pertoire a des droits uniques */
				if (multimediaspace_verifieacl($this->id, MULTIMEDIASPACE_DROITSLECTUREREPERTOIRE)) {
					return true;
				}
			} else {
				/* Le r�pertoire h�rite des droits d'acc�s parents */
				$repertoiresparents = $this->repertoiresparents(false);
				$repertoiresparents = array_reverse($repertoiresparents);
				/* Recherche le premier r�pertoire qui a des droits uniques */
				for($i=0;$i<=count($repertoiresparents)-1;$i++) {
					if ($repertoiresparents[$i]->heritedesdroits == 2) {
						if (multimediaspace_verifieacl($repertoiresparents[$i]->id, MULTIMEDIASPACE_DROITSLECTUREREPERTOIRE)) {
							return true;
						}
						break;
					}
				}
			}
		} else {
			/* On v�rifie les droits dans le r�pertoire et dans ses sous-r�pertoires */
			if ($this->heritedesdroits == 2) {
				/* Le r�pertoire a des droits uniques */
				if (multimediaspace_verifieacl($this->id, MULTIMEDIASPACE_DROITSLECTUREREPERTOIRE)) {
					return true;
				}
			} else {
				/* Le r�pertoire h�rite des droits d'acc�s parents */
				$repertoiresparents = $this->repertoiresparents(false);
				$repertoiresparents = array_reverse($repertoiresparents);
				/* Recherche le premier r�pertoire qui a des droits uniques */
				for($i=0;$i<=count($repertoiresparents)-1;$i++) {
					if ($repertoiresparents[$i]->heritedesdroits == 2) {
						if (multimediaspace_verifieacl($repertoiresparents[$i]->id, MULTIMEDIASPACE_DROITSLECTUREREPERTOIRE)) {
							return true;
						}
						break;
					}
				}
			}
			$ssrep = $this->sousrepertoires(true);
			for($i=0;$i<=count($ssrep)-1;$i++) {
				if ($ssrep[$i]->heritedesdroits == 2) {
					/* Le r�pertoire a des droits uniques */
					if (multimediaspace_verifieacl($ssrep[$i]->id, MULTIMEDIASPACE_DROITSLECTUREREPERTOIRE)) {
						return true;
					}
				} else {
					/* Le r�pertoire h�rite des droits d'acc�s parents */
					$repertoiresparents = $ssrep[$i]->repertoiresparents(false);
					$repertoiresparents = array_reverse($repertoiresparents);
					/* Recherche le premier r�pertoire qui a des droits uniques */
					for($j=0;$j<=count($repertoiresparents)-1;$j++) {
						if ($repertoiresparents[$j]->heritedesdroits == 2) {
							if (multimediaspace_verifieacl($repertoiresparents[$j]->id, MULTIMEDIASPACE_DROITSLECTUREREPERTOIRE)) {
								return true;
							}
							break;
						}
					}
				}
			}
		}
		return false;
	}
	
	/* Retourne le chemin o� sont situ�es les vid�os de ce r�pertoire */
	function cheminrepertoirevideos() {
		$chemin = multimediaspace_chemintemplatesmodule().'videos/';
		/* R�cup�re les identifiants des r�pertoires parents */
		$repertoiresparents = $this->repertoiresparents(false);
		for($i=0;$i<=count($repertoiresparents)-1;$i++) {
			if ($i == 0) {
				$chemin .= $repertoiresparents[$i]->id;
			} else {
				$chemin .= '/'.$repertoiresparents[$i]->id;
			}
		}
		/* R�pertoire courant */
		$chemin .= '/'.$this->id;
		return $chemin;
	}
	
	/* Retourne le chemin o� sont situ�es les vid�os de ce r�pertoire en rapport au lecteur flash */
	function cheminrepertoirevideospourlecteur() {
		$chemin = '../../videos/';
		/* R�cup�re les identifiants des r�pertoires parents */
		$repertoiresparents = $this->repertoiresparents(false);
		for($i=0;$i<=count($repertoiresparents)-1;$i++) {
			if ($i == 0) {
				$chemin .= $repertoiresparents[$i]->id;
			} else {
				$chemin .= '/'.$repertoiresparents[$i]->id;
			}
		}
		/* R�pertoire courant */
		$chemin .= '/'.$this->id;
		return $chemin;
	}
	
	/* Retourne le chemin o� sont situ�es les images de ce r�pertoire */
	function cheminrepertoireimages() {
		$chemin = multimediaspace_cheminuploadmodule().'images/';
		/* R�cup�re les identifiants des r�pertoires parents */
		$repertoiresparents = $this->repertoiresparents(false);
		for($i=0;$i<=count($repertoiresparents)-1;$i++) {
			if ($i == 0) {
				$chemin .= $repertoiresparents[$i]->id;
			} else {
				$chemin .= '/'.$repertoiresparents[$i]->id;
			}
		}
		/* R�pertoire courant */
		$chemin .= '/'.$this->id;
		return $chemin;
	}
	
	/* Supprime l'objet */
	function supprime() {
		if ($this->id != '') {
			/* Suppression des sous-r�pertoires et de leurs vid�os */
			$sousrepertoires = $this->sousrepertoires(true);
			for ($i=0;$i<=count($sousrepertoires)-1;$i++) {
				$sousrepertoires[$i]->supprime();
			}
			
			/* Suppression des vid�os de ce r�pertoire */
			$videos = $this->videos();
			for ($i=0;$i<=count($videos)-1;$i++) {
				$videos[$i]->supprime();
			}
			
			/* Suppression du ticket */
			$requete = "delete from `".MULTIMEDIASPACE_REPERTOIRE."` where id='".$this->id."'";
			$idrequete = 0;
			$erreurs = array();
			$res = multimediaspace_sql($requete, $erreurs, $idrequete);
			
			/* Suppression des r�pertoires dans le r�pertoire d'upload */
			multimediaspace_supprimerepertoire($this->cheminrepertoirevideos());
			multimediaspace_supprimerepertoire($this->cheminrepertoireimages());
		}
	}
	
}

