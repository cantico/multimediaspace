<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
************************************************************************
* Copyright (c) 2008 by CANTICO ( http://www.cantico.fr )              *
************************************************************************/
include_once 'base.php';



require_once dirname(__FILE__) . '/fonctionsgen.php';
require_once dirname(__FILE__) . '/adminpages.php';
require_once dirname(__FILE__) . '/userpages.php';


$idx = bab_rp('idx', 'droitsgest');



$erreurs = array(); //Gestion des erreurs : si vide pas d'erreurs


if (!bab_isUserAdministrator()) {

	$erreurs[] = multimediaspace_traduire("You don't have access to this page, you are not an administrator");

} else {

	switch($idx) {

		case 'configuration':
			multimediaspace_titre('Configuration');
			multimediaspace_configuration($erreurs);
			multimediaspace_onglet('configuration', 'Configuration', 'admin&idx=configuration');
			multimediaspace_onglet('videotemplates', 'Video templates', 'admin&idx=videotemplates');
			multimediaspace_onglet('droitsgest', 'Access rights', 'admin&idx=droitsgest');
			break;

		case 'configurationenr':
			//V�rification des donn�es
			$champs = array('lien', 'expediteur_nom', 'expediteur_mail', 'lecteur_video_par_defaut'); //champs � r�cup�rer
			$donnees = multimediaspace_recuperedonnees('post', $champs);
			$champsnv = array('lien','expediteur_nom', 'expediteur_mail', 'lecteur_video_par_defaut'); //champs obligatoires (non vides)
			if (multimediaspace_champsobligatoires($donnees, $champsnv)) {
				multimediaspace_configurationenr($donnees,$erreurs);
				$idx = 'configuration';
				multimediaspace_titre('Configuration');
				multimediaspace_configuration();
				multimediaspace_onglet('configuration', 'Configuration', 'admin&idx=configuration');
				multimediaspace_onglet('videotemplates', 'Video templates', 'admin&idx=videotemplates');
				multimediaspace_onglet('droitsgest', 'Access rights', 'admin&idx=droitsgest');
			} else {
				//Des champs n'ont pas �t� renseign�s
				$idx = 'configuration';
				multimediaspace_titre('Configuration');
				multimediaspace_configuration();
				multimediaspace_onglet('configuration', 'Configuration', 'admin&idx=configuration');
				multimediaspace_onglet('videotemplates', 'Video templates', 'admin&idx=videotemplates');
				multimediaspace_onglet('droitsgest', 'Access rights', 'admin&idx=droitsgest');
			}
			break;

		case 'videotemplates':
			multimediaspace_titre('Video templates');
			multimediaspace_videotemplates($erreurs);
			multimediaspace_onglet('configuration', 'Configuration', 'admin&idx=configuration');
			multimediaspace_onglet('videotemplates', 'Video templates', 'admin&idx=videotemplates');
			multimediaspace_onglet('droitsgest', 'Access rights', 'admin&idx=droitsgest');
			break;

		case 'videotemplateedit':
			$template = bab_rp('template', null);
			multimediaspace_titre('Edit video template');
			multimediaspace_videotemplateedit($template);
			multimediaspace_onglet('configuration', 'Configuration', 'admin&idx=configuration');
			multimediaspace_onglet('videotemplates', 'Video templates', 'admin&idx=videotemplates');
			multimediaspace_onglet('videotemplateedit', 'Edit video template', 'admin&idx=videotemplateedit');
			multimediaspace_onglet('droitsgest', 'Access rights', 'admin&idx=droitsgest');
			break;

		case 'videotemplatesave':
			$template = bab_rp('template', null);
			multimediaspace_videotemplatesave($template);
			header('Location: '. $GLOBALS['babUrlScript'].'?tg=' . bab_rp('tg') . '&idx=videotemplates');
			die;
			break;

		case 'videotemplatedelete':
			$template = bab_rp('template', null);
			multimediaspace_deleteVideoTemplate($template);
			header('Location: '. $GLOBALS['babUrlScript'].'?tg=' . bab_rp('tg') . '&idx=videotemplates');
			die;
			break;
			

		case 'droitsgestenr':
			multimediaspace_enregistreACL();
			$erreurs[] = multimediaspace_traduire('Save done');
			$idx = 'droitsgest';
			multimediaspace_titre("Definition of the administrators");
			multimediaspace_droitsgest($erreurs);
			multimediaspace_onglet('configuration', 'Configuration', 'admin&idx=configuration');
				multimediaspace_onglet('videotemplates', 'Video templates', 'admin&idx=videotemplates');
			multimediaspace_onglet('droitsgest', 'Access rights', 'admin&idx=droitsgest');
			break;

		case 'droitsgest':
		default:
			multimediaspace_titre("Definition of the administrators");
			multimediaspace_droitsgest($erreurs);
			multimediaspace_onglet('configuration', 'Configuration', 'admin&idx=configuration');
			multimediaspace_onglet('videotemplates', 'Video templates', 'admin&idx=videotemplates');
			multimediaspace_onglet('droitsgest', 'Access rights', 'admin&idx=droitsgest');
			break;

	}
	$babBody->setCurrentItemMenu($idx);
}

multimediaspace_erreurs($erreurs);

