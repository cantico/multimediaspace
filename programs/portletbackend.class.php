<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


bab_Functionality::includefile('PortletBackend');


require_once dirname(__FILE__) . '/fonctionsgen.php';
require_once dirname(__FILE__) . '/portletvideo.class.php';

/**
 * MultimediaSpace Portlet backend
 */
class Func_PortletBackend_MultimediaSpace extends Func_PortletBackend
{
	public function getDescription()
	{
		return multimediaspace_traduire('Multimedia Space Portlets');
	}


	public function select($category = null)
	{
		return array(
			'Video' => $this->portlet_Video(),
		);
	}


	public function portlet_Video()
	{
		return new multimediaspace_PortletDefinition_Video();
	}



	/**
	 * (non-PHPdoc)
	 * @see Func_PortletBackend::getConfigurationActions()
	 */
	public function getConfigurationActions()
	{
		return array();
	}
}



