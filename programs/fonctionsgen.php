<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2007 by CANTICO ( http://www.cantico.fr )              *
 ************************************************************************/
include_once 'base.php';

global $babDB;
$babDB->db_query("SET sql_mode = ''");

/* Liste des fonctions g�n�rales
 * Version 3.5
 * - Ajout de la fonction multimediaspace_cheminphpmodule()
 * - Ajout de la fonction multimediaspace_renommefichier()
 * Version 3.4
 * - Am�lioration de la fonction multimediaspace_taillefichier() afin de g�rer la taille en Mo
 * - Ajout de la fonction multimediaspace_nomfichier() pour conna�tre le nom d'un fichier � partir d'un chemin de fichier
 * - Ajout de la fonction multimediaspace_nomrepertoire()
 * Version 3.3
 * - Am�lioration de multimediaspace_telechargefichier() car probl�me avec le header parfois
 * - Ajout de la fonction multimediaspace_mimefichier() qui renvoie le type mime d'un fichier
 * Version 3.2
 * - Correction dans multimediaspace_calculeredimensionnementimage()
 * - Am�lioration de multimediaspace_telechargefichier()
 * Version 3.1
 * - Am�lioration de la fonction multimediaspace_sql : db_fetch_array remplac�e par db_fetch_assoc
 * - Ajout de la fonction ovovideos_navigateur() permettant de conna�tre le navigateur utilis� par l'utilisateur
 * Version 3.0
 * - Am�lioration de la fonction multimediaspace_fichierdepose pour �tre compatible PHP 4.1.0
 * - Am�lioration de la fonction multimediaspace_deplacefichiertmp pour �tre compatible PHP 4.1.0 et
 *   pour indiquer si le nom du fichier a �t� modifi�
 * - Ajout de la fonction multimediaspace_donneesfichierdepose
 * Version 2.9
 * - Ajout de la fonction tronquerchaine
 * Version 2.8
 * - Ajout de fonctions permettant la cr�ation d'un compte utilisateur et la mise � jour des coordonn�es
 * - Ajout de multimediaspace_idgroupeadministrateurs()
 * - Am�lioration de multimediaspace_ficheannuaire afin de ne pas pr�cisers l'id de l'utilisateur
 * - Ajoute la fonction multimediaspace_ajouteutilisateurdansgroupe()
 * - Am�lioration de multimediaspace_envoimail afin de v�rifier l'adresse des destinataires
 * - Am�lioration de multimediaspace_supprimefichier
 * - Ajout de multimediaspace_supprimeACL() pour supprimer des droits ACL sur un objet
 * Version 2.7
 * - Correction de la fonction listeannees : l'ann�e courante n'�tait pas correcte
 * Version 2.6
 * - Modification de la fonction de traduction pour forcer la langue
 * Version 2.5
 * - Ajout d'une fonction pour valider l'�criture d'une adresse mail
 * Version 2.4
 * - Ajout de fonctions permettant l'affichage d'une image et son redimensionnement :
 * multimediaspace_tailleimage()
 * multimediaspace_typeimage()
 * multimediaspace_calculeredimensionnementimage()
 * multimediaspace_afficheimage()
 * Version 2.3
 * - Ajout de la fonction multimediaspace_membresdroit($nomtable, $idobjet)
 * Version 2.2
 * - Ajout d'une description des champs retourn�s par ficheannuaire() 
 * Version 2.1
 * - ajout d'une fonction v�rifiant si le formulaire a d�j� �t� envoy� : emp�che les probl�mes d� aux F5 ou boutons pr�c�dent, suivant
 * Version 2.0
 * - modification de la multimediaspace_recuperedonnees() pour ne pas �tre oblig� de pr�ciser la m�thode (get ou post)
 * Version 1.9
 * - ajout de multimediaspace_membresgroupe()
 * - ajout de multimediaspace_groupesdroit()
 * - am�lioration de multimediaspace_envoiemail afin d'ajouter des pi�ces jointes aux mails
 * - am�lioration de multimediaspace_envoiemail afin d'envoyer des mails avec le template mailtemplate.html
 * Version 1.8
 * - ajout de fonctions pour g�rer les cookies
 * - am�lioration du d�bug de multimediaspace_sql() en affichant la requete
 * Version 1.7
 * - am�lioration de la fonction telechargefichier en ajoutant un test sur l'existence du fichier
 * - ajout de multimediaspace_taillefichier()
 * - ajout de multimediaspace_possiblelecturefichier()
 * - ajout de multimediaspace_existefichier()
 * - ajout de multimediaspace_taillemaxdepotfichiers()
 * - am�lioration de multimediaspace_fichierdepose()
 * - ajout de multimediaspace_multipages()
 * Version 1.6 :
 * - ajout multimediaspace_cheminuploadmodule()
 * Version 1.5 :
 * - ajout de la fonction multimediaspace_ficheannuaire() afin de r�cup�rer les donn�es d'un utilisateur
 * Version 1.4 :
 * - modification de multimediaspace_template() afin de g�rer plusieurs templates dans une m�me page
 * - modification de datedujour() afin de formater une date
 * - ajout de la fonction urltemplatesmodule() pour r�cup�rer l'url d'acc�s aux fichiers templates du module
 */

$texte = 'multimediaspace';

//Traduction pour gestion du multi-langues
function multimediaspace_traduire($texte, $langue='') {
	if ($langue != '') {
		return bab_translate($texte, 'multimediaspace', $langue);
	} else {
		return bab_translate($texte, 'multimediaspace');
	}
}

//Affiche le titre de la page
function multimediaspace_titre($texte) {
	global $babBody;
	$babBody->title = multimediaspace_traduire($texte);
}

//Affiche un onglet
function multimediaspace_onglet($identifiant,$texte,$url) {
	global $babBody;
	global $babAddonUrl;
	$babBody->addItemMenu($identifiant, multimediaspace_traduire($texte),
	        $babAddonUrl.$url);
}

//Active l'onglet courant
function multimediaspace_ongletcourant($identifiant) {
	global $babBody;
	$babBody->setCurrentItemMenu($identifiant);
}

//Affiche une erreur sur la page
function multimediaspace_erreurs($erreurs) {
	global $babBody;
	if (is_array($erreurs)) { //$erreurs est un tableau
		if (count($erreurs) != 0) {
			for ($i=0; $i<=count($erreurs)-1; $i++) {
				if ($i == 0) {
					$babBody->msgerror = multimediaspace_traduire($erreurs[$i]);
				} else {
					$babBody->msgerror = $babBody->msgerror."<br />".multimediaspace_traduire($erreurs[$i]);
				}
			}
		}
	} else { //$erreurs est une cha�ne
		if ($erreurs != "") {
			$babBody->msgerror = multimediaspace_traduire($erreurs);
		}
	}
}

//R�cup�re les donn�es pass�es en GET ou POST
function multimediaspace_recuperedonnees($mode='', $champs) {
	$donnees = array();
	for ($i=0; $i<=count($champs)-1; $i++) {
		switch($mode) {
			case 'get':
				$donnees[$champs[$i]] = bab_gp($champs[$i], '');
				break;
			case 'post':
				$donnees[$champs[$i]] = bab_pp($champs[$i], '');
				break;
			default:
				$donnees[$champs[$i]] = bab_rp($champs[$i], '');
				break;
		}
	}
	return $donnees;
}

//V�rifie les donn�es obligatoires pass�es par GET et POST et cr�� le message d'erreur si besoin
//champnvvaleurs permet de donner des valeurs autres que les id de champs pour le message d'erreurs
function multimediaspace_champsobligatoires($donnees, $champsnv, $champsnvvaleurs = array()) {
	$res = true; //vrai : pas d'erreurs
	$msgerreur = array();
	for ($i=0; $i<=count($champsnv)-1; $i++) {
		if (isset($donnees[$champsnv[$i]])) {
			if ($donnees[$champsnv[$i]] == "") {
				$res = false;
				if (count($champsnvvaleurs) == 0) {
					$msgerreur[] = $champsnv[$i]; //R�cup�ration des champs obligatoires qui sont vides
				} else {
					$msgerreur[] = $champsnvvaleurs[$i];
				}
			}
		} else {
			$res = false;
			if (count($champsnvvaleurs) == 0) {
				$msgerreur[] = $champsnv[$i]; //R�cup�ration des champs obligatoires qui n'existent pas
			} else {
				$msgerreur[] = $champsnvvaleurs[$i];
			}
		}
	}
	//Construction du message d'erreur
	if (!$res) {
		if (count($msgerreur) == 1) {
			multimediaspace_erreurs(array(multimediaspace_traduire("Error :the field ").$msgerreur[0]." ".multimediaspace_traduire("must be informed")));
		} else {
			$msg = $msgerreur[0];
			for ($i=1; $i<=count($msgerreur)-1; $i++) {
				$msg = $msg.", ".$msgerreur[$i];
			}
			multimediaspace_erreurs(multimediaspace_traduire("Error :the fields ").$msg." ".multimediaspace_traduire("must be informed"));
		}
	}
	return $res;
}

//Echappe une cha�ne ou les donn�es d'un tableau pour une requ�te SQL et renvoie le r�sultat en r�f�rence
function multimediaspace_echappesql(&$donnees) {
	global $babDB;
	if (is_array($donnees)) { //si c'est un tableau
		foreach ($donnees as $cle => $valeur) {
			$donnees[$cle] = $babDB->db_escape_string($donnees[$cle]);
		}
	} else { //si c'est une cha�ne
		$donnees = $babDB->db_escape_string($donnees);
	}
}

//Echappe une cha�ne ou les donn�es d'un tableau pour une requ�te SQL et renvoie le r�sultat en return
function multimediaspace_echappesqlsr($donnees) {
	global $babDB;
	if (is_array($donnees)) { //si c'est un tableau
		foreach ($donnees as $cle => $valeur) {
			$donnees[$cle] = $babDB->db_escape_string($donnees[$cle]);
		}
	} else { //si c'est une cha�ne
		$donnees = $babDB->db_escape_string($donnees);
	}
	return $donnees;
}

//Execute une requ�te SQL
function multimediaspace_sql($requete, &$erreurs, &$idrequete) {
	global $babDB;
	$babDB->errorManager(false);
	$res = $babDB->db_query($requete);
	$idrequete = $res; //identifiant de la requ�te
	//Gestion des erreurs
	if (!$res) {
		switch(substr(strtolower($requete), 0, 6)) {
			case "insert":
				$erreurs[] = multimediaspace_traduire("Error during the recording in the database");
				break;
			case "update":
				$erreurs[] = multimediaspace_traduire("Error during the recording in the database");
				break;
			case "select":
			default:
				$erreurs[] = multimediaspace_traduire("Error during the access to the database");
				break;
		}
		$erreurs[] = $babDB->db_print_error('');
		bab_debug($babDB->db_print_error('').'with the request:<br />'.$requete);
	}
	
	//Traitement des r�sultats
	$resultats = array();
	$compteur = 0;
	
	if ((substr(strtolower($requete), 0, 6) == "select") || (substr(strtolower($requete), 0, 4) == "show")) {
		if ($babDB->db_num_rows($res) != 0) {
			$tab = array();
			while($tab = $babDB->db_fetch_assoc($res)) {
				//traitement de la ligne pour avoir un enregistrement par index
				$tabcles = array_keys($tab);//retourne les cl�s du tableau
				for ($i=0;$i<=count($tabcles)-1;$i++) {
					$resultats[$compteur][$tabcles[$i]] = $tab[$tabcles[$i]];
				}
				$compteur++;
			}
		}
	}
	return $resultats; //retourne un tableau des r�sultats
}

//Retourne l'id du dernier enregistrement cr�� par SQL
function multimediaspace_sqlid($idrequete) {
	global $babDB;
	return $babDB->db_insert_id($idrequete);
}

//Lance un template d'affichage
function multimediaspace_template(&$objets, $fichiers, $templates, $pleinecran = false)
{
	global $babBody;
	global $babAddonHtmlPath;
	
	$babBody->addStyleSheet('toolbar.css');

	if (is_array($objets)) {
		if (!$pleinecran) {
			$babBody->babecho(bab_printTemplate($objeta, $babAddonHtmlPath."fonctionsgen.html", "debut"));
			for ($i=0;$i<=count($objets)-1;$i++) {
				$babBody->babecho(bab_printTemplate($objets[$i], $babAddonHtmlPath.$fichiers[$i], $templates[$i]));
			}
			$babBody->babecho(bab_printTemplate($objeta, $babAddonHtmlPath."fonctionsgen.html", "fin"));
		} else {
			$templatesfusionnes = bab_printTemplate($objeta, $babAddonHtmlPath."fonctionsgen.html", "debut");
			for ($i=0;$i<=count($objets)-1;$i++) {
				$templatesfusionnes = $templatesfusionnes.bab_printTemplate($objets[$i], $babAddonHtmlPath.$fichiers[$i], $templates[$i]);
			}
			$templatesfusionnes = $templatesfusionnes.bab_printTemplate($objeta, $babAddonHtmlPath."fonctionsgen.html", "fin");
			$babBody->babPopup($templatesfusionnes);
		}
	} else {
		if (!$pleinecran) {
			$babBody->babecho(bab_printTemplate($objets, $babAddonHtmlPath."fonctionsgen.html", "debut"));
			$babBody->babecho(bab_printTemplate($objets, $babAddonHtmlPath.$fichiers, $templates));
			$babBody->babecho(bab_printTemplate($objets, $babAddonHtmlPath."fonctionsgen.html", "fin"));
		} else {
			$babBody->babPopup(bab_printTemplate($objets, $babAddonHtmlPath."fonctionsgen.html", "debut").bab_printTemplate($objets, $babAddonHtmlPath.$fichiers, $templates).bab_printTemplate($objeta, $babAddonHtmlPath."fonctionsgen.html", "fin"));
		}
	}
}

//Cr�� le code html des champs d'un formulaire
function multimediaspace_champform($nom, $type, $valeur, $champsnoms = "", $champsvaleurs = "") {
	$res = "";
	$valeur = htmlentities($valeur);
	switch($type) {
		case "texte":
			$res = "<input name=\"$nom\" type=\"text\" value=\"$valeur\">";
			break;
		case "textemulti":
			$res = "<textarea name=\"$nom\" rows=\"4\" cols=\"30\">$valeur</textarea>";
			break;
		case "cache":
			$res = "<input name=\"$nom\" type=\"hidden\" value=\"$valeur\">";
			break;
		case "passe":
			$res = "<input name=\"$nom\" type=\"password\" value=\"$valeur\">";
			break;
		case "listeouinon":
			$res = "<select name=\"$nom\">";
			$oui = multimediaspace_traduire("yes");
			$non = multimediaspace_traduire("no");
			if ($valeur == 0) {
				$res = $res."<option value=\"0\" selected=\"selected\">$non</option>";
				$res = $res."<option value=\"1\">$oui</option>";
			} else {
				$res = $res."<option value=\"0\">$non</option>";
				$res = $res."<option value=\"1\" selected=\"selected\">$oui</option>";
			}
			$res = $res."</select>";
			break;
		case "liste":
			$res = "<select name=\"$nom\">";
			$res = $res."<option value=\"\">&nbsp;</option>"; //champ vide
			for ($i=0;$i<=count($champsnoms)-1;$i++) {
				if ($valeur == $champsvaleurs[$i]) {
					$res = $res."<option value=\"".$champsvaleurs[$i]."\" selected=\"selected\">$champsnoms[$i]</option>";
				} else {
					$res = $res."<option value=\"".$champsvaleurs[$i]."\">$champsnoms[$i]</option>";
				}
			}
			$res = $res."</select>";
			break;
	}
	return $res;
}

//Fonction permettant d'envoyer un mail
function multimediaspace_envoimail($sujet, $texte, $expediteur, $expediteurmail, $destinataire, $destinatairemail, $fichiers='', $templateovidentia = false) {
	global $babInstallPath;
	include_once $babInstallPath."utilit/mailincl.php";

	$mail = bab_mail();
	if(!$mail) { //Envoi de mails d�sactiv� dans la configuration du site Ovidentia
		return true;
	} else {
		$mail->mailFrom($expediteurmail, $expediteur);
		$mail->mailTo($destinatairemail, $destinataire);
		$mail->mailSubject($sujet);
		if ($templateovidentia) {
			$mail->mailBody($mail->mailTemplate($texte)); //texte html
		} else {
			$mail->mailBody($texte); //texte html
		}
		$mail->mailAltBody(strip_tags($texte)); //texte brut
		//Pi�ces jointes
		if ($fichiers != '' && is_array($fichiers)) {
			for ($i=0;$i<=count($fichiers)-1;$i++) {
				$nomfichier = $fichiers[$i]['nom'];
				$cheminfichier = $fichiers[$i]['chemin'];
				$typefichier = $fichiers[$i]['type'];
				$mail->mailFileAttach($cheminfichier, $nomfichier, $typefichier);
			}
		}
		//Emp�che l'envoi de mails si erreurs
		$res = false;
		if (multimediaspace_valideadressemail($destinatairemail)) {
			$res = $mail->send();
		}
		return($res);
	}
}

//Retourne la date du jour format�e pour MySQL
function multimediaspace_datedujour($format='a-m-j', $langue='fr') {
	switch($format) {
		case 'a-m-j':
			return date("Y-m-d");
			break;
		case 'j-m-a':
			return date("d-m-Y");
			break;
		case 'j mt a':
			$mois = date('m');
			if ($langue == 'fr') {
				switch(date('m')) {
					case '01':
						$mois = 'janvier';
						break;
					case '02':
						$mois = 'f�vrier';
						break;
					case '03':
						$mois = 'mars';
						break;
					case '04':
						$mois = 'avril';
						break;
					case '05':
						$mois = 'mai';
						break;
					case '06':
						$mois = 'juin';
						break;
					case '07':
						$mois = 'juillet';
						break;
					case '08':
						$mois = 'ao�t';
						break;
					case '09':
						$mois = 'septembre';
						break;
					case '10':
						$mois = 'octobre';
						break;
					case '11':
						$mois = 'novembre';
						break;
					case '12':
						$mois = 'd�cembre';
						break;
				}
			} else {
				$mois = date('F');
			}
			return date("d ").$mois.date(" Y");
			break;
		default:
			return false;
			break;
	}
}

//Retourne la date du jour format�e pour MySQL avec l'heure
function multimediaspace_datedujourdatetime() {
	return date("Y-m-d H:i:s");
}

//Permet de formater une date SQL en date fran�aise (jour/mois/ann�e)
function multimediaspace_formatedate($date) {
	if ((substr($date,8,2)."/".substr($date,5,2)."/".substr($date,0,4)) != "00/00/0000") {
		return substr($date,8,2)."/".substr($date,5,2)."/".substr($date,0,4);
	} else {
		return "";
	}
}

//Permet de formater une date SQL en date fran�aise avec heure (jour/mois/ann�e h:m)
function multimediaspace_formatedatetime($date) {
	if ((substr($date,8,2)."/".substr($date,5,2)."/".substr($date,0,4)." ".substr($date,11,5)) != "00/00/0000 00:00") {
		return substr($date,8,2)."/".substr($date,5,2)."/".substr($date,0,4)." ".substr($date,11,5);
	} else {
		return "";
	}
}

//Permet de formater une date SQL en date fran�aise (mois-ann�e)
function multimediaspace_formatedatema($date) {
	return substr($date,5,2)."-".substr($date,0,4);
}

//Calcule l'anciennet� d'une date
function multimediaspace_dateanciennete($date, $format='datetime') {
	$tab = array();
	if ($format == 'datetime') {
				
	}
	return array('annees' => '0', 'mois' => '0', 'jours' => '0', 'heures' => '0', 'secondes' => '0');
}

//Retourne les options pour un select d'une liste des jours possibles dans un calendrier
//Si le param�tre jour est indiqu�, on initialise la liste avec sinon on initialise avec le jour courant
//Le deuxi�me param�tre permet d'ajouter le jour 0
function multimediaspace_listejours($jour = "", $zero = false) {
	$tabjours = "";
	$jourcourant = date("d");//jour courant
	if ($jour != "") {
		$jourcourant = $jour;
	}
	$valeurinit = 1;
	if ($zero) {
		$valeurinit = 0;
	}
	for($i=$valeurinit;$i<=31;$i++) {
		$jour = $i;
		if ($i <= 9) {
			$jour = "0".$i;
		}
		if ($jourcourant == $jour) {
			$tabjours = $tabjours."<option value=\"$jour\" selected=\"selected\"> $jour</option>";
		} else {
			$tabjours = $tabjours."<option value=\"$jour\"> $jour</option>";
		}
	}
	return $tabjours;
}

//Retourne les options pour un select d'une liste des mois possibles dans un calendrier
//Si le param�tre mois est indiqu�, on initialise la liste avec sinon on initialise avec le mois courant
//Le deuxi�me param�tre permet d'ajouter le jour 0
function multimediaspace_listemois($mois = "", $zero = false) {
	$tabmois = "";
	$moiscourant = date("m");//mois courant
	if ($mois != "") {
		$moiscourant = $mois;
	}
	$valeurinit = 1;
	if ($zero) {
		$valeurinit = 0;
	}
	for($i=$valeurinit;$i<=12;$i++) {
		$mois = $i;
		if ($i <= 9) {
			$mois = "0".$i;
		}
		if ($moiscourant == $mois) {
			$tabmois = $tabmois."<option value=\"$mois\" selected=\"selected\"> $mois</option>";
		} else {
			$tabmois = $tabmois."<option value=\"$mois\"> $mois</option>";
		}
	}
	return $tabmois;
}

//Retourne les options pour un select d'une liste des ann�es possibles dans un calendrier
//Si le param�tre annee est indiqu� on initialise la liste avec, sinon on initialise avec l'ann�e courante
//Le deuxi�me param�tre permet d'ajouter le jour 0
function multimediaspace_listeannees($annee = '', $zero = false, $anneedebut = 2000, $anneefin = 2100) {
	$tabannees = "";
	$anneescourant = date("Y");//ann�e courante
	if ($annee != "") {
		$anneescourant = $annee;
	}
	if ($zero) {
		$tabannees = $tabannees."<option value=\"0000\" selected=\"selected\"> 0000</option>";
	}
	for($i=$anneedebut;$i<=$anneefin;$i++) {
		$annees = $i;
		if ($anneescourant == $annees) {
			$tabannees = $tabannees."<option value=\"$annees\" selected=\"selected\"> $annees</option>";
		} else {
			$tabannees = $tabannees."<option value=\"$annees\"> $annees</option>";
		}
	}
	return $tabannees;
}

//V�rifie si un r�pertoire existe
function multimediaspace_existerepertoire($chemin) {
	return is_dir($chemin);
}

//V�rifie si un fichier existe
function multimediaspace_existefichier($chemin) {
	return file_exists($chemin);
}

//Cr�� le r�pertoire s'il n'existe pas
function multimediaspace_creationrepertoire($chemin, $mode='') {
	if(!multimediaspace_existerepertoire($chemin)) {
		return @bab_mkdir($chemin, $mode);
	} else {
		return true; //le r�pertoire existe d�j�
	}
}

//Supprime un r�pertoire sur le serveur Web
function multimediaspace_supprimerepertoire($chemin) {
	if(is_dir($chemin)) {
		return rmdir($chemin);
	} else {
		return false;
	}
}

//Renvoie la liste des noms des sous-r�pertoires d'un r�pertoire
function multimediaspace_sousrepertoires($chemin) {
	if(multimediaspace_existerepertoire($chemin)) {
		$repertoires = array();		
		$aignorer = array ('.','..');
        $pointeur = opendir($chemin);
        while ($fichier = readdir($pointeur)) {
        	if (multimediaspace_existerepertoire($chemin.$fichier) && !in_array($fichier,$aignorer)) {
            	$repertoires[] = $fichier;
            }
        }
		asort($repertoires); //Tri alphab�tique
		if (count($repertoires) != 0) {
			return $repertoires;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

//V�rifie si un fichier a �t� d�pos� par POST depuis un champ input file
function multimediaspace_fichierdepose($nomchampinput) {
	/* V�rifie si on est en version sup�rieure � 4.1.0 pour utiliser $HTTP_POST_FILES ou $_FILES */
	if (version_compare(phpversion(), '4.1.0') > 0) {
		if (isset($_FILES[$nomchampinput]['name'])) {
			if ($_FILES[$nomchampinput]['name'] != '') {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		if (isset($HTTP_POST_FILES[$nomchampinput]['name'])) {
			if ($HTTP_POST_FILES[$nomchampinput]['name'] != '') {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}

//Retourne les donn�es sur un fichier d�pos� par POST (utiliser auparavant multimediaspace_fichierdepose())
function multimediaspace_donneesfichierdepose($nomchampinput) {
	$donnees = array();
	/* V�rifie si on est en version sup�rieure � 4.1.0 pour utiliser $HTTP_POST_FILES ou $_FILES */
	if (version_compare(phpversion(), '4.1.0') > 0) {
		$donnees['nom'] = $_FILES[$nomchampinput]['name'];
		$donnees['nomtemporaire'] = $_FILES[$nomchampinput]['tmp_name'];
		$donnees['type'] = $_FILES[$nomchampinput]['type'];
		$donnees['taille'] = $_FILES[$nomchampinput]['size'];
		if (isset($_FILES[$nomchampinput]['error'])){
			$donnees['erreur'] = $_FILES[$nomchampinput]['error'];
		} else {
			$donnees['erreur'] = 0; //Aucune erreur
		}		
	} else {
		$donnees['nom'] = $HTTP_POST_FILES[$nomchampinput]['name'];
		$donnees['nomtemporaire'] = $HTTP_POST_FILES[$nomchampinput]['tmp_name'];
		$donnees['type'] = $HTTP_POST_FILES[$nomchampinput]['type'];
		$donnees['taille'] = $HTTP_POST_FILES[$nomchampinput]['size'];
		if (isset($HTTP_POST_FILES[$nomchampinput]['error'])){
			$donnees['erreur'] = $HTTP_POST_FILES[$nomchampinput]['error'];
		} else {
			$donnees['erreur'] = 0; //Aucune erreur
		}
	}
	return $donnees;
}

/* D�place le fichier du dossier temporaire pass� par POST dans un r�pertoire
   $avecmodificationdunomdufichier a �t� ajout� afin de conna�tre le nom du fichier s'il a �t� modifi�
   L'ancien fonctionnement doit rester pour rester compatible avec les anciens modules qui auraient pu utiliser la fonction */
function multimediaspace_deplacefichiertmp($nomchamp, $chemin, $avecmodificationdunomdufichier=false) {
	/* V�rifie si on est en version sup�rieure � 4.1.0 pour utiliser $HTTP_POST_FILES ou $_FILES */
	if (version_compare(phpversion(), '4.1.0') > 0) {
		$filename1=stripslashes($_FILES[$nomchamp]['name']);
	} else {
		$filename1=stripslashes($HTTP_POST_FILES[$nomchamp]['name']);
	}
	//V�rifie si un fichier de ce m�me nom existe d�j� dans le r�pertoire
	if ($avecmodificationdunomdufichier) {
		while(file_exists($chemin.$filename1)) {
			//Recherche de l'extension du fichier
			$pos = strrpos($filename1, '.');
			if ($pos === false || $pos == -1) { //pas de point dans le nom du fichier
				$filename1 = $filename1.'-2';
			} else { //point trouv� donc extension pr�sente
				$filename1 = substr($filename1, 0, $pos).'-2.'.substr($filename1, $pos+1, strlen($filename1)-$pos);
			}
		}
	} else {
		/* Si un fichier de m�me nom existe d�j� dans le r�pertoire de destination on ne d�pose pas le nouveau fichier */
		if (file_exists($chemin.$filename1)) {
			return false;
		}
	}
	
	//D�p�t du fichier sur le serveur dans le r�pertoire d'upload
	/* V�rifie si on est en version sup�rieure � 4.1.0 pour utiliser $HTTP_POST_FILES ou $_FILES */
	if (version_compare(phpversion(), '4.1.0') > 0) {
		if(!@move_uploaded_file($_FILES[$nomchamp]['tmp_name'], $chemin.$filename1)) {
			if ($avecmodificationdunomdufichier) {
				$donnees = array();
				$donnees['erreur'] = true;
				$donnees['nomfichier'] = $filename1;
				return $donnees;
			} else {
				return false;
			}
		}
	} else {
		if(!move_uploaded_file($HTTP_POST_FILES[$nomchamp]['tmp_name'], $chemin.$filename1)) {
			return false;
		}
	}
	if ($avecmodificationdunomdufichier) {
		$donnees = array();
		$donnees['erreur'] = false;
		$donnees['nomfichier'] = $filename1;
		return $donnees;
	} else {
		return true;
	}
}

/* Renomme un fichier
   Si $avecmodificationdunomdufichier est � true, on renomme le fichier dans le cas o� un fichier de m�me nom existe */
function multimediaspace_renommefichier($chemin='', $nomancienfichier='', $nomnouveaufichier='', $avecmodificationdunomdufichier=false) {
	if ($nomancienfichier != '' && $nomnouveaufichier != '') {
		//V�rifie si un fichier de ce m�me nom existe d�j� dans le r�pertoire
		if ($avecmodificationdunomdufichier) {
			while(file_exists($chemin.'/'.$nomnouveaufichier)) {
				//Recherche de l'extension du fichier
				$pos = strrpos($nomnouveaufichier, '.');
				if ($pos === false || $pos == -1) { //pas de point dans le nom du fichier
					$nomnouveaufichier = $nomnouveaufichier.'-2';
				} else { //point trouv� donc extension pr�sente
					$nomnouveaufichier = substr($nomnouveaufichier, 0, $pos).'-2.'.substr($nomnouveaufichier, $pos+1, strlen($nomnouveaufichier)-$pos);
				}
			}
		} else {
			/* Si un fichier de m�me nom existe d�j� dans le r�pertoire de destination on ne renomme pas le nouveau fichier */
			if (file_exists($chemin.'/'.$nomnouveaufichier)) {
				return false;
			}
		}
		$donnees = array();
		$donnees['erreur'] = @rename($chemin.'/'.$nomancienfichier, $chemin.'/'.$nomnouveaufichier);
		$donnees['nomfichier'] = $nomnouveaufichier;
		return $donnees;
	} else  {
		/* Les noms de fichiers sont incorrects */
		$donnees = array();
		$donnees['erreur'] = true;
		$donnees['nomfichier'] = $nomnouveaufichier;
		return $donnees;
	}
}

/* Retourne le type MIME d'un fichier */
function multimediaspace_mimefichier($cheminfichier) {
	global $babDB;
	$mime = "application/octet-stream"; /* type mime par d�faut */
	if ($extension = strrchr($cheminfichier,".")) {
		$extension = strtolower(substr($extension,1));
		/* Types MIME */
		$typesmime = array();
		$typesmime[] = array('extension' => 'ai', 'mime' => 'application/postscript');
		$typesmime[] = array('extension' => 'asc', 'mime' => 'text/plain');
		$typesmime[] = array('extension' => 'au', 'mime' => 'audio/basic');
		$typesmime[] = array('extension' => 'avi', 'mime' => 'video/x-msvideo');
		$typesmime[] = array('extension' => 'bin', 'mime' => 'application/octet-stream');
		$typesmime[] = array('extension' => 'bmp', 'mime' => 'image/bmp');
		$typesmime[] = array('extension' => 'class', 'mime' => 'application/octet-stream');
		$typesmime[] = array('extension' => 'css', 'mime' => 'text/css');
		$typesmime[] = array('extension' => 'doc', 'mime' => 'application/msword');
		$typesmime[] = array('extension' => 'dvi', 'mime' => 'application/x-dvi');
		$typesmime[] = array('extension' => 'exe', 'mime' => 'application/octet-stream');
		$typesmime[] = array('extension' => 'flv', 'mime' => 'video/x-flv');
		$typesmime[] = array('extension' => 'gif', 'mime' => 'image/gif');
		$typesmime[] = array('extension' => 'htm', 'mime' => 'text/html');
		$typesmime[] = array('extension' => 'html', 'mime' => 'text/html');
		$typesmime[] = array('extension' => 'jpe', 'mime' => 'image/jpeg');
		$typesmime[] = array('extension' => 'jpeg', 'mime' => 'image/jpeg');
		$typesmime[] = array('extension' => 'jpg', 'mime' => 'image/jpeg');
		$typesmime[] = array('extension' => 'js', 'mime' => 'application/x-javascript');
		$typesmime[] = array('extension' => 'mid', 'mime' => 'audio/midi');
		$typesmime[] = array('extension' => 'midi', 'mime' => 'audio/midi');
		$typesmime[] = array('extension' => 'mp3', 'mime' => 'audio/mpeg');
		$typesmime[] = array('extension' => 'mpeg', 'mime' => 'video/mpeg');
		$typesmime[] = array('extension' => 'odt', 'mime' => 'application/vnd.oasis.opendocument.text');
		$typesmime[] = array('extension' => 'ods', 'mime' => 'application/vnd.oasis.opendocument.spreadsheet');
		$typesmime[] = array('extension' => 'odp', 'mime' => 'application/vnd.oasis.opendocument.presentation');
		$typesmime[] = array('extension' => 'odc', 'mime' => 'application/vnd.oasis.opendocument.chart');
		$typesmime[] = array('extension' => 'odf', 'mime' => 'application/vnd.oasis.opendocument.formula');
		$typesmime[] = array('extension' => 'odb', 'mime' => 'application/vnd.oasis.opendocument.database');
		$typesmime[] = array('extension' => 'odi', 'mime' => 'application/vnd.oasis.opendocument.image');
		$typesmime[] = array('extension' => 'odm', 'mime' => 'application/vnd.oasis.opendocument.text-master');
		$typesmime[] = array('extension' => 'ott', 'mime' => 'application/vnd.oasis.opendocument.text-template');
		$typesmime[] = array('extension' => 'ots', 'mime' => 'application/vnd.oasis.opendocument.spreadsheet-template');
		$typesmime[] = array('extension' => 'otp', 'mime' => 'application/vnd.oasis.opendocument.presentation-template');
		$typesmime[] = array('extension' => 'otg', 'mime' => 'application/vnd.oasis.opendocument.graphics-template');
		$typesmime[] = array('extension' => 'pdf', 'mime' => 'application/pdf');
		$typesmime[] = array('extension' => 'png', 'mime' => 'image/png');
		$typesmime[] = array('extension' => 'ppt', 'mime' => 'application/vnd.ms-powerpoint');
		$typesmime[] = array('extension' => 'ps', 'mime' => 'application/postscript');
		$typesmime[] = array('extension' => 'rtf', 'mime' => 'text/rtf');
		$typesmime[] = array('extension' => 'sxw', 'mime' => 'application/vnd.sun.xml.writer');
		$typesmime[] = array('extension' => 'tar', 'mime' => 'application/x-tar');
		$typesmime[] = array('extension' => 'txt', 'mime' => 'text/plain');
		$typesmime[] = array('extension' => 'wav', 'mime' => 'audio/x-wav');
		$typesmime[] = array('extension' => 'xls', 'mime' => 'application/vnd.ms-excel');
		$typesmime[] = array('extension' => 'xml', 'mime' => 'text/xml');
		$typesmime[] = array('extension' => 'zip', 'mime' => 'application/zip');
		for ($i=0;$i<=count($typesmime)-1;$i++) {
			if ($typesmime[$i]['extension'] == $extension) {
				$mime = $typesmime[$i]['mime'];
			}
		}
	}
	return $mime;
}

/* Propose le t�l�chargement d'un fichier
   $parametreobselete �tait le nom du fichier (basename permet de l'avoir automatiquement) */
function multimediaspace_telechargefichier($cheminfichier, $parametreobselete='') {
	/* Test l'existence du fichier et les droits d'acc�s du fichier */
	if (is_file($cheminfichier) && multimediaspace_possiblelecturefichier($cheminfichier)) {
		$nomfichier = basename($cheminfichier);
		$mime = multimediaspace_mimefichier($cheminfichier);
		$fsize = filesize($cheminfichier);
		set_time_limit(3600); //Fixe le temps maximum d'ex�cution d'un script
		if(substr(multimediaspace_navigateur(), 0, 2) == 'IE') { //Si le navigateur est Internet Explorer
			header('Cache-Control: public');
		}
		header("Content-Disposition: attachment; filename=\"$nomfichier\""."\n");
		header("Content-Type: $mime"."\n");
		header("Content-Length: ". $fsize."\n");
		header("Content-transfert-encoding: binary"."\n");
		$fp = fopen($cheminfichier, 'rb');
		if ($fp) {
			while(!feof($fp)) {
				print fread($fp, 8192);
			}
			fclose($fp);
			exit;
		}
	} else {
		bab_debug('The file don\'t exist or isn\'t accessible : <br />'.$cheminfichier);
	}
}

//Retourne la taille en Ko d'un fichier
function multimediaspace_taillefichier($chemin, $format = "Ko") {
	$taille = 0;
	switch($format) {
		case "Mo":
			if (file_exists($chemin)) {
				$taille = round(filesize($chemin)/1024/1024, 2);
			}
			break;
		case "Ko":
			if (file_exists($chemin)) {
				$taille = round(filesize($chemin)/1024, 2);
			}
			break;
		case "o":
			if (file_exists($chemin)) {
				$taille = filesize($chemin);
			}
			break;
	}
	clearstatcache();
	return $taille;
}

//Indique si le fichier est accessible en lecture
function multimediaspace_possiblelecturefichier($chemin) {
	return is_readable($chemin);
}

//Supprime un fichier sur le serveur Web
function multimediaspace_supprimefichier($chemin='', $nomfichier='') {
	/* Si $nomfichier n'existe pas, tout le chemin est indiqu� dans $chemin */
	/* S'il le chemin ne termine pas par un slash, on le rajoute */
	if ($nomfichier != '' && $chemin != '' && $chemin != '/' && (substr($chemin, -1) != '/')) {
		$chemin .= '/';
	}
	if (file_exists($chemin.$nomfichier)) {
		return unlink($chemin.$nomfichier);
	} else {
		return false;
	}
}

//Renvoie la taille maximale en Ko de d�p�t de fichiers avec les variables upload_max_filesize et post_max_size
function multimediaspace_taillemaxdepotfichiers() {
	$uploadmax = trim(ini_get("upload_max_filesize")); //Supprime les espaces en d�but et fin de cha�ne
	$postmax = trim(ini_get("post_max_size")); //Supprime les espaces en d�but et fin de cha�ne
	
	$last = strtolower($uploadmax{strlen($uploadmax)-1});
    $uploadmax = substr($uploadmax, 0, strlen($uploadmax)-1);
    switch($last) {
    	case 'G':
        case 'g': // Le modifieur 'G' est disponible depuis PHP 5.1.0
            $uploadmax *= 1024;
        case 'M':
        case 'm':
            $uploadmax *= 1024;
        case 'K':
        case 'k':
            $uploadmax *= 1024;
    }
    
	$last = strtolower($postmax{strlen($postmax)-1});
    $postmax = substr($postmax, 0, strlen($postmax)-1);
    switch($last) {
    	case 'G':
        case 'g': // Le modifieur 'G' est disponible depuis PHP 5.1.0
            $postmax *= 1024;
        case 'M':
        case 'm':
            $postmax *= 1024;
        case 'K':
        case 'k':
            $postmax *= 1024;
    }	
	$taillemax = 0;
	if (!$postmax) {
		$taillemax=$uploadmax;
	} else {
		if ($uploadmax <= $postmax) {
			$taillemax=$uploadmax;
		} else {
			$taillemax=$postmax;
		}
	}
	return round($taillemax/1024, 2);
}

/* Retourne le nom du fichier � partir d'un chemin de fichier */
function multimediaspace_nomfichier($cheminfichier) {
	return basename($cheminfichier);
}

/* Retourne le nom du r�pertoire � partir d'un chemin de r�pertoire */
function multimediaspace_nomrepertoire($cheminrepertoire) {
	return dirname($cheminrepertoire);
}

//Affiche le formulaire de droits d'acc�s (ACL)
//Filtres : tableau de 6 valeurs (0 ou 1 : 1 permettant d'activer la fonction)
//   $filtres[0] : permet de d�sactiver la liste des groupes
//   $filtres[1] : fonctionnalit� d�pr�ci�e (sans effet)
//   $filtres[2] : permet de d�sactiver le groupe "Utilisateurs d'Ovidentia"
//   $filtres[3] : permet de d�sactiver le groupe "Utilisateurs enregistr�s"
//   $filtres[4] : permet de d�sactiver le groupe "Utilisateurs anonymes"
//   $filtres[5] : permet de d�sactiver un ou plusieurs groupes sp�cifi�s par leurs ID (le param�tre est un tableau) 
function multimediaspace_acl($tg, $idx, $idobjet, $nomtable, $traduction, $filtres) {
	global $babInstallPath;
	include_once $babInstallPath.'admin/acl.php';
	$macl = new macl($tg, $idx, $idobjet, 'groupes');
	$macl->addtable($nomtable,multimediaspace_traduire($traduction));
	$macl->filter($filtres[0],$filtres[1],$filtres[2],$filtres[3],$filtres[4],$filtres[5]);
	$macl->babecho();
}

//Affiche le formulaire de droits d'acc�s (ACL) avec droits multiples
//Filtres : tableau de 6 valeurs (0 ou 1 : 1 permettant d'activer la fonction)
//   $filtres[0] : permet de d�sactiver la liste des groupes
//   $filtres[1] : fonctionnalit� d�pr�ci�e (sans effet)
//   $filtres[2] : permet de d�sactiver le groupe "Utilisateurs d'Ovidentia"
//   $filtres[3] : permet de d�sactiver le groupe "Utilisateurs enregistr�s"
//   $filtres[4] : permet de d�sactiver le groupe "Utilisateurs anonymes"
//   $filtres[5] : permet de d�sactiver un ou plusieurs groupes sp�cifi�s par leurs ID (le param�tre est un tableau) 
function multimediaspace_aclmultiples($tg, $idx, $idobjet, $nomstables, $traductions, $filtres) {
	global $babInstallPath;
	include_once $babInstallPath.'admin/acl.php';
	$macl = new macl($tg, $idx, $idobjet, 'groupes');
	for ($i=0;$i<=count($nomstables)-1;$i++) {
		$macl->addtable($nomstables[$i],multimediaspace_traduire($traductions[$i]));
		$macl->filter($filtres[$i][0],$filtres[$i][1],$filtres[$i][2],$filtres[$i][3],$filtres[$i][4],$filtres[$i][5]);
	}
	$macl->babecho();
}

//V�rifie les droits d'acc�s (ACL) sur un objet : retourne vrai si l'objet a les droits
function multimediaspace_verifieacl($idobjet, $nomtable) {
	global $babInstallPath;
	include_once $babInstallPath.'admin/acl.php';
	
	return bab_isAccessValid($nomtable, $idobjet);
}

//Enregistre les droits d'acc�s (ACL)
function multimediaspace_enregistreACL() {
	global $babInstallPath;
	include_once $babInstallPath.'admin/acl.php';
	
	maclGroups();
}

//Supprime les droits d'acc�s (ACL) d'un objet
function multimediaspace_supprimeACL($idobjet, $nomtable) {
	global $babInstallPath;
	include_once $babInstallPath.'admin/acl.php';
	
	aclDelete($nomtable, $idobjet);
}

//Retourne l'identifiant de l'utilisateur courant
function multimediaspace_idusercourant() {
	global $BAB_SESS_USERID;
	return $BAB_SESS_USERID;
}

//V�rifie si l'utilisateur courant est authentifi�
function multimediaspace_estauthentifie() {
	global $BAB_SESS_LOGGED;
	return $BAB_SESS_LOGGED;
}

//Cr�� un compte utilisateur
function multimediaspace_creationcompteutilisateur($prenom, $nom, $prenom2, $email, $identifiant, $passe, $passe2, $confirme, $erreur='', $placedsgroupepardefaut=true) {
	//Le dernier param�tre bool�en tient compte du groupe par d�faut choisi par l'administrateur dans les options d'inscription
	$idusercree = bab_registerUser($prenom, $nom, $prenom2, $email, $identifiant, $passe, $passe2, $confirme, $erreur, $placedsgroupepardefaut);
	if ($erreur == '') {
		return $idusercree;
	} else {
		return '';
	}
}

//Modifie les coordonn�es d'un compte utilisateur : utilise le m�me tableau renvoy� par la fonction ficheannuaire
function multimediaspace_modifiecompteutilisateur($idutilisateur='', $ficheannuaire) {
	if ($idutilisateur == '') {
		$idutilisateur = multimediaspace_idusercourant();
	}
	$erreur = '';
	bab_updateUserById($idutilisateur, $ficheannuaire, $erreur);
	return $erreur;
}

/* Renvoie les donn�es de la fiche annuaire
	Surnom : cn
	Nom : sn
	Deuxi�me pr�nom : mn
	Pr�nom : givenname
	Photo : jpegphoto
	Adresse de messagerie : email
	T�l�phone (bureau) : btel
	T�l. mobile : mobile
	T�l�phone (domicile) : htel
	T�l�copie (bureau) : bfax
	Titre : title
	Service : departmentnumber
	Soci�t� : organisationname
	Rue (bureau) : bstreetaddress
	Ville (bureau) : bcity
	Code postal (bureau) : bpostalcode
	D�p/R�gion (bureau) : bstate
	Pays (bureau) : bcountry
	Rue (domicile) : hstreetaddress
	Ville (domicile) : hcity
	Code postal (domicile) : hpostalcode
	D�p/R�gion (domicile) : hstate
	Pays (domicile) : hcountry
	Utilisateur 1 : user1
	Utilisateur 2 : user2
	Utilisateur 3 : user3
 */
function multimediaspace_ficheannuaire($idutilisateur='') {
	/* Si l'id n'est pas pr�cis�, on prend l'id de l'utilisateur courant */
	if ($idutilisateur == '') {
		$idutilisateur = multimediaspace_idusercourant();
	}
	return bab_getUserInfos($idutilisateur);
}

//Ajoute un utilisateur dans un ou plusieurs groupes
function multimediaspace_ajouteutilisateurdansgroupe($idutilisateur='', $idgroupes=array()) {
	/* Si l'id n'est pas pr�cis�, on prend l'id de l'utilisateur courant */
	if ($idutilisateur == '') {
		$idutilisateur = multimediaspace_idusercourant();
	}
	if (is_array($idgroupes)) {
		for ($i=0;$i<=count($idgroupes)-1;$i++) {
			if ($idgroupes[$i] != '') {
				bab_attachUserToGroup($idutilisateur, $idgroupes[$i]);
			}
		}
	} else {
		if ($idgroupes != '') {
			bab_attachUserToGroup($idutilisateur, $idgroupes);
		}
	}	
}

/* Retourne l'url d'acc�s aux fichiers templates du module (/ en fin)
   Fonction obsol�te : utiliser multimediaspace_chemintemplatesmodule() */
function multimediaspace_urltemplatesmodule() {
	global $babInstallPath, $babAddonHtmlPath;
	return $babInstallPath.'skins/ovidentia/templates/'.$babAddonHtmlPath;
}

//Retourne le chemin d'acc�s aux fichiers templates du module (/ en fin)
function multimediaspace_chemintemplatesmodule() {
	global $babInstallPath, $babAddonHtmlPath;
	return $babInstallPath.'skins/ovidentia/templates/'.$babAddonHtmlPath;
}

//Retourne le chemin d'acc�s aux fichiers php du module (/ en fin)
function multimediaspace_cheminphpmodule() {
	global $babAddonPhpPath;
	return $babAddonPhpPath;
}

//Retourne le chemin du r�pertoire de t�l�chargements du module (/ en fin)
function multimediaspace_cheminuploadmodule() {
	global $babAddonUpload;
	return $babAddonUpload;
}

//Calcul des �l�ments courants d'un multipages
function multimediaspace_multipages($nbtotalenregistrements, $pagecourante, $nbenregistrementsparpages) {
	$resultats = array();
	//Page courante
	$resultats['pagecourante'] = $pagecourante;
	//Page pr�c�dente
	if ($pagecourante == 1) {
		$resultats['pageprecedente'] = false;
	} else {
		$resultats['pageprecedente'] = $pagecourante - 1;
	}
	//Nombre total de pages
	$modulo = $nbtotalenregistrements % $nbenregistrementsparpages;
	if ($nbenregistrementsparpages > $nbtotalenregistrements) {
		$resultats['nbtotalpages'] = 1;
	} else {
		if ($modulo == 0) {
			$resultats['nbtotalpages'] = $nbtotalenregistrements / $nbenregistrementsparpages;
		} else {
			$division = floor($nbtotalenregistrements / $nbenregistrementsparpages); //arrondi � l'entier inf�rieur
			$resultats['nbtotalpages'] = $division + 1;
		}
	}
	//Page suivante
	if ($pagecourante < $resultats['nbtotalpages']) {
		$resultats['pagesuivante'] = $pagecourante + 1;
	} else {
		$resultats['pagesuivante'] = false;
	}
	//Nombre d'enregistrements dans la page
	if ($pagecourante < $resultats['nbtotalpages']) {
		$resultats['nbenrpagecourante'] = $nbenregistrementsparpages;
	} else {
		//On se trouve en derni�re page
		if ($pagecourante == 1) {
			 $resultats['nbenrpagecourante'] = $nbtotalenregistrements;
		} else {
			//on r�cup�re le reste des enregistrements en derni�re page
			$division = floor($nbtotalenregistrements / $nbenregistrementsparpages);
			$resultats['nbenrpagecourante'] = $nbtotalenregistrements - ($pagecourante * $division);
		}
	}
	//Nombre d'enregistrements restants
	$division = floor($nbtotalenregistrements / $pagecourante);
	$resultats['nbenrrestants'] = $nbtotalenregistrements - ($pagecourante * $division);
	//Index du premier enregistrement de la page
	if ($pagecourante == 1) {
		$resultats['indexpremierenrpagecourante'] = 1;
	} else {
		$division = floor($nbtotalenregistrements / $pagecourante);
		$resultats['indexpremierenrpagecourante'] = ($pagecourante * $division) + 1;
	}
	//Index du dernier enregistrement de la page
	if ($pagecourante < $resultats['nbtotalpages']) {
		$division = floor($nbtotalenregistrements / $pagecourante);
		$resultats['indexdernierenrpagecourante'] = ($pagecourante * ($division+1)) - 1;
	} else {
		//On est en derni�re page
		$resultats['indexdernierenrpagecourante'] = $nbtotalenregistrements;
	}
	return $resultats;
}

/* Cr�� un cookie */
function multimediaspace_creecookie($nom, $valeur, $expiration=0) {
	/* $expiration doit �tre un timestamp
	 * si vaut 0 : le cookie expire � la fin de la session ou lorsque le navigateur est ferm�
	 */
	return setcookie($nom, $valeur, $expiration);
}

/* Renvoie la valeur d'un cookie */
function multimediaspace_valeurcookie($nom) {
	if (isset($_COOKIE[$nom])) {
		return $_COOKIE[$nom];
	} else {
		return '';
	}
}

/* Supprime un cookie */
function multimediaspace_supprimecookie($nom) {
	return setcookie($nom, '', time() - 3600);
}

/* Retourne l'identifiant du groupe des administrateurs */
function multimediaspace_idgroupeadministrateurs() {
	global $BAB_ADMINISTRATOR_GROUP;
	$numgroupeadmin = $BAB_ADMINISTRATOR_GROUP;
	if ($numgroupeadmin == '' || $numgroupeadmin == null) {
		$numgroupeadmin = 3; //valeur des versions d'Ovidentia 6.4.0
	}
	return $numgroupeadmin;
}

/* Renvoie la liste des membres d'un groupe : un membre est un tableau index� avec id, name et email */
function multimediaspace_membresgroupe($idgroupe) {
	return bab_getGroupsMembers($idgroupe);
}

/* Renvoie la liste des groupes sur un droit */
function multimediaspace_groupesdroit($nomtable, $idobjet) {
	return bab_getGroupsAccess($nomtable, $idobjet);
}

/* Renvoie la liste des utilisateurs sur un droit
 * Renvoie un tableau index� par l'id utilisateur :
 * array
   (
    [154] =>
        (
            [name] => Guillaume Andr�
            [email] => test@test.com
		)
	)
 */
function multimediaspace_membresdroit($nomtable, $idobjet) {
	global $babInstallPath;
	include_once $babInstallPath.'admin/acl.php';
	return AclGetAccessUsers($nomtable, $idobjet);
}

/* Enregistre en base de donn�es le num�ro al�atoire soumis par un formulaire
   Ceci permet d'emp�cher les doublons d'enregistrements apr�s avoir appuy� sur F5
   en v�rifiant si la valeur a d�j� �t� soumise
   La table doit contenir ces champs :
   CREATE TABLE `NOMTABLE` (
	  `id` int(11) unsigned NOT NULL auto_increment,
	  `valeurformulaire` varchar(255) NOT NULL,
	  `date` DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
	  PRIMARY KEY  (`id`)
	) TYPE=MyISAM;
	
	A mettre dans le fichier PHP qui affiche le formulaire de cr�ation :
	//Emp�che la saisie
	$this->antif5 = multimediaspace_antif5valeur();
	
	A mettre dans le formulaire :
	<input type="hidden" name="antif5" value="{ antif5 }">
	
	A mettre dans la fonction PHP qui re�oit les donn�es :
	//V�rifie le formulaire pour emp�cher les doublons avec F5
	$antif5 = multimediaspace_antif5(multimediaspace_ANTIF5, $donnees['antif5']);
	if ($antif5) {
		//Lancer l'enregistrement
	}
	
*/
function multimediaspace_antif5($nomtable, $valeurformulaire) {
	multimediaspace_echappesql($valeurformulaire);
	multimediaspace_echappesql($nomtable);
	/* V�rifie si la valeur existe dans la table */
	$requete = "select `id` from `".$nomtable."` where valeurformulaire='".$valeurformulaire."'";
	$idrequete = 0;
	$erreurs = array();
	$res = multimediaspace_sql($requete, $erreurs, $idrequete);
	$verifie = false;
	if (count($res) == 1) {
		$verifie = true;
	}
	
	if ($verifie) {
		/* La valeur existe */
		/* On en profite pour �purer les enregistrements vieux de 2 jours */
		$date = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d")-2, date("Y")));
		$requete = "delete from `".$nomtable."`
								  where `date` < '$date'
								";
		$idrequete = 0;
		multimediaspace_sql($requete, $erreurs, $idrequete);
		return false;
	} else {
		/* La valeur n'existe pas, on la sauvegarde */
		$date = multimediaspace_datedujourdatetime();
		$requete = "insert into `".$nomtable."` ( `valeurformulaire` , `date` )
						  values (  '$valeurformulaire' , '$date' )
						";
		$idrequete = 0;
		multimediaspace_sql($requete, $erreurs, $idrequete);
		if (count($erreurs) == 0) {
			$erreurs[] = "Save done";
		}
		/* On en profite pour �purer les enregistrements vieux de 2 jours */
		$date = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d")-2, date("Y")));
		$requete = "delete from `".$nomtable."`
								  where `date` < '$date'
								";
		$idrequete = 0;
		multimediaspace_sql($requete, $erreurs, $idrequete);
		return true;
	}
	return true;
}

/* Retourne une valeur al�atoire � placer dans le formulaire
   Ceci permet d'emp�cher les doublons d'enregistrements apr�s avoir appuy� sur F5
   en v�rifiant si la valeur a d�j� �t� soumise
   La table doit contenir ces champs :
   CREATE TABLE `NOMTABLE` (
	  `id` int(11) unsigned NOT NULL auto_increment,
	  `valeurformulaire` varchar(255) NOT NULL,
	  `date` DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
	  PRIMARY KEY  (`id`)
	) TYPE=MyISAM;
	
*/
function multimediaspace_antif5valeur() {
	$valeur = '';
	$chaine = 'abcdefghijklmnpqrstuvwxy';
	if (version_compare(phpversion(), '4.2.0') >= 0) {
		srand();
	} else {
		srand((double)microtime()*1000000);
	}
	for($i=0;$i<50;$i++) {
		$valeur .= $chaine[rand()%strlen($chaine)];
	}
	return $valeur;
}

/* Renvoie les tailles d'une image : largeur et hauteur */
function multimediaspace_tailleimage($cheminimage) {
	$tab = @getimagesize($cheminimage);
	$taille = array('largeur' => $tab[0], 'hauteur' => $tab[1]);
	return $taille;
}

/* Renvoie le type d'une image : jpeg, gif ou png */
function multimediaspace_typeimage($cheminimage) {
	$tab = @getimagesize($cheminimage);
	$type = '';
	switch($tab[2]) {
		case '2':
			$type = 'jpeg';
			break;
		case '1':
			$type = 'gif';
			break;
		case '3':
			$type = 'png';
			break;
	}
	return $type;
}

/* Calcule la taille d'une image (hauteur et largeur) en gardant les proportions
   Si hauteurvoulue et largeurvoulue sont indiqu�es, on calcule les tailles de telle sorte que l'image passe dans le rectangle form� par largeurvoulue et hauteurvoulue
   Si hauteurvoulue est pr�cis�e mais pas largeurvoulue, on calcule les tailles en proportion de l'image d'origine afin que la hauteur corresponde � hauteurvoulue
   Si largeurvoulue est pr�cis�e mais pas hauteurvoulue, on calcule les tailles en proportion de l'image d'origine afin que la largeur corresponde � largeurvoulue
   Remarque : le script peut agrandir ou r�tr�cir l'image selon les conditions */

function multimediaspace_calculeredimensionnementimage($largeuroriginale,$hauteuroriginale,$largeurvoulue='',$hauteurvoulue='') {
	/* Si les 2 valeurs voulues ne sont pas pr�cis�es, on renvoit les valeurs d'origine */
	if (($largeurvoulue == '' || $largeurvoulue == 0) && ($hauteurvoulue == '' || $hauteurvoulue == 0)) {
		$tab=array();
		$tab['largeur'] = $largeuroriginale;
		$tab['hauteur'] = $hauteuroriginale;
		return $tab;
	}
	$ratio=$largeuroriginale/$hauteuroriginale;
	if ($largeurvoulue != '' && $hauteurvoulue != '') { //Largeur et hauteur sont indiqu�es
		/* On redimensionne l'image afin qu'elle passe dans le rectangle form� par largeurvoulue et hauteurvoulue */
		/* R�tr�cit l'image */
		$largeuroriginaletmp = $largeuroriginale;
		$hauteuroriginaletmp = $hauteuroriginale;
		if ($largeuroriginaletmp > $largeurvoulue) {
			while($largeuroriginale > $largeurvoulue) {
				$largeuroriginale--;
				$hauteuroriginale=round($largeuroriginale/$ratio);
			}
		}
		if ($hauteuroriginaletmp > $hauteurvoulue) {
			while($hauteuroriginale > $hauteurvoulue) {
				$hauteuroriginale--;
				$largeuroriginale=round($hauteuroriginale*$ratio);
			}
		}
		/* Agrandit l'image */
		if ($largeuroriginaletmp < $largeurvoulue) {
			while($largeuroriginale < $largeurvoulue) {
				$largeuroriginale++;
				$hauteuroriginale=round($largeuroriginale/$ratio);
			}
		}
		if ($hauteuroriginaletmp < $hauteurvoulue) {
			while($hauteuroriginale < $hauteurvoulue) {
				$hauteuroriginale++;
				$largeuroriginale=round($hauteuroriginale*$ratio);
			}
		}
	} else {
		if ($largeurvoulue != '' && $hauteurvoulue == '') { //Seule la largeur est pr�cis�e
			/* On redimensionne l'image afin d'avoir la largeur voulue */
			/* On r�tr�cit l'image */
			if ($largeuroriginale > $largeurvoulue) {
				while($largeuroriginale > $largeurvoulue) {
					$largeuroriginale--;
					$hauteuroriginale=round($largeuroriginale/$ratio);
				}
			}
			/* On agrandit l'image */
			if ($largeuroriginale < $largeurvoulue) {
				while($largeuroriginale < $largeurvoulue) {
					$largeuroriginale++;
					$hauteuroriginale=round($largeuroriginale/$ratio);
				}
			}
		}
		if ($largeurvoulue == '' && $hauteurvoulue != '') { //Seule la hauteur est pr�cis�e
			/* On redimensionne l'image afin d'avoir lahauteur voulue */
			/* On r�tr�cit l'image */
			if ($hauteuroriginale > $hauteurvoulue) {
				while($hauteuroriginale > $hauteurvoulue) {
					$hauteuroriginale--;
					$largeuroriginale=round($hauteuroriginale*$ratio);
				}
			}
			/* On agrandit l'image */
			if ($hauteuroriginale < $hauteurvoulue) {
				while($hauteuroriginale < $hauteurvoulue) {
					$hauteuroriginale++;
					$largeuroriginale=round($hauteuroriginale*$ratio);
				}
			}
		}
	}
	$tab=array();
	$tab['largeur'] = $largeuroriginale;
	$tab['hauteur'] = $hauteuroriginale;
	return $tab;
}

/* Renvoie en header l'image dont le chemin est indiqu�
   L'image est redimensionn�e si les tailles sont pr�cis�es
   Si valeursfixes est � vrai, on redimensionne l'image avec les valeurs largeur et hauteur (les 2 valeurs sont obligatoires)
   Si valeursfixes est � faux, on redimensionne l'image afin qu'elle tienne dans le rectangle form� par la largeur et la hauteur (la hauteur peut �tre optionnelle ainsi que la largeur)
   typeimage : jpeg, gif ou png ; si non pr�cis� on recherche  */
function multimediaspace_afficheimage($cheminimage, $largeuroriginale, $hauteuroriginale, $largeurvoulue='', $hauteurvoulue='', $valeursfixes=false, $typeimage='') {
	if ($valeursfixes) {
		/* Tailles fixes */
		$largeurfinale = $largeurvoulue;
		$hauteurfinale = $hauteurvoulue;		
	} else {
		/* L'image doit �tre contenue dans le rectangle form� par la largeur et la hauteur */
		$tailles = multimediaspace_calculeredimensionnementimage($largeuroriginale,$hauteuroriginale,$largeurvoulue,$hauteurvoulue);
		$largeurfinale = $tailles['largeur'];
		$hauteurfinale = $tailles['hauteur'];
		
		/* R�cup�ration du type de l'image s'il n'est pas pr�cis� en param�tre */
		if ($typeimage == '') {
			$typeimage = multimediaspace_typeimage($cheminimage);
		}
		switch($typeimage) {
			case 'jpg':
			case 'jpeg':
				$tmp = imagecreatefromjpeg($cheminimage);
				break;
			case 'gif':
				$tmp = imagecreatefromgif($cheminimage);
				break;
			case 'png':
				$tmp = imagecreatefrompng($cheminimage);
				break;
		}
		if (isset($tmp)) {
			if (function_exists('imagecreatetruecolor') && $typeimage != 'gif') {
				$out = imagecreatetruecolor($largeurfinale, $hauteurfinale);
			} else {
				$out = imagecreate($largeurfinale, $hauteurfinale);
			}
			/* imgsize[0] : reel width of image
			   imgsize[1] : reel height of image
			   wimg : width desired
			   himg : height desired
			*/
			imagecopyresized($out, $tmp, 0, 0, 0, 0, $largeurfinale, $hauteurfinale, $largeuroriginale, $hauteuroriginale);
			imagedestroy($tmp);
				
			switch($typeimage)	{
				case 'jpg':
				case 'jpeg':
					header('Content-type: image/jpeg');
					imagejpeg($out);
					break;
				case 'gif':
					header('Content-type: image/gif');
					imagegif($out);
					break;
				case 'png':
					header('Content-type: image/png');
					imagepng($out);
					break;
			}
		}
	}
	
}

/* Valide l'�criture d'une adresse mail : n'effectue pas tous les tests */
function multimediaspace_valideadressemail($adresse) {
	/* s�pare la chaine en 2 autour de l'arobas */
	$tab = explode('@', $adresse);
	if (count($tab) != 2) {
		/* la chaine doit contenir un arobas */
		return false;
	}
	/* ne doit contenir qu'un seul arobas */
	if (strpos($tab[0], '@') !== false || strpos($tab[1], '@') !== false) {
		return false;
	}
	/* la premi�re chaine doit faire minimum 1 caract�re et la deuxi�me doit faire 3 caract�res */
	if (strlen($tab[0]) < 1 || strlen($tab[1]) < 3) {
		return false;
	}
	/* la deuxi�me chaine doit contenir un point */
	if (strpos($tab[1], '.') === false) {
		return false;
	}
	/* un seul point en seconde chaine */
	if (strpos($tab[1], '.') !== strrpos($tab[1], '.')) {
		return false;
	}
	/* le point ne doit pas se trouver en premi�re position */
	if (strpos($tab[1], '.') === 0) {
		return false;
	}
	/* le point ne doit pas se trouver en derni�re position */
	if (strpos($tab[1], '.') === strlen($tab[1])) {
		return false;
	}
	return true;
}

//tronque une cha�ne ou toutes les chaines du tableau
function multimediaspace_tronquechaine($chaine='', $nbcaracteresmax=0, $ajoute3petitspoints=false) {
	if (is_array($chaine)) {
		$chaine2 = array();
		for ($i=0;$i<=count($chaine)-1;$i++) {
			$tronque = substr($chaine[$i], 0, $nbcaracteresmax);
			if ($tronque == $chaine[$i]) {
				$chaine2[] = $tronque;
			} else {
				if ($ajoute3petitspoints) {
					$chaine2[] = $tronque.'...';
				} else {
					$chaine2[] = $tronque;
				}
			}
		}
		return $chaine2;
	} else {
		$tronque = substr($chaine, 0, $nbcaracteresmax);
		if ($tronque == $chaine) {
			return $tronque;
		} else {
			if ($ajoute3petitspoints) {
				return $tronque.'...';
			} else {
				return $tronque;
			}
		}
	}
}

/* Retourne le nom du navigateur de l'utilisateur courant */
function multimediaspace_navigateur() {
	$navuser = $_SERVER['HTTP_USER_AGENT'];
	$navigateur = '';
	if (eregi('msie', $navuser) && !eregi('opera', $navuser)) {
		//Internet Explorer
		$navigateur = 'IE';
		/* V�rifie la version d'Internet Explorer
		   Exemple sous IE7 : [HTTP_USER_AGENT] => Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648) */
		$tab = explode('MSIE', $_SERVER['HTTP_USER_AGENT']);
		if (isset($tab[1])) {
			$tab2 = explode(';', $tab[1]);
			if (isset($tab2[0])) {
				if (is_numeric(trim($tab2[0]))) {
					if (trim($tab2[0]) < 6) {
						$navigateur = 'IE5';
					}
					if (trim($tab2[0]) >= 6 && trim($tab2[0]) < 7) {
						$navigateur = 'IE6';
					}
					if (trim($tab2[0]) >= 7 && trim($tab2[0]) < 8) {
						$navigateur = 'IE7';
					}
					if (trim($tab2[0]) >= 8 && trim($tab2[0]) < 9) {
						$navigateur = 'IE8';
					}
				}
			}
		}
		
	} elseif (eregi('opera', $navuser)) {
		//Opera
		$navigateur = 'O';
	} elseif (eregi('Mozilla/4.', $navuser)) {
		//Netscape 4.x
		$navigateur = 'N4';
	} elseif (eregi('Mozilla/5.0', $navuser) && !eregi('Konqueror', $navuser) && !eregi('Firefox', $navuser) && !eregi('Safari', $navuser)) {
		//Netscape 6
		$navigateur = 'N6';
	} elseif(strchr($_SERVER[ 'HTTP_USER_AGENT' ], 'Safari')) {
		/* Safari (Mac OS) */
		$navigateur = "S";
	} elseif(strchr($_SERVER[ 'HTTP_USER_AGENT' ], 'Firefox')) {
		/* FireFox */
		$navigateur = "F";
	} elseif(strchr($_SERVER[ 'HTTP_USER_AGENT' ], 'Konqueror')) {
		/* Konqueror (Gnu/Linux KDE) */
		$navigateur="Konqueror";
	} elseif(strchr($_SERVER[ 'HTTP_USER_AGENT' ], 'Epiphany')) {
		/* Epiphany (Gnu/Linux Gnome) */
		$navigateur="Epiphany";
	} elseif(strchr($_SERVER[ 'HTTP_USER_AGENT' ], 'Lynx')) {
		/* Lynx (text browser) */
		$navigateur="Lynx"; 
	} else {
		//Autres navigateurs
		$navigateur = '';
	}
	return $navigateur;
}

