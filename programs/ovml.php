<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2008 by CANTICO ( http://www.cantico.fr )              *
 ************************************************************************/
include_once 'base.php';

//Chargement des fonctions
global $babAddonPhpPath;
global $babInstallPath;
require_once dirname(__FILE__).'/fonctionsgen.php';
require_once dirname(__FILE__).'/fonctions.php';

/*
	Gestion des containers OVML du module
	
	Exemples d'utilisation :
	
	Liste des r�pertoires de premier niveau :
	<OCAddon name="multimediaspace" action="folders">
		<a href="<OVFolderUrl>"><OVFolderName> - <OVFolderNumberOfVideosWithSubFolders> video(s)</a><br />
	</OCAddon>

	Informations sur le r�pertoire 5 :
	<OCAddon name="multimediaspace" action="folder" folderid="5">
		<a href="<OVFolderUrl>"><OVFolderName> - <OVFolderNumberOfVideos> video(s)</a>	
	</OCAddon>

	Liste des vid�os 5 derni�res vid�os du r�pertoire 2, tri�es par dates d�croissantes, affichage des images miniatures en 150x150 pixels et lancement automatique des vid�os :
	<OCAddon name="multimediaspace" action="videos" folderid="2" withsubfolders="yes" rows="5" offset="0" orderobject="date" ordertype="desc" imagewidth="150" imageheight="150" playautomatically="yes">
		<div>
			<img src="<OVVideoImageUrl>" alt="" title=""><br />
			<a href="<OVVideoUrl>"><OVVideoName></a><br />
			<OVVideoHtmlCode><br />
		</div>
	</OCAddon>
	
	Informations sur la vid�o 9, affichage de l'image miniature en 150x150 pixels et lancement automatique de la vid�o :
	<OCAddon name="multimediaspace" action="video" videoid="9" imagewidth="150" imageheight="150" playautomatically="yes">
		<img src="<OVVideoImageUrl>" alt="" title=""><br />
		<a href="<OVVideoUrl>"><OVVideoName></a><br />
		<OVVideoHtmlCode>
	</OCAddon>
	
	Informations sur la vid�o 9, affichage du lecteur en 400x350 pixels :
	<OCAddon name="multimediaspace" action="video" videoid="9" playerwidth="400" playerheight="350">
		<a href="<OVVideoUrl>"><OVVideoName></a><br />
		<OVVideoHtmlCode>
	</OCAddon>
	
*/
function multimediaspace_ovml($args) {
	$donnees = array();
	
	if (isset($args['action'])) {
		switch($args['action']){
			case 'Folders':
			case 'folders':
				$repertoires = array();
				if (isset($args['folderid'])) {
					if (is_numeric($args['folderid'])) {
						/* On veut les sous-r�pertoires du r�pertoire indiqu� */
						$repertoire = new Multimediaspace_Repertoire($args['folderid']);
						$repertoire->charge();
						$repertoires = $repertoire->sousrepertoires(false);
					}
				} else {
					/* On veut les r�pertoires de premier niveau */
					$espace = new Multimediaspace_EspaceMultimedia();
					$repertoires = $espace->repertoires(false);
				}
				for ($i=0;$i<=count($repertoires)-1;$i++) {
					if ($repertoires[$i]->adroitslecture(true)) {
						$tab = array();
						$tab['FolderId'] = $repertoires[$i]->id;
						$tab['FolderName'] = $repertoires[$i]->nom;
						$tab['FolderUrl'] = $repertoires[$i]->url;
						$tab['FolderNumberOfVideos'] = $repertoires[$i]->nbvideos(false);
						$tab['FolderNumberOfVideosWithSubFolders'] = $repertoires[$i]->nbvideos(true);
						$donnees[] = $tab;
					}
				}
				break;
			case 'Folder':
			case 'folder':
				if (isset($args['folderid'])) {
					if (is_numeric($args['folderid'])) {
						$repertoire = new Multimediaspace_Repertoire($args['folderid']);
						$repertoire->charge();
						if ($repertoire->adroitslecture(true)) {
							$tab = array();
							$tab['FolderId'] = $repertoire->id;
							$tab['FolderName'] = $repertoire->nom;
							$tab['FolderUrl'] = $repertoire->url;
							$tab['FolderNumberOfVideos'] = $repertoire->nbvideos(false);
							$tab['FolderNumberOfVideosWithSubFolders'] = $repertoire->nbvideos(true);
							$donnees[] = $tab;
						}
					}
				}
				break;
			case 'Videos':
			case 'videos':
				$folderid = '';
				/* V�rification si un id de r�pertoire est indiqu� */
				if (isset($args['folderid'])) {
					if (is_numeric($args['folderid'])) {
						$folderid = $args['folderid'];
					}
				}
				/* Prend en compte les sous-r�pertoires */
				$prendreencomptelessousrepertoires = false;
				if (isset($args['withsubfolders'])) {
					if ($args['withsubfolders'] == 'yes') {
						$prendreencomptelessousrepertoires = true;
					}
				}
				if (!isset($args['folderid']) && !isset($args['withsubfolders'])) {
					$prendreencomptelessousrepertoires = true;
				}
				
				/* V�rification du tri choisit */
				$nomchamppourlordre = 'date';
				if (isset($args['orderobject'])) {
					switch($args['orderobject']) {
						case 'date':
							break;
						case 'id':
							break;
						case 'name':
							$nomchamppourlordre = 'nom';
							break;
						case 'type':
							break;
						case 'width':
							$nomchamppourlordre = 'largeur';
							break;
						case 'height':
							$nomchamppourlordre = 'hauteur';
							break;
						case 'author':
							$nomchamppourlordre = 'auteur';
							break;
						default:
							$nomchamppourlordre = 'date';
							break;
					}
				}
				$ordrealphabetique = 'croissant';
				if (isset($args['ordertype'])) {
					if ($args['ordertype'] == 'desc') {
						$ordrealphabetique = 'decroissant';
					}
				}
				/* Nombre de vid�os max */
				$index = '';
				$nbvideos = '';
				if (isset($args['rows'])) {
					if (is_numeric($args['rows'])) {
						$nbvideos = $args['rows'];
					}
				}
				if (isset($args['offset'])) {
					if (is_numeric($args['offset'])) {
						$index = $args['offset'];
					}
				}
				
				$videos = multimediaspace_listevideos($folderid, $prendreencomptelessousrepertoires, $nomchamppourlordre, $ordrealphabetique, $index, $nbvideos);
				for ($i=0;$i<=count($videos)-1;$i++) {
					$tab = array();
					$tab['VideoId'] = $videos[$i]->id;
					$tab['VideoFolderId'] = $videos[$i]->idrepertoire;
					$tab['VideoName'] = $videos[$i]->nom;
					$tab['VideoType'] = 'flvfile';
					switch($videos[$i]->type) {
						case 'flv':
							$tab['VideoType'] = 'flvfile';
							break;
						case 'videointegree':
							$tab['VideoType'] = 'integratedvideo';
							break;
						case 'url':
							$tab['VideoType'] = 'urlextern';
							break;
					}
					if ($videos[$i]->type == 'url') {
						$tab['VideoUrl'] = $videos[$i]->valeur;
					} else {
						$tab['VideoUrl'] = $videos[$i]->url;
					}
					if (isset($args['playautomatically'])) {
						if (is_numeric($args['playautomatically'])) {
							if ($args['playautomatically'] == 'yes') {
								$video[$i]->jouerautomatiquement = 2;
							} else {
								$video[$i]->jouerautomatiquement = 1;
							}
						}
					}
					$tab['VideoWidth'] = $videos[$i]->largeur;
					$tab['VideoHeight'] = $videos[$i]->hauteur;
					$tab['PlayerWidth'] = $videos[$i]->largeurlecteur;
					$tab['PlayerHeight'] = $videos[$i]->hauteurlecteur;
					$tab['VideoImageUrl'] = $videos[$i]->urlminiature;
					if (isset($args['imagewidth']) && is_numeric($args['imagewidth'])) {
						if (isset($args['imageheight']) && is_numeric($args['imageheight'])) {
							global $babAddonUrl;
							$tab['VideoImageUrl'] = $babAddonUrl.'user&idx=voirminiature&idvideo='.$videos[$i]->id.'&largeurvoulue='.$args['imagewidth'].'&hauteurvoulue='.$args['imageheight'];
						}
					}
					if (isset($args['playerwidth']) && is_numeric($args['playerwidth'])) {
						$videos[$i]->largeurlecteur = $args['playerwidth'];
					}
					if (isset($args['playerheight']) && is_numeric($args['playerheight'])) {
						$videos[$i]->hauteurlecteur = $args['playerheight'];
					}
					$tab['VideoHtmlCode'] = $videos[$i]->codehtml();
					$tab['VideoDescription'] = $videos[$i]->description;
					$donnees[] = $tab;
				}
				break;
			case 'Video':
			case 'video':
				if (isset($args['videoid'])) {
					if (is_numeric($args['videoid'])) {
						$video = new Multimediaspace_Video($args['videoid']);
						$video->charge();
						$tab = array();
						$tab['VideoId'] = $video->id;
						$tab['VideoFolderId'] = $video->idrepertoire;
						$tab['VideoType'] = 'flvfile';
						switch($video->type) {
							case 'flv':
								$tab['VideoType'] = 'flvfile';
								break;
							case '':
								$tab['VideoType'] = 'integratedvideo';
								break;
							case '':
								$tab['VideoType'] = 'urlextern';
								break;
						}
						$tab['VideoName'] = $video->nom;
						if ($video->type == 'url') {
							$tab['VideoUrl'] = $video->valeur;
						} else {
							$tab['VideoUrl'] = $video->url;
						}
						if (isset($args['playautomatically'])) {
							if (is_numeric($args['playautomatically'])) {
								if ($args['playautomatically'] == 'yes') {
									$video->jouerautomatiquement = 2;
								} else {
									$video->jouerautomatiquement = 1;
								}
							}
						}
						$tab['VideoUploadDate'] = $video->date;
						$tab['VideoWidth'] = $video->largeur;
						$tab['VideoHeight'] = $video->hauteur;
						$tab['PlayerWidth'] = $video->largeurlecteur;
						$tab['PlayerHeight'] = $video->hauteurlecteur;
						$tab['VideoImageUrl'] = $video->urlminiature;
						if (isset($args['imagewidth']) && is_numeric($args['imagewidth'])) {
							if (isset($args['imageheight']) && is_numeric($args['imageheight'])) {
								global $babAddonUrl;
								$tab['VideoImageUrl'] = $babAddonUrl.'user&idx=voirminiature&idvideo='.$video->id.'&largeurvoulue='.$args['imagewidth'].'&hauteurvoulue='.$args['imageheight'];
							}
						}
						if (isset($args['playerwidth']) && is_numeric($args['playerwidth'])) {
							$video->largeurlecteur = $args['playerwidth'];
						}
						if (isset($args['playerheight']) && is_numeric($args['playerheight'])) {
							$video->hauteurlecteur = $args['playerheight'];
						}
						$tab['VideoHtmlCode'] = $video->codehtml();
						$tab['VideoDescription'] = $video->description;
						$donnees[] = $tab;
					}
				}
				break;
		}
	}
	
	return $donnees;
}

