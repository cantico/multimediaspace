<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/fonctions.php';
require_once dirname(__FILE__) . '/fonctionsgen.php';


class multimediaspace_PortletDefinition_Video implements portlet_PortletDefinitionInterface
{

	public $name = 'Video';
	
	
	public function __construct()
	{
		$this->name = multimediaspace_traduire('Video');
	}

	public function getId()
	{
		return 'Video';
	}

	public function getName()
	{
		return $this->name;
	}


	public function getDescription()
	{
		return multimediaspace_traduire('Video');
	}


	public function getPortlet()
	{
		return new multimediaspace_Portlet_Video();
	}

	/**
	 * @return array
	 */
	public function getPreferenceFields()
	{
		require_once dirname(__FILE__).'/Multimediaspace_Repertoire.php';
		require_once dirname(__FILE__).'/Multimediaspace_Video.php';
		
		global $babDB;

		$sql = '
			SELECT * FROM ' . MULTIMEDIASPACE_REPERTOIRE . '
			WHERE id > 1';
		
		$repertoires = $babDB->db_query($sql);
		
		$videoOptions = array(
			array(
				'value' => '',
				'label' => ''
			)
		);
		while ($repertoire = $babDB->db_fetch_assoc($repertoires)) {
			$videoOptions[] = array(
				'value' => 'dir' . $repertoire['id'],
				'label' => $repertoire['nom']
			);
			
			$repertoire = new Multimediaspace_Repertoire($repertoire['id']);
			$videos = $repertoire->videos('date', 'decroissant');
			foreach ($videos as $video) {
				$videoOptions[] = array(
					'value' => $video->id,
					'label' => bab_nbsp() . bab_nbsp() . bab_nbsp() . bab_nbsp() . $video->nom
				);
			}
			
		}		


		
		$preferenceFields = array();
		
		$preferenceFields[] = array(
			'type' => 'int',
			'label' => multimediaspace_traduire('Width'),
			'name' => 'width'
		);
		
		$preferenceFields[] = array(
			'type' => 'int',
			'label' => multimediaspace_traduire('Height'),
			'name' => 'height'
		);
		
		$preferenceFields[] = array(
			'type' => 'list',
			'label' => multimediaspace_traduire('Video'),
			'name' => 'video',
			'options' => $videoOptions
		);		

		$preferenceFields[] = array(
			'type' => 'string',
			'label' => multimediaspace_traduire('Block title'),
			'name' => 'blockTitle'
		);

		return $preferenceFields;
	}


	/**
	 * Returns the widget rich icon URL.
	 * 128x128 ?
	 *
	 * @return string
	 */
	public function getRichIcon()
	{
		return '';
	}


	/**
	 * Returns the widget icon URL.
	 * 16x16 ?
	 *
	 * @return string
	 */
	public function getIcon()
	{
		return '';
	}

	/**
	 * Get thumbnail URL
	 * max 120x60
	 */
	public function getThumbnail()
	{
		return '';
	}

	public function getConfigurationActions()
	{
		return array();
	}
}





class multimediaspace_Portlet_Video extends Widget_Item implements portlet_PortletInterface
{
	private $portletId = null;

	private $width = null;

	private $height = null;

	private $video = null;
	
	private $definition = null;
	
	private $blockTitle = null;
	
	/**
	 * Instanciates the widget factory.
	 *
	 * @return Func_Widgets
	 */
	function Widgets()
	{
		$jquery = bab_functionality::get('jquery');
		$jquery->includeCore();
		$jquery->includeUi();
		$GLOBALS['babBody']->addStyleSheet($jquery->getStyleSheetUrl());
		if ($icons = @bab_functionality::get('Icons/Oxygen')) {
			$icons->includeCss();
		} else if ($icons = @bab_functionality::get('Icons')) {
			$icons->includeCss();
		}

		$W = bab_Functionality::get('Widgets');
		$W->includePhpClass('Widget_Icon');
		return $W;
	}


	/**
	 */
	public function __construct()
	{
		$W = $this->Widgets();

		$this->item = $W->VBoxItems();
	}


	public function getName()
	{
		return get_class($this);
	}


	public function getPortletDefinition()
	{
		if (!isset($this->definition)) {
			$this->definition = new multimediaspace_PortletDefinition_Video();
		}
		return $this->definition;
	}


	/**
	 * receive current user configuration from portlet API
	 */
	public function setPreferences(array $configuration)
	{
		foreach ($configuration as $name => $value) {
			$this->setPreference($name, $value);
		}
	}



	public function setPreference($name, $value)
	{
		if ($name === 'width') {
			$this->width = $value;
		}
		if ($name === 'height') {
			$this->height = $value;
		}
		if ($name === 'video') {
			$this->video = $value;
			
			if (empty($this->blockTitle) && !empty($value)) {
				
				require_once dirname(__FILE__).'/Multimediaspace_Video.php';
				require_once dirname(__FILE__).'/Multimediaspace_Repertoire.php';
				
				if (substr($value, 0, strlen('dir')) === 'dir') {
					$idrepertoire = substr($value, strlen('dir'));
					$obj = new Multimediaspace_Repertoire($idrepertoire);
					$obj->charge();
				} else {
					$obj = new Multimediaspace_Video($value);
					$obj->charge();
				}
			
				$def = $this->getPortletDefinition();
				$def->name = $obj->nom;
			}
		}
		if ($name === 'blockTitle') {
			if (!empty($value)) {
				$this->blockTitle = $value;
				$def = $this->getPortletDefinition();
				$def->name = $value;
			}
		}
	}


	public function setPortletId($id)
	{
		$this->portletId = $id;
	}





	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{
		$W = $this->Widgets();

		$box = multimediaspace_displayVideo($this->video, $this->width, $this->height);
		
		$display = $box->display($canvas);
		

		return $display;
	}



}



