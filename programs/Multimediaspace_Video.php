<?php
class Multimediaspace_Video {
	
	/* Variables */
	var $id; /* Id dans la base de donn�es */
	var $idrepertoire;
	var $idservice; /* Id du service concern� : youtube, dailymotion, googlevideo */
	var $nom;
	var $description;
	var $date;
	var $nomfichiervideo; /* Fichier vid�o : plac� dans le r�pertoire d'upload/videos/idrepertoire/ */
	var $type; /* flv, url (url d'acc�s � la vid�o), videointegree (code html embed fourni par des services comme youtube) (autres possibilit�s : avi, wmv, mpg, mov, flv, swf) */
	var $largeur; /* largeur du lecteur vid�o */
	var $hauteur; /* hauteur du lecteur vid�o */
	var $largeurlecteur; /* largeur du lecteur */
	var $hauteurlecteur; /* hauteur du lecteur */
	var $nomfichierminiature; /* image associ�e : plac�e dans le r�pertoire d'upload/images/idrepertoire/ */
	var $typeminiature;
	var $largeurminiature;
	var $hauteurminiature;
	var $nblecture; /* statistiques : nombre de fois visionn�e */
	var $auteur; /* ID Utilisateur Ovidentia */
	var $url; /* url d'acc�s � la vid�o et ses commentaires, valeur g�n�r�e : elle n'est pas enregistr�e en base de donn�es */
	var $urlminiature; /* url pour afficher la miniature, valeur g�n�r�e : elle n'est pas enregistr�e en base de donn�es */
	var $urltelechargementvideo; /* url pour t�l�charger le fichier video, valeur g�n�r�e : elle n'est pas enregistr�e en base de donn�es */
	var $valeur; /* peut �tre un code html ou une url, est utilis� pour les types youtubeintegration ou url */
	var $jouerautomatiquement; /* valeur 1 (ne joue pas automatiquement la vid�o d�s son chargement) ou 2 (jouer automatiquement la vid�o d�s son chargement) */
	
	/* Constructeur */
	function __construct($id='', $idrepertoire='', $idservice='', $nom='', $description='', $date='', $nomfichiervideo='', $type='', $largeur='', $hauteur='', $largeurlecteur='', $hauteurlecteur='', $nomfichierminiature='', $typeminiature='', $largeurminiature='', $hauteurminiature='', $nblecture=0, $auteur='', $valeur='', $jouerautomatiquement=1) {
		$this->id = $id;
		$this->idrepertoire = $idrepertoire;
		$this->idservice = $idservice;
		$this->nom = $nom;
		$this->description = $description;
		$this->date = $date;
		$this->nomfichiervideo = $nomfichiervideo;
		$this->type = $type;
		if ($this->type != 'template' && $this->type != 'flv' && $this->type != 'videointegree' && $this->type != 'url') {
			$this->type = 'flv';
		}
		$this->largeur = $largeur;
		$this->hauteur = $hauteur;
		$this->largeurlecteur = $largeurlecteur;
		$this->hauteurlecteur = $hauteurlecteur;
		$this->nomfichierminiature = $nomfichierminiature;
		$this->typeminiature = $typeminiature;
		$this->largeurminiature = $largeurminiature;
		$this->hauteurminiature = $hauteurminiature;
		$this->nblecture = $nblecture;
		$this->auteur = $auteur;
		$this->valeur = $valeur;
		global $babAddonUrl;
		$this->url = $babAddonUrl.'user&amp;idx=voirvideo&amp;idvideo='.$this->id;
		$this->urlminiature = $babAddonUrl.'user&amp;idx=voirminiature&amp;idvideo='.$this->id;
		$this->urltelechargementvideo = $babAddonUrl.'user&idx=telechargevideo&idvideo='.$this->id;
		$this->jouerautomatiquement = $jouerautomatiquement;
		if ($this->jouerautomatiquement != 1 && $this->jouerautomatiquement != 2) {
			$this->jouerautomatiquement = 1;
		}
		if ($this->type != 'flv') {
			$this->jouerautomatiquement = 1;
		}
	}
	
	/* Charge les donn�es enregistr�es dans la table de donn�es dans l'objet */
	public function charge()
	{
		$requete = "select `id`, `idrepertoire`, `idservice`, `nom`, `description`, `date`, `nomfichiervideo`, `type`, `largeur`, `hauteur`, `largeurlecteur`, `hauteurlecteur`, `nomfichierminiature`, `typeminiature`, `largeurminiature`, `hauteurminiature`, `nblecture`, `auteur`, `valeur`, `jouerautomatiquement` from `".MULTIMEDIASPACE_VIDEO."` where id='".$this->id."'";
		$idrequete = 0;
		$erreurs = array();
		$res = multimediaspace_sql($requete, $erreurs, $idrequete);
		if (count($res) == 1) {
			$this->idrepertoire = $res[0]['idrepertoire'];
			$this->idservice = $res[0]['idservice'];
			$this->nom = $res[0]['nom'];
			$this->description = $res[0]['description'];
			$this->date = $res[0]['date'];
			$this->nomfichiervideo = $res[0]['nomfichiervideo'];
			$this->type = $res[0]['type'];
			if ($this->type != 'template' && $this->type != 'flv' && $this->type != 'videointegree' && $this->type != 'url') {
				$this->type = 'flv';
			}
			$this->largeur = $res[0]['largeur'];
			$this->hauteur = $res[0]['hauteur'];
			$this->largeurlecteur = $res[0]['largeurlecteur'];
			$this->hauteurlecteur = $res[0]['hauteurlecteur'];
			$this->nomfichierminiature = $res[0]['nomfichierminiature'];
			$this->typeminiature = $res[0]['typeminiature'];
			$this->largeurminiature = $res[0]['largeurminiature'];
			$this->hauteurminiature = $res[0]['hauteurminiature'];
			$this->nblecture = $res[0]['nblecture'];
			$this->auteur = $res[0]['auteur'];
			$this->valeur = $res[0]['valeur'];
			global $babAddonUrl;
			$this->url = $babAddonUrl.'user&amp;idx=voirvideo&amp;idvideo='.$this->id;
			$this->urlminiature = $babAddonUrl.'user&amp;idx=voirminiature&amp;idvideo='.$this->id;
			$this->jouerautomatiquement = $res[0]['jouerautomatiquement'];
			if ($this->jouerautomatiquement != 1 && $this->jouerautomatiquement != 2) {
				$this->jouerautomatiquement = 1;
			}
		}
	}
	
	/* Enregistre/Modifie une vid�o */
	function enregistre() {
		if ($this->id == '') {
			/* On cr�� la nouvelle vid�o */
			$idrepertoire = $this->idrepertoire;
			$date = multimediaspace_datedujourdatetime();
			$nomfichiervideo = $this->nomfichiervideo;
			$type = $this->type;
			if ($type != 'template' && $type != 'flv' && $type != 'videointegree' && $type != 'url') {
				$type = 'flv';
			}
			$nom = $this->nom;
			$description = $this->description;
			$largeur = $this->largeur;
			$hauteur = $this->hauteur;
			$largeurlecteur = $this->largeurlecteur;
			$hauteurlecteur = $this->hauteurlecteur;
			$nomfichierminiature = $this->nomfichierminiature;
			$typeminiature = $this->typeminiature;
			$largeurminiature = $this->largeurminiature;
			$hauteurminiature = $this->hauteurminiature;
			$nblecture = $this->nblecture;
			$valeur = $this->valeur;
			$jouerautomatiquement = $this->jouerautomatiquement;
			if ($jouerautomatiquement != 1 && $jouerautomatiquement != 2) {
				$jouerautomatiquement = 1;
			}
			multimediaspace_echappesql($idrepertoire);
			multimediaspace_echappesql($nomfichiervideo);
			multimediaspace_echappesql($type);
			multimediaspace_echappesql($nom);
			multimediaspace_echappesql($description);
			multimediaspace_echappesql($largeur);
			multimediaspace_echappesql($hauteur);
			multimediaspace_echappesql($largeurlecteur);
			multimediaspace_echappesql($hauteurlecteur);
			multimediaspace_echappesql($nomfichierminiature);
			multimediaspace_echappesql($typeminiature);
			multimediaspace_echappesql($largeurminiature);
			multimediaspace_echappesql($hauteurminiature);
			multimediaspace_echappesql($nblecture);
			multimediaspace_echappesql($valeur);
			multimediaspace_echappesql($jouerautomatiquement);
			
			$requete = "insert into `".MULTIMEDIASPACE_VIDEO."` (  `idrepertoire`, `idservice` , `date` , `nomfichiervideo` , `type` , `nom` , `description` , `largeur` , `hauteur` , `largeurlecteur` , `hauteurlecteur` , `nomfichierminiature` , `typeminiature` , `largeurminiature` , `hauteurminiature` , `nblecture` , `valeur` , `jouerautomatiquement` )
					  values ( '$idrepertoire', '0' , '$date' , '$nomfichiervideo' , '$type' , '$nom' , '$description' , '$largeur' , '$hauteur' , '$largeurlecteur' , '$hauteurlecteur' , '$nomfichierminiature' , '$typeminiature' , '$largeurminiature' , '$hauteurminiature' , '$nblecture' , '$valeur', '$jouerautomatiquement' )
					";
			$idrequete = 0;
			
			multimediaspace_sql($requete, $erreurs, $idrequete);
			if (count($erreurs) == 0) {
				$this->id = multimediaspace_sqlid($idrequete);
				$erreurs[] = "Save done";
			}
		} else {
			/* On met � jour : on ne fait pas de d�placement, on modifie seulement le nom et les options */
			$id = $this->id;
			$nomfichiervideo = $this->nomfichiervideo;
			$type = $this->type;
			$nom = $this->nom;
			$description = $this->description;
			$largeur = $this->largeur;
			$hauteur = $this->hauteur;
			$largeurlecteur = $this->largeurlecteur;
			$hauteurlecteur = $this->hauteurlecteur;
			$nomfichierminiature = $this->nomfichierminiature;
			$typeminiature = $this->typeminiature;
			$largeurminiature = $this->largeurminiature;
			$hauteurminiature = $this->hauteurminiature;
			$nblecture = $this->nblecture;
			$valeur = $this->valeur;
			$jouerautomatiquement = $this->jouerautomatiquement;
			if ($jouerautomatiquement != 1 && $jouerautomatiquement != 2) {
				$jouerautomatiquement = 1;
			}
			multimediaspace_echappesql($nomfichiervideo);
			multimediaspace_echappesql($type);
			multimediaspace_echappesql($nom);
			multimediaspace_echappesql($description);
			multimediaspace_echappesql($largeur);
			multimediaspace_echappesql($hauteur);
			multimediaspace_echappesql($largeurlecteur);
			multimediaspace_echappesql($hauteurlecteur);
			multimediaspace_echappesql($nomfichierminiature);
			multimediaspace_echappesql($typeminiature);
			multimediaspace_echappesql($largeurminiature);
			multimediaspace_echappesql($hauteurminiature);
			multimediaspace_echappesql($nblecture);
			multimediaspace_echappesql($valeur);
			multimediaspace_echappesql($jouerautomatiquement);
			
			$requete = "update `".MULTIMEDIASPACE_VIDEO."` set `nomfichiervideo`='".$nomfichiervideo."', `type`='".$type."', `nom`='".$nom."', `description`='".$description."', `largeur`='".$largeur."', `hauteur`='".$hauteur."', `largeurlecteur`='".$largeurlecteur."', `hauteurlecteur`='".$hauteurlecteur."', `nomfichierminiature`='".$nomfichierminiature."', `typeminiature`='".$typeminiature."', `typeminiature`='".$typeminiature."', `largeurminiature`='".$largeurminiature."', `hauteurminiature`='".$hauteurminiature."', `nblecture`='".$nblecture."', `valeur`='".$valeur."', `jouerautomatiquement`='".$jouerautomatiquement."' where id='".$id."'";
			
			multimediaspace_sql($requete, $erreurs, $idrequete="");
			if (count($erreurs) == 0) {
				$erreurs[] = "Save done";
			}
		}
	}
	
	/* Retourne la liste des commentaires */
	function commentaires() {
		$commentaires = array();
		
		return $commentaires;
	}
	
	/* Retourne le nombre des commentaires */
	function nbcommentaires() {
		$nbcommentaires = 0;
		
		return $nbcommentaires;
	}
	
	/**
	 *  Verifie si l'utilisateur courant a les droits de lecture sur le repertoire.
	 */
	function adroitslecture()
	{
		$repertoire = $this->repertoire();
		if ($repertoire->adroitslecture(false)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Retourne le code HTML pour afficher la video dans la page dediee (le type url n'a pas de page dediee)
	 */
	public function codehtml()
	{		
		$code = '';
		
		if ($this->type == 'flv') {
			/* Recherche du lecteur � utiliser */
			$configurationModule = multimediaspace_configurationmodule();
			if ($configurationModule['lecteur_video_par_defaut'] == 'flvplayer_maxi') {
				$cheminLecteurVideo = 'flvplayer/player_flv_maxi.swf';
			} else {
				$cheminLecteurVideo = 'flvplayer/player_flv_mini.swf';
			}
			
			/* Avec FLV Player, on utilise la largeur et la hauteur du lecteur */
			$code .= '
					<!-- Player : http://flv-player.net/
					     Author : http://portfolio.neolao.com
						 Licences : Creative Commons BY SA (http://creativecommons.org/licenses/by-sa/3.0/deed.fr)
								    MPL 1.1 (http://www.mozilla.org/MPL/)
					-->
					<object type="application/x-shockwave-flash" data="'.multimediaspace_urltemplatesmodule().'lecteurs/'.$cheminLecteurVideo.'" width="'.$this->largeurlecteur.'" height="'.$this->hauteurlecteur.'">
					    <param name="movie" value="'.multimediaspace_urltemplatesmodule().'lecteurs/'.$cheminLecteurVideo.'" />
					    <param name="allowFullScreen" value="true" />';
						if ($this->jouerautomatiquement == 2) {
							$autoplay = 1;
						} else {
							$autoplay = 0;
						}
			$code .= '  <param name="FlashVars" value="flv='.urlencode($this->cheminvideopourlecteur()).'&amp;title='.urlencode(multimediaspace_chainesansaccents($this->nom)).'&amp;autoplay='.$autoplay.'&amp;autoload=1&amp;margin=0&amp;showstop=1&amp;showvolume=1&amp;showtime=1&amp;showfullscreen=1&width='.$this->largeurlecteur.'&amp;height='.$this->hauteurlecteur.'" />
						<embed src="'.multimediaspace_urltemplatesmodule().'lecteurs/'.$cheminLecteurVideo.'" width="'.$this->largeurlecteur.'" height="'.$this->hauteurlecteur.'" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="flv='.urlencode($this->cheminvideopourlecteur()).'&amp;title='.urlencode(multimediaspace_chainesansaccents($this->nom)).'&amp;autoplay='.$autoplay.'&amp;autoload=1&amp;margin=0&amp;showstop=1&amp;showvolume=1&amp;showtime=1&amp;showfullscreen=1&amp;width='.$this->largeurlecteur.'&amp;height='.$this->hauteurlecteur.'" />
					</object>';
			
			/* Avec le lecteur JW FLV Player */
			/*$code .= '
					<!-- Player : JW FLV Media Player 4.3
					     Author : http://www.longtailvideo.com/
					-->
					<script type="text/javascript" src="'.multimediaspace_urltemplatesmodule().'lecteurs/jw_flv_media_player/swfobject.js"></script>
					<div id="mediaspace">This div will be replaced</div>
				  	<script type="text/javascript">
						  var s1 = new SWFObject(\''.multimediaspace_urltemplatesmodule().'lecteurs/jw_flv_media_player/player.swf\',\'ply\',\''.$this->largeur.'\',\''.$this->hauteur.'\',\'5\',\'#ffffff\');
						  s1.addParam(\'allowfullscreen\',\'true\');
						  s1.addParam(\'allowscriptaccess\',\'always\');
						  s1.addParam(\'wmode\',\'opaque\');
						  s1.addParam(\'flashvars\',\'file='.urlencode($this->cheminvideopourlecteur()).'&controlbar=over&fullscreen=true&stretching=fill\');
						  s1.write(\'mediaspace\');
					</script>
					';*/
		}
		if ($this->type == 'videointegree') {
			$code .= $this->valeur;
		}

		if ($this->type == 'template') {

			$parameters = unserialize($this->valeur);
			$templateName = $parameters['template'];
			unset($parameters['template']);
			
			$template = multimediaspace_getVideoTemplate($templateName);
			
			$body = $template['body'];
			
			$addon = bab_getAddonInfosInstance('multimediaspace');
			$parameters['__IMAGE__'] = $addon->getUrl().'user&amp;idx=voirminiature&amp;idvideo='.$this->id;
			if (!empty($this->largeur)) {
				$parameters['__WIDTH__'] = $this->largeur;
				$parameters['__IMAGE__'] .= '&amp;largeurvoulue=' . $this->largeur;
			} else {
				$parameters['__IMAGE__'] .= '&amp;largeurvoulue=360';
			}
			if (!empty($this->hauteur)) {
				$parameters['__HEIGHT__'] = $this->hauteur;
				$parameters['__IMAGE__'] .= '&amp;hauteurvoulue=' . $this->hauteur;
			} else {
				$parameters['__IMAGE__'] .= '&amp;hauteurvoulue=300';
			}
						
			foreach ($parameters as $paramName => $paramValue) {
				$body = str_replace($paramName, $paramValue, $body);
			}
			$code .= $body;
			
		}
		
		/* Type URL : pas de page d�di�e */
		
		//so.addParam("flashvars","&file=video.flv");
		//so.addParam("flashvars","&file='.urlencode($this->urlvideoheader).'");
		//so.addParam("flashvars","&file='.urlencode($this->cheminvideo()).'");
		
		/*if ($this->type == 'flv') {
			//Cr�� un objet SWFObject (la librairie swfobject doit �tre pr�sente)
			$code .= '<script type="text/javascript">
					    var so = new SWFObject("flvplayer.swf","player","400","4003","7");
					    so.addParam("allowfullscreen","true"); // permet d\'utiliser le mode plein �cran
					    so.addVariable("file","video.flv"); // d�termine l\'emplacement de la vid�o � jouer
					    so.addVariable("displayheight","300"); // d�termine la taille de l\'affichage de la vid�o
					    so.write("player");
					</script>';
			
			//Code � afficher si le navigateur n'a pas activ� le javascript
			$code .= '<embed
					    src="http://www.monsite.com/mediaplayer.swf"
					    width="400"
					    height="400"
					    type="application/x-shockwave-flash"
					    pluginspage="http://www.macromedia.com/go/getflashplayer"
					    flashvars="file=http://www.monsite.com/video.flv&displayheight=300"	/>';
		}*/
		
		/*
		Balise embed :
		<embed></embed>
		SRC: URL of resource to be embedded
		border:
		Hspace: Param�tre la largeur (en pixels) de l'espace r�serv� entre le texte et le bord gauche/droit du cadre dans lequel est incrust� l'objet  	   	 
		Vspace: Param�tre la hauteur (en pixels) de l'espace r�serv� entre le texte et le bord sup�rieur/inf�rieur du cadre dans lequel est incrust� l'objet
	    WIDTH: width of area in which to show resource
	    HEIGHT: height of area in which to show resource
	    ALIGN: how text should flow around the picture (ABSBOTTOM | ABSMIDDLE | MIDDLE | TEXTTOP | RIGHT | LEFT | BASELINE | CENTER | BOTTOM | TOP)
	    NAME: name of the embedded object
	    PLUGINSPAGE: where to get the plugin software
	    PLUGINURL: where to get the JAR archive for automatic installation (for Netscape)
	    HIDDEN: indicates if the embedded object is visible or not. FALSE is the default.
	    HREF: make this object a link
	    TARGET: frame to link to
	    AUTOSTART: if the sound/movie should start automatically
		autoplay: true ou false
	    LOOP: how many times to play the sound/movie, true or false
	    PLAYCOUNT: how many times to play the sound/movie
	    VOLUME: how loud to play the sound, 0 � 100
	    CONTROLS: which sound control to display for Netscape (VOLUMELEVER | STOPBUTTON | PAUSEBUTTON | PLAYBUTTON | SMALLCONSOLE | CONSOLE)
	    CONTROLLER: if controls should be displayed (true ou false) for IE
	    MASTERSOUND: indicates the object in a sound group with the sound to use (seulement pour un son et Netscape)
	    STARTTIME: how far into the sound to start and stop
	    ENDTIME: when to finish playing
		type: application/x-shockwave-flash, video/windowsmedia, audio/x-mpegurl, application/mpeg, image/jpeg...
		quality: high
		
		Balise object :
		
		<noembed>
		<object>
		  <param name="movie" value="mavideo.swf" />
		  <param name="quality" value="high" />
		</object>
		</noembed>

		Peut se cascader. Ici l'image sera affich� si la vid�o n'est pas interpr�t�e,
		le texte sera affich� si l'image n'est pas affich�e :
		<OBJECT data="proddemo.mpeg" type="application/mpeg">
		    <OBJECT data="prodStill.jpg" type="image/jpeg">
		        Le nouveau Gadget 3000 !
		    </OBJECT>
		</OBJECT>

		Attributs de object :
		ID="terre"
		CLASSID="clsid:83A38BF0-B33A-A4FF-C619A82E891D", clsid:02BF25D5- 8C17- 4B23-BC80- D3488ABDDC6B (r�f�rence au contr�le ActiveX)
		style="width:320px;height:260px"
		codebase: 
		data="movie.swf"
		type="application/x-shockwave-flash", "application/mpeg", "image/jpeg"
		width="500"
		height="500"
		standby="chargement en cours" : pr�cise le texte affich� pendant le chargement du m�dia

		Plugins � lancer :
		Divx Web Player : <embed pluginspage="http://labs.divx.com/Player" type="video/divx" src="dede.avi" width="280" height="200" />
		Windows Media Player :
		<embed type="application/x-mplayer2" src="dede.avi" />
		ou
		<OBJECT width=175 height=30 classid="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95"
			codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Versi�on=6,0,02,902"
			standby="Chargement de Microsoft Windows Media Player..." type="application/x-oleobject">
			<PARAM NAME="FileName" VALUE="le chemin complet du fichier">
			<PARAM NAME="animationatStart" VALUE="true">
			<PARAM NAME="transparentatStart" VALUE="true">
			<PARAM NAME="autoStart" VALUE="false">
			<PARAM NAME="showControls" VALUE="true">
			<PARAM NAME="autoSize" VALUE="0">
			<EMBED type="application/x-mplayer2" pluginspage = "http://www.microsoft.com/Windows/MediaPlayer/"
				SRC="le chemin complet du fichier" width="175" height="30" AutoStart="false"
				autosize="0" transparentatStart="true" animationatStart="true" showControls="true">
			</EMBED>
		</object>
		ou
		<object type=\"application/x-mplayer2\" width=\"\" height=\"\">
			<param name=\"autostart\" value=\"true\" />
			<param name=\"showcontrols\" value=\"true\" />
			<param name=\"filename\" value=\"*.mpg\" />
		</object>
		
		Quicktime :
		<embed type="video/quicktime" src="dede.mov" controller="true" width="300" height="300"/>
		ou
		<OBJECT CLASSID="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" WIDTH="160" HEIGHT="16"
			CODEBASE="http://www.apple.com/qtactivex/qtplugin.cab">
			<PARAM name="SRC" VALUE="le chemin complet du fichier">
			<PARAM name="AUTOPLAY" VALUE="false">
			<PARAM name="LOOP" VALUE="false">
			<PARAM name="CONTROLLER" VALUE="true">
			<EMBED type="audio/x-mpegurl" src="le chemin complet du fichier" autoplay="false" width="160px"
			height="16px" loop="false" controller="true" PLUGINSPAGE="http://www.apple.com/quicktime/download/">
			</EMBED>
		</OBJECT> 
		ou
		<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" width="" height="">
			<param name="src" value="*.mov" />
			<param name="controller" value="true" />
			<object type="video/quicktime" data="/images/peint.mov" width="" height="" class="mov">
				<param name="controller" value="true" />
			</object>
		</object>


		Real Player : .rm
		<embed type="audio/x-pn-realaudio-plugin" src="dede.rm" height="200px" width="200px" controls="all" console="video"/>
		ou
		<OBJECT CLASSID="clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA" WIDTH="175" HEIGHT="30">
			<PARAM NAME="CONTROLS" VALUE="ControlPanel">
			<PARAM NAME="CONSOLE" VALUE="Clip1">
			<PARAM NAME="AUTOSTART" VALUE="false">
			<PARAM NAME="nologo" VALUE="true">
			<PARAM NAME="NOJAVA" VALUE="true">
			<PARAM NAME="SRC" VALUE="http://le chemin complet du fichier">
			<PARAM NAME="LOOP" VALUE="false">
			<EMBED SRC="le chemin complet du fichier3" WIDTH="175" HEIGHT="30"
			TYPE="audio/x-pn-realaudio-plugin"
			NOLOGO="true" NOJAVA="true" CONTROLS="ControlPanel" CONSOLE="Clip1"
			AUTOSTART="false" LOOP="false"></EMBED>
		</OBJECT>
		ou
		<object classid=\"clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA\" width=\"\" height=\"\">
			<param name=\"autostart\" value=\"true\" />
			<param name=\"shuffle\" value=\"0\" />
			<param name=\"prefetch\" value=\"0\" />
			<param name=\"nolabels\" value=\"0\" />
			<param name=\"src\" value=\"*.rm\" />
			<param name=\"controls\" value=\"ImageWindow\" />
			<param name=\"console\" value=\"video1\" />
			<param name=\"loop\" value=\"0\" />
			<param name=\"numloop\" value=\"0\" />
			<param name=\"center\" value=\"0\" />
			<param name=\"maintainaspect\" value=\"0\" />
			<!--[if !IE]> <-->
				<object type=\"audio/x-pn-realaudio-plugin\" data=\"../Videos/Reaction_public.rm\" width=\"\" height=\"\">
					<param name=\"autostart\" value=\"true\" /><param name=\"controls\" value=\"ImageWindow\" />
				</object>
			<!--> <! endif -->
		</object>
		
		Assistant youtube :
		http://www.google.com/uds/solutions/wizards/videobar.html
		
		Fichier swf :
		<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0">
			<param name="movie" value="skins/nomduskin/ovml/nomdufichier.swf" />
			<param name="quality" value="high" />
			<param name="wmode" value="transparent" />
			<embed width="550" height="400" src="skins/nomduskin/ovml/nomdufichier.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent" /> 
		</object>
		
		*/
		return $code;
	}

	
	
	
	
	/* Retourne le chemin de la vid�o */
	function cheminvideo() {
		$chemin = '';
		if ($this->nomfichiervideo != '') {
			$repertoire = $this->repertoire();
			$chemin = $repertoire->cheminrepertoirevideos().'/'.$this->nomfichiervideo;
		}
		return $chemin;
	}
	
	/* Retourne le chemin de la vid�o par rapport au lecteur flash */
	function cheminvideopourlecteur() {
		$chemin = '';
		if ($this->nomfichiervideo != '') {
			$repertoire = $this->repertoire();
			$chemin = $repertoire->cheminrepertoirevideospourlecteur().'/'.$this->nomfichiervideo;
		}
		return $chemin;
	}
	
	/* Retourne le chemin de la miniature */
	function cheminminiature() {
		$chemin = '';
		if ($this->nomfichierminiature != '') {
			$repertoire = $this->repertoire();
			$chemin = $repertoire->cheminrepertoireimages().'/'.$this->nomfichierminiature;
		}
		return $chemin;
	}
	
	/* Retourne le r�pertoire dont fait partie la vid�o */
	function repertoire()
	{
		require_once dirname(__FILE__) . '/Multimediaspace_Repertoire.php';
		$repertoire = new Multimediaspace_Repertoire($this->idrepertoire);
		$repertoire->charge();
		return $repertoire;
	}
	
	/* Supprime l'objet */
	function supprime() {
		if ($this->id != '') {		
			/* Suppression de la vid�o */
			$requete = "delete from `".MULTIMEDIASPACE_VIDEO."` where id='".$this->id."'";
			$idrequete = 0;
			$erreurs = array();
			$res = multimediaspace_sql($requete, $erreurs, $idrequete);
			
			/* Suppression du fichier r�el de la vid�o */
			if ($this->nomfichiervideo != '') {
				multimediaspace_supprimefichier($this->cheminvideo());
			}
			
			/* Suppression du fichier r�el de la miniature */
			if ($this->nomfichierminiature != '') {
				multimediaspace_supprimefichier($this->cheminminiature());
			}
		}
	}
	
	/* Supprime la miniature */
	function supprimeminiature() {
		if ($this->nomfichierminiature != '') {
			/* Suppression du fichier r�el de la miniature */
			multimediaspace_supprimefichier($this->cheminminiature());
			$this->nomfichierminiature = '';
			$this->typeminiature = '';
			$this->largeurminiature = '';
			$this->hauteurminiature = '';
			$this->enregistre();
		}
	}
	
	/* Supprime la vid�o */
	function supprimevideo() {
		if ($this->nomfichiervideo != '') {
			/* Suppression du fichier r�el de la vid�o */
			multimediaspace_supprimefichier($this->cheminvideo());
			$this->nomfichiervideo = '';
			$this->enregistre();
		}
	}

}
