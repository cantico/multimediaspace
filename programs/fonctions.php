<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
************************************************************************
* Copyright (c) 2008 by CANTICO ( http://www.cantico.fr )              *
************************************************************************/
include_once 'base.php';

@include_once $GLOBALS['babInstallPath'].'utilit/registerglobals.php';


//Chargement des fonctions
global $babAddonPhpPath;
global $babInstallPath;
require_once dirname(__FILE__).'/fonctionsgen.php';

//D�finition des constantes
//Tables de donn�es
define ('MULTIMEDIASPACE_DROITSGEST', 'multimediaspace_droitsgest');
define ('MULTIMEDIASPACE_CONFIGURATION', 'multimediaspace_configuration');
define ('MULTIMEDIASPACE_REPERTOIRE', 'multimediaspace_repertoire');
define ('MULTIMEDIASPACE_DROITSLECTUREREPERTOIRE', 'multimediaspace_droitslecturerepertoire');
define ('MULTIMEDIASPACE_VIDEO', 'multimediaspace_video');
define ('MULTIMEDIASPACE_COMMENTAIRE', 'multimediaspace_commentaire');
define ('MULTIMEDIASPACE_ANTIF5', 'multimediaspace_antif5');
define ('MULTIMEDIASPACE_FICHIEROVML', 'multimediaspace_fichierovml');

//Chargement des classes
include_once dirname(__FILE__).'/Multimediaspace_EspaceMultimedia.php';
include_once dirname(__FILE__).'/Multimediaspace_Repertoire.php';
include_once dirname(__FILE__).'/Multimediaspace_Video.php';
include_once dirname(__FILE__).'/Multimediaspace_Commentaire.php';

//Fonctions

/* V�rifie si l'utilisateur courant est gestionnaire du module */
function multimediaspace_estgestionnaire() {
	if (multimediaspace_verifieacl(1, MULTIMEDIASPACE_DROITSGEST)) {
		return true;
	}
	return false;
}

/* Renvoie la configuration du module */
function multimediaspace_configurationmodule() {
	//Recherche de la valeur enregistr�e dans la base
	$requete = "select `lien`, `expediteur_nom`, `expediteur_mail`, `lecteur_video_par_defaut` from ".MULTIMEDIASPACE_CONFIGURATION;
	$resultats = multimediaspace_sql($requete, $erreurs, $idrequete="");
	$lien = $resultats[0]['lien'];
	$expediteur_nom = $resultats[0]['expediteur_nom'];
	$expediteur_mail = $resultats[0]['expediteur_mail'];
	$lecteur_video_par_defaut = $resultats[0]['lecteur_video_par_defaut'];
	$tab = array('lien' => $lien, 'expediteur_nom' => $expediteur_nom, 'expediteur_mail' => $expediteur_mail, 'lecteur_video_par_defaut' => $lecteur_video_par_defaut);
	return $tab;
}

/* Ajoute un lien Ins�rer une vid�o dans la popup des fonctions d'Ovidentia dans l'�diteur wysiwyg */
function multimediaspace_editeurwysiwyg_lien(&$evenement) {
	global $babAddonUrl, $babAddonHtmlPath;
	/* V�rifie les droits d'acc�s : on affiche le lien si l'utilisateur est gestionnaire
	 ou a des droits de lecture dans au moins un r�pertoire */
	$affichelien = false;
	if (multimediaspace_estgestionnaire()) {
		$affichelien = true;
	}
	$espace = new Multimediaspace_EspaceMultimedia();
	if ($espace->adroitslecture()) {
		$affichelien = true;
	}
	if ($affichelien) {
		$evenement->addFunction(
				multimediaspace_traduire('Video'),
				multimediaspace_traduire('Insert a video from the multimedia space'),
				$babAddonUrl.'user&idx=editeurwysiwyglisterepertoires',
				'skins/ovidentia/templates/'.$babAddonHtmlPath.'images/video2.gif'
		);
	}
}

/* Renvoie la liste des vid�os (objets Multimediaspace_Video) en tenant compte des droits et des filtres
 * Si $idrepertoire est renseign�, on prendra les vid�os de ce r�pertoire et de ses sous-r�pertoires si
* 	$prendencomptelessousrepertoires vaut vrai
* Si $idrepertoire n'est pas renseign�, il faut que $prendencomptelessousrepertoires vaille vrai pour r�cup�rer
* 	toutes les vid�os de tous les r�pertoires
* $nomchamppourlordre : date, id, nom, type, largeur, hauteur, auteur
* Si $tenirCompteDesDroitsDAcces vaut false, on ne tient pas compte des droits d'acc�s
*/
function multimediaspace_listevideos($idrepertoire='', $prendencomptelessousrepertoires=false, $nomchamppourlordre='date', $ordrealphabetique='croissant', $index='', $nbvideos='', $tenirCompteDesDroitsDAcces=true) {
	$videos = array();

	/* On recherche dans un r�pertoire */
	if ($idrepertoire != '') {
		//V�rifie les droits d'acc�s
		$repertoire = new Multimediaspace_Repertoire($idrepertoire);
		$repertoire->charge();
		if ($tenirCompteDesDroitsDAcces) {
			if (!$repertoire->adroitslecture($prendencomptelessousrepertoires)) {
				return $videos; /* Pas de droits d'acc�s, on retourne de suite le tableau des vid�os vide */
			}
		}
	}
	/* On r�cup�re les sous-r�pertoires du r�pertoire d�sir� */
	$idrepertoires = '';
	if ($idrepertoire != '' && $prendencomptelessousrepertoires) {
		$repertoire = new Multimediaspace_Repertoire($idrepertoire);
		$repertoire->charge();
		$sousrepertoires = $repertoire->sousrepertoires(true);
		for($i=0;$i<=count($sousrepertoires)-1;$i++) {
			if ($i == 0) {
				$idrepertoires .= $sousrepertoires[$i]->id;
			} else {
				$idrepertoires .= ','.$sousrepertoires[$i]->id;
			}
		}
	}
	if ($idrepertoire == '' && $prendencomptelessousrepertoires) {
		$espace = new Multimediaspace_EspaceMultimedia();
		$sousrepertoires = $espace->repertoires(true);
		for($i=0;$i<=count($sousrepertoires)-1;$i++) {
			if ($i == 0) {
				$idrepertoires .= $sousrepertoires[$i]->id;
			} else {
				$idrepertoires .= ','.$sousrepertoires[$i]->id;
			}
		}
	}
	if ($idrepertoire == '' && !$prendencomptelessousrepertoires) {
		return $videos;
	}

	/* Champ pour ordonner */
	switch($nomchamppourlordre) {
		case 'date':
		case 'id':
		case 'nom':
		case 'type':
		case 'largeur':
		case 'hauteur':
		case 'auteur':
			break;
		default:
			$nomchamppourlordre = 'date';
		break;
	}

	/* V�rifie si un nombre maximum de vid�os est d�sir� (pour multi-pages par exemple) */
	if ($index == '' && $nbvideos != '') {
		$index = 0;
	}

	/* Construction de la requ�te */
	$requete = "select `id` from `".MULTIMEDIASPACE_VIDEO."` ";
	$where = false;
	if ($idrepertoire != '') {
		if ($where) {
			$requete .= " and idrepertoire='".$idrepertoire."' ";
		} else {
			$requete .= " where idrepertoire='".$idrepertoire."' ";
		}
		$where = true;
	}
	if ($idrepertoires != '') {
		if ($where) {
			$requete .= ' or `idrepertoire` in ('.$idrepertoires.')';
		} else {
			$requete .= ' where `idrepertoire` in ('.$idrepertoires.')';
		}
		$where = true;
	}
	$requete .= "order by ".$nomchamppourlordre." ";
	if ($ordrealphabetique == 'croissant') {
		$requete .= 'asc';
	} else {
		$requete .= 'desc';
	}
	if ($index != '' && $nbvideos != '') {
		$requete .= ' limit '.$index.','.$nbvideos;
	}

	$idrequete = 0;
	$erreurs = array();
	$res = multimediaspace_sql($requete, $erreurs, $idrequete);
	for($i=0;$i<=count($res)-1;$i++) {
		$video = new Multimediaspace_Video($res[$i]['id']);
		$video->charge();
		$videos[] = $video;
	}
	return $videos;
}

/* Retourne la liste des fichiers OVML pouvant �tre utilis�s dans les fonctions de l'�diteur wysiwyg */
function multimediaspace_fichiersovml() {
	//Recherche de la valeur enregistr�e dans la base
	$requete = "select `id`, `cheminfichierovml`, `description` from ".MULTIMEDIASPACE_FICHIEROVML;
	return $resultats = multimediaspace_sql($requete, $erreurs, $idrequete="");
}

/* Retourne l'url sans nom de domaine : utilis� pour les urls dans l'�diteur wysiwyg afin que l'�diteur affiche les images et les vid�os */
function multimediaspace_urlsansnomdedomaine($url) {
	global $babAddonUrl;
	/* On transforme l'url vers l'image car l'�diteur wysiwyg supprime automatiquement la racine et n'affiche pas l'image lorsqu'on revient en �dition de l'article */
	/* On supprime tout ce qu'il y a avant // et // */
	if (strpos($url, '//') !== false) {
		$url = substr($url, strpos($url, '//')+2, strlen($url) - strpos($url, '//') - 2);
	}
	/* Position du ? */
	$pospoint = strpos($url, '?');
	/* Position du slash juste avant le ? */
	$url2 = substr($url, 0, $pospoint);
	$posdernierslash = strrpos($url2, '/');
	/* Recherche du premier slash apr�s http:// */
	$pospremierslash = strpos($url, '/');
	$url = substr($url, $pospremierslash); //On garde le slash devant l'url
	return $url;
}

/* Retourne une cha$ine sans les accents : destin� au titre de la vid�o affich� par le lecteur
 ainsi que pour renommer un fichier vid�o d�pos� sur le portail */
function multimediaspace_chainesansaccents($chaine) {
	$chaine = str_replace('�', 'a', $chaine);
	$chaine = str_replace('�', 'a', $chaine);
	$chaine = str_replace('�', 'e', $chaine);
	$chaine = str_replace('�', 'e', $chaine);
	$chaine = str_replace('�', 'e', $chaine);
	$chaine = str_replace('�', 'u', $chaine);
	$chaine = str_replace('�', 'u', $chaine);
	$chaine = str_replace('�', 'i', $chaine);
	$chaine = str_replace('�', 'c', $chaine);
	return $chaine;
}







/**
 * @return array
 */
function multimediaspace_getVideoTemplates()
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/multimediaspace/templates/');

	$templates = array();
	while ($dir = $registry->fetchChildDir()) {
		$templates[$dir] = $dir;
	}

	return $templates;
}




function multimediaspace_getVideoTemplate($templateName)
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/multimediaspace/templates/' . $templateName);

	$template = array();

	$template['name'] = $registry->getValue('name', '');
	$template['description'] = $registry->getValue('description', '');
	$template['body'] = $registry->getValue('body', '');
	$template['parameters'] = $registry->getValue('parameters', '');

	return $template;
}




function multimediaspace_saveVideoTemplate($template)
{
	if (!is_array($template) || empty($template['name'])) {
		return false;
	}
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/multimediaspace/templates');
	$registry->changeDirectory($template['name']);
	$registry->setKeyValue('name', $template['name']);
	$registry->setKeyValue('description', $template['description']);
	$registry->setKeyValue('body', $template['body']);
	$registry->setKeyValue('parameters', $template['parameters']);

	return true;
}


function multimediaspace_deleteVideoTemplate($templateName)
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/multimediaspace/templates');
	$registry->changeDirectory($templateName);
	return $registry->deleteDirectory();
}


function multimediaspace_extractVideoTemplateParameters($parametersString)
{
	$parameters = array();
	$lines = explode("\n", $parametersString);
	foreach ($lines as $line) {
		$line = trim($line);
		list($paramName, $paramDescription) = explode(',', $line);
		$parameters[$paramName] = $paramDescription;
	}

	return $parameters;
}




/**
 * Instanciates the widget factory.
 *
 * @return Func_Widgets
 */
function multimediaspace_Widgets()
{
	$jquery = bab_functionality::get('jquery');
	$jquery->includeCore();
	$jquery->includeUi();
	$GLOBALS['babBody']->addStyleSheet($jquery->getStyleSheetUrl());
	if ($icons = @bab_functionality::get('Icons/Oxygen')) {
		$icons->includeCss();
	} else if ($icons = @bab_functionality::get('Icons')) {
		$icons->includeCss();
	}

	$W = bab_Functionality::get('Widgets');
	$W->includePhpClass('Widget_Icon');
	return $W;
}


/**
 * @param string $video
 * @param string $width
 * @param string $height
 *
 * @return string The video code.
 */
function multimediaspace_displayVideo($video, $width = null, $height = null)
{
	global $babDB;

	$W = multimediaspace_Widgets();

	$box = $W->VBoxItems();

	require_once dirname(__FILE__).'/Multimediaspace_Video.php';
	require_once dirname(__FILE__).'/Multimediaspace_Repertoire.php';

	if (substr($video, 0, strlen('dir')) === 'dir') {
		$idrepertoire = substr($video, strlen('dir'));
		$repertoire = new Multimediaspace_Repertoire($idrepertoire);
		$videos = $repertoire->videos('date', 'decroissant');
		$video = $videos[0];
	} else {
		$video = new Multimediaspace_Video($video);
		$video->charge();
	}
	if (!$video || !$video->adroitslecture()) {
		$box->addItem(
			$W->Html('<div style="padding: 1em 0; text-align: center;">' . bab_toHtml(multimediaspace_traduire('No video')) . '</div>')
		);
		return $box;
	}
	

	if (!empty($width)) {
		$video->largeur = $width;
	}
	if (!empty($height)) {
		$video->hauteur = $height;
	}
	$html = $video->codehtml();


	$box->addItem($W->Html($html));

	return $box;
}
