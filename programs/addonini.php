; <?php /*

[general]
name="multimediaspace"
version="2.3"
addon_type="EXTENSION"
encoding="ISO-8859-15"
mysql_character_set_database="latin1,utf8"
description="Video player"
delete=1
longdesc=""
db_prefix="multimediaspace_"
ov_version="6.7"
php_version="5.0.0"
mysql_version="3.23"
author="J�r�me Aizier (Cantico)"

[addons]
widgets				="1.0.18"

; */ ?>
