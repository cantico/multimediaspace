<?php
class Multimediaspace_EspaceMultimedia {
	
	/* Variables */
	
	/* Constructeur */
	function __construct() {
		
	}
	
	/* V�rifie si l'utilisateur courant a des droits de lecture dans au moins un r�pertoire */
	function adroitslecture() {
		$repertoires = $this->repertoires(true);
		for($i=0;$i<=count($repertoires)-1;$i++) {
			if ($repertoires[$i]->adroitslecture()) {
				return true;
			}
		}
		return false;
	}
	
	/* Retourne le r�pertoire parent : le r�pertoire nomm� Espace Multim�dia qui n'est pas affich� dans l'arborescence,
	   il permet simplement d'utiliser la repr�sentation intervallaire */
	function repertoireparent() {
		$requete = "select `id` from `".MULTIMEDIASPACE_REPERTOIRE."` where `bornegauche` = '1'";
		$idrequete = 0;
		$erreurs = array();
		$res = multimediaspace_sql($requete, $erreurs, $idrequete);
		if(count($res) == 1) {
			$repertoire = new Multimediaspace_Repertoire($res[0]['id']);
			$repertoire->charge();
			return $repertoire;
		}
		return false;
	}
	
	/* Renvoie la liste des sous-r�pertoires en tenant compte du premier niveau ou de tous les sous-niveaux */
	function repertoires($prendreencomptelessousniveaux=false) {
		/*
		Aide :
		- Tous les �l�ments d�pendant d'un �l�ment de r�f�rence (sous arbre) :
			select * from   NEW_FAMILLE where  NFM_BG > 22   and NFM_BD < 35 order by NFM_BG asc
		- Tous les p�res d'un �l�ment de r�f�rence :
			select * from   NEW_FAMILLE where  NFM_BG < 29   and NFM_BD > 34 order by NFM_BG asc
		*/
		
		/* R�cup�re le r�pertoire parent pour conna�tre ses bornes gauche et droite */
		$repertoireparent = $this->repertoireparent();
		$ssrep = array();
		/* R�cup�re tous les r�pertoires enfants (sous-r�pertoires et sous-sous-r�pertoires...) */
		$requete = "select * from `".MULTIMEDIASPACE_REPERTOIRE."` where `bornegauche` > '".$repertoireparent->bornegauche."' and `bornedroite` < '".$repertoireparent->bornedroite."' order by bornegauche asc";
		$idrequete = 0;
		$erreurs = array();
		$res = multimediaspace_sql($requete, $erreurs, $idrequete);
		for($i=0;$i<=count($res)-1;$i++) {
			$repertoire = new Multimediaspace_Repertoire($res[$i]['id'],$res[$i]['nom'],$res[$i]['bornegauche'],$res[$i]['bornedroite'],$res[$i]['heritedesdroits'],$res[$i]['commentairesactifs']);
			$ssrep[] = $repertoire;
		}
		
		if (!$prendreencomptelessousniveaux) {
			/* Parcourt les r�sultats et recherche des noeuds (pas des feuilles). Pour avoir uniquement les r�pertoires
			   du niveau juste en-dessous, on supprime les enfants de ses sous-r�pertoires */
			$trouve = true;
			while ($trouve) { /* Tant qu'on trouve des r�pertoires � supprimer */
				$trouve = false;
				for($i=0;$i<=count($ssrep)-1;$i++) {
					/* R�cup�re les noeuds */
					if ($ssrep[$i]->bornedroite - $ssrep[$i]->bornegauche > 1) {
						for($j=0;$j<=count($ssrep)-1;$j++) {
							if ($ssrep[$j]->bornegauche > $ssrep[$i]->bornegauche && $ssrep[$j]->bornedroite < $ssrep[$i]->bornedroite) {
								/* Supprime le r�pertoire du tableau */
								array_splice($ssrep, $j, 1);
								$trouve = true;
								break(2); /* Sort des 2 boucles for */
							}
						}
					}
				}
			}
		}
		return $ssrep;
	}
	
	/* Retourne le nombre de vid�os */
	function nbvideos() {
		return 0;
	}
	
}

